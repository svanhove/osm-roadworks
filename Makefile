.PHONY: all import download dockerenv clean

PWD=$(shell pwd)
CONTINENT=europe

all: import dockerenv download upload clean

## Download map
download:
	docker run --rm -v $(PWD):/data osmgraph mkdir -p /data/1-download
	docker run --rm -v $(PWD):/data osmgraph wget --progress=dot:giga http://download.geofabrik.de/$(CONTINENT)-latest.osm.pbf -O /data/1-download/$(CONTINENT)-latest.osm.pbf

## create docker
dockerenv:
	docker build -t osmgraph -f Dockerfile.build .

## Import
import:
	docker run -v $(PWD):/data -e "trafficData.username"=bemobile -e "trafficData.password"=oY1qR0EW9XYiGRFhXfESSjoHzZpfbv -e kafkaBroker=dep-kafka00.be-mobile-ops.net:9092 -e kafkaSourceTopic=tmc-event-osm-be -e groupId=graph01 --dns-search be-mobile-ops.net -e JAVA_OPTS="-Xmx10000m -Xms10000m -server" -p 8988:8988 -d osmgraph0 ./graphhopper.sh import /data/1-download/$(CONTINENT)-latest.osm.pbf

upload:
 	docker run --rm -v $(PWD):/data osmgraph zip /data/$(CONTINENT)-latest-osm-gh.zip /data/1-download/$(CONTINENT)-latest.osm-gh/*
 	docker run --rm -v $(PWD):/data osmgraph curl -u bemobile\:oY1qR0EW9XYiGRFhXfESSjoHzZpfbv -T /data/repository.be-mobile.biz/dumps/gh/europe /data/$(CONTINENT)-latest-osm-gh.zip http\://repository.be-mobile.biz/dumps/gh/europe
## clean
clean:
	docker run --rm -v $(PWD):/data osmgraph rm -rf /data/1-download
