package com.graphhopper.routing;

import static com.graphhopper.routing.AlternativeRoute.calcSortBy;
import com.graphhopper.routing.ch.Path4CH;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.weighting.FastestWeighting;
import com.graphhopper.routing.weighting.ShortestWeighting;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.SPTEntry;
import com.graphhopper.util.CHEdgeIterator;
import com.graphhopper.util.CHEdgeIteratorState;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.GHUtility;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TObjectProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Helper class to find alternatives and alternatives for round trip.
*/
public class AlternativeBidirSearch extends AStarBidirection {
    private final double explorationFactor;
    private int to;
    private int from;
    private static final Comparator<AlternativeInfo> ALT_COMPARATOR = new Comparator<AlternativeInfo>() {
        @Override
        public int compare(AlternativeInfo o1, AlternativeInfo o2) {
            return Double.compare(o1.sortBy, o2.sortBy);
        }
    };
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public AlternativeBidirSearch(Graph graph, Weighting weighting, TraversalMode tMode,
                                  double explorationFactor) {
        super(graph, weighting, tMode);
        this.explorationFactor = explorationFactor;
//            weightedEdgeFilter=new WeightedEdgeFilter(weighting);
    }

    public TIntObjectMap<AStar.AStarEntry> getBestWeightMapFrom() {
        return bestWeightMapFrom;
    }

    public TIntObjectMap<AStar.AStarEntry> getBestWeightMapTo() {
        return bestWeightMapTo;
    }

    @Override
    public boolean finished() {
        // we need to finish BOTH searches identical to CH
        if (finishedFrom && finishedTo)
            return true;

        if (isMaxVisitedNodesExceeded())
            return true;

        // The following condition is necessary to avoid traversing the full graph if areas are disconnected
        // but it is only valid for none-CH e.g. for CH it can happen that finishedTo is true but the from-SPT could still reach 'to'
        if (!bestPath.isFound() && (finishedFrom || finishedTo))
            return true;

        // increase overlap of both searches:
//            return currFrom.weight + currTo.weight > explorationFactor * bestPath.getWeight();
        return false;
        // This is more precise but takes roughly 20% longer: return currFrom.weight > bestPath.getWeight() && currTo.weight > bestPath.getWeight();
        // For bidir A* and AStarEdge.getWeightOfVisitedPath see comment in AStarBidirection.finished
    }

    public Path searchBest(int to, int from) {
        this.to=to;
        this.from=from;
        createAndInitPath();
        initFrom(to, 0);
        initTo(from, 0);
        // init collections and bestPath.getWeight properly
        runAlgo();
        return extractPath();
    }

    /**
     * @return the information necessary to handle alternative paths. Note that the paths are
     * not yet extracted.
     */
    public List<AlternativeInfo> calcAlternatives(final int maxPaths,
                                                  double maxWeightFactor, final double weightInfluence,
                                                  final double maxShareFactor, final double shareInfluence,
                                                  final double minPlateauFactor, final double plateauInfluence) {
        final double maxWeight = maxWeightFactor * bestPath.getWeight();
//        Weighting weighting1 = bestPath.getWeighting();
        double radius=(0.03*bestPath.getWeight());
        double onRoute=(0.002*bestPath.getWeight());
//        double radius=Math.min(0.05*bestPath.getWeight(),maxRadius);
//        double onRoute=Math.min(0.002*bestPath.getWeight(),maxOnRoute);
        expandBestWeightMaps(onRoute,radius,6,bestPath);
        final TIntSet bestNodes=getNodes(bestPath);

        final TIntObjectHashMap<TIntSet> traversalIDMap = new TIntObjectHashMap<TIntSet>();
        final AtomicInteger startTID = addToMap(traversalIDMap, bestPath);

        // find all 'good' alternatives from forward-SPT matching the backward-SPT and optimize by
        // small total weight (1), small share and big plateau (3a+b) and do these expensive calculations
        // only for plateau start candidates (2)
        final List<AlternativeInfo> alternatives = new ArrayList<AlternativeInfo>(maxPaths);
        final Counter countCrossings=new Counter();

        double bestPlateau = bestPath.getWeight();
        double bestShare = 0;
        double sortBy = calcSortBy(weightInfluence, bestPath.getWeight(),
                shareInfluence, bestShare,
                plateauInfluence, bestPlateau);

        final AlternativeInfo bestAlt = new AlternativeInfo(sortBy, bestPath,
                bestPath.sptEntry, bestPath.edgeTo, bestShare, getAltNames(graph, bestPath.sptEntry));
        alternatives.add(bestAlt);
        final List<SPTEntry> bestPathEntries = new ArrayList<SPTEntry>(2);

//            bestAlternativeWeightMapFrom.forEachEntry(new TIntObjectProcedure<AlternativeSPTEntry>() {
//                @Override
//                public boolean execute(final int traversalId, final AlternativeSPTEntry fromSPTEntry) {
//                    double lat=graph.getNodeAccess().getLat(fromSPTEntry.adjNode);
//                    double lon=graph.getNodeAccess().getLon(fromSPTEntry.adjNode);
//                    logger.info(""+lat+","+lon+",");
//                    return true;
//                }
//            });
        checkMapForNullOptimalPointers(bestAlternativeWeightMapFrom);
        checkMapForNullOptimalPointers(bestAlternativeWeightMapTo);
        bestAlternativeWeightMapFrom.forEachEntry(new TIntObjectProcedure<AlternativeSPTEntry>() {
            @Override
            public boolean execute(final int traversalId, final AlternativeSPTEntry fromSPTEntry) {
                SPTEntry toSPTEntry = bestAlternativeWeightMapTo.get(traversalId);
                if (toSPTEntry == null)
                    return true;
                
                if (traversalMode.isEdgeBased()) {
                    if (toSPTEntry.parent != null)
                        // move to parent for two reasons:
                        // 1. make only turn costs missing in 'weight' and not duplicating current edge.weight
                        // 2. to avoid duplicate edge in Path
                        toSPTEntry = toSPTEntry.parent;
                    // TODO else if fromSPTEntry.parent != null fromSPTEntry = fromSPTEntry.parent;

                } else // The alternative path is suboptimal when both entries are parallel
                    if (fromSPTEntry.edge == toSPTEntry.edge)
                        return true;
                countCrossings.increment();

                // (1) skip too long paths
                final double weight = fromSPTEntry.getWeightOfVisitedPath() + toSPTEntry.getWeightOfVisitedPath();
                if (weight > maxWeight)
                    return true;

                // (2) Use the start traversal ID of a plateau as ID for the alternative path.
                // Accept from-EdgeEntries only if such a start of a plateau
                // i.e. discard if its parent has the same edgeId as the next to-SPTEntry.
                // Ignore already added best path
                if (isBestPath(fromSPTEntry, bestPath))
                    return true;

                // For edge based traversal we need the next entry to find out the plateau start
                SPTEntry tmpFromEntry = traversalMode.isEdgeBased() ? fromSPTEntry.parent : fromSPTEntry;
                if (tmpFromEntry == null || tmpFromEntry.parent == null) {
                    // we can be here only if edge based and only if entry is not part of the best path
                    // e.g. when starting point has two edges and one is part of the best path the other edge is path of an alternative
                    assert traversalMode.isEdgeBased();
                } else {
                    int nextToTraversalId = traversalMode.createTraversalId(tmpFromEntry.adjNode,
                            tmpFromEntry.parent.adjNode, tmpFromEntry.edge, true);
                    SPTEntry tmpNextToSPTEntry = bestAlternativeWeightMapTo.get(nextToTraversalId);
                    if (tmpNextToSPTEntry == null)
                        return true;

                    if (traversalMode.isEdgeBased())
                        tmpNextToSPTEntry = tmpNextToSPTEntry.parent;
                    // skip if on plateau
                    if (fromSPTEntry.edge == tmpNextToSPTEntry.edge)
                        return true;
                }
//                verifySPT(fromSPTEntry,(AlternativeSPTEntry)toSPTEntry);
                // (3a) calculate plateau, we know we are at the beginning of the 'from'-side of
                // the plateau A-B-C and go further to B
                // where B is the next-'from' of A and B is also the previous-'to' of A.
                //
                //      *<-A-B-C->*
                //        /    \
                //    start    end
                //
                // extend plateau in only one direction necessary (A to B to ...) as we know
                // that the from-SPTEntry is the start of the plateau or there is no plateau at all
                //
//                double la=graph.getNodeAccess().getLat(fromSPTEntry.adjNode);
//                double lo=graph.getNodeAccess().getLon(fromSPTEntry.adjNode);
                double plateauWeight = calculatePlateauWeight(toSPTEntry);
                
//                System.out.println(""+la+";"+lo+";"+plateauWeight);
                if (plateauWeight <= 0 || plateauWeight / weight < minPlateauFactor)
                    return true;
//                    logger.info("Found plateau of "+plateauWeight);

                if (fromSPTEntry.parent == null)
                    throw new IllegalStateException("not implemented yet. in case of an edge based traversal the parent of fromSPTEntry could be null");

                // (3b) calculate share
                SPTEntry fromEE = getFirstShareEE((AlternativeSPTEntry)fromSPTEntry, true);
                SPTEntry toEE = getFirstShareEE((AlternativeSPTEntry)toSPTEntry, false);
                double sharedOnForward=fromEE!=null?fromEE.getWeightOfVisitedPath():0;//these are original GH code, but 
                double sharedOnBackward=toEE!=null?toEE.getWeightOfVisitedPath():0;
                double sharedOnForward2=fromEE!=null?((AlternativeSPTEntry)fromEE).getWeightOnOriginal():0;
                double sharedOnBackward2=toEE!=null?((AlternativeSPTEntry)toEE).getWeightOnOriginal():0;

                double lat=graph.getNodeAccess().getLat(fromSPTEntry.adjNode);
                double lon=graph.getNodeAccess().getLon(fromSPTEntry.adjNode);
//                double latTo=graph.getNodeAccess().getLat(toEE.adjNode);
//                double lonTo=graph.getNodeAccess().getLon(toEE.adjNode);
//                System.out.println(""+lat+","+lon+",");

                double shareWeight = sharedOnForward2+sharedOnBackward2;
                double sharedRatio=shareWeight / bestPath.getWeight();
//                logger.info("Found share of "+shareWeight+" on "+bestPath.getWeight());
                boolean smallShare = shareWeight / bestPath.getWeight() < maxShareFactor;
                boolean smallDetour=(fromSPTEntry.getWeightOfVisitedPath()+toSPTEntry.getWeightOfVisitedPath()-shareWeight)/(bestPath.getWeight()-shareWeight)<3;
//                if (smallShare) 
                if (smallShare&&smallDetour) 
                {
                    verifySPT(fromSPTEntry,(AlternativeSPTEntry)toSPTEntry);
                    AlternativeSPTEntry aspt=simplifyPath(fromSPTEntry,bestNodes);
                    AlternativeSPTEntry aspto=bestAlternativeWeightMapTo.get(aspt.adjNode);
                    SPTEntry p=toSPTEntry;
//                    while(p!=null){
//                        double lat=graph.getNodeAccess().getLat(p.adjNode);
//                        double lon=graph.getNodeAccess().getLon(p.adjNode);
//
//                        p=p.parent;
//                        if(Math.abs(lat-50.8219965)<0.1&&Math.abs(lon-4.835107844)<0.1){
//                            System.out.println("Node "+p.adjNode+" "+lat+","+lon+",");
//                        }
//                    }
                    List<String> altNames = getAltNames(graph, aspt);
//                    List<String> altNames = getAltNames(graph, fromSPTEntry);

                    double sortBy = calcSortBy(weightInfluence, weight, shareInfluence, shareWeight, plateauInfluence, plateauWeight);
                    double worstSortBy = getWorstSortBy();

                    // plateaus.add(new PlateauInfo(altName, plateauEdges));
                    if (sortBy < worstSortBy || alternatives.size() < maxPaths) {

                        Path path = new Path4CH(graph,graph.getBaseGraph(),weighting).
                                setSPTEntryTo(aspto).setSPTEntry(aspt).
//                                setSPTEntryTo(toSPTEntry).setSPTEntry(fromSPTEntry).
                                setWeight( aspto.getWeightOfVisitedPath() + aspt.getWeightOfVisitedPath());
//                            Path path = new PathBidirRef(graph, weighting).
//                                    setSPTEntryTo(toSPTEntry).setSPTEntry(fromSPTEntry).
//                                    setWeight(weight);
//                        logger.info("Found path with ratio "+sharedRatio+", extracting");
////                        logger.info("parents "+ lat+","+lon+","+latTo+","+lonTo);
//                        logger.info("parents "+ lat+","+lon);
//                        logger.info("On Original "+sharedOnForward+", "+sharedOnBackward);
//                        logger.info("On Original "+sharedOnForward2+", "+sharedOnBackward2);
                        path.extract();
                        for (AlternativeInfo alternative : alternatives) {
                            if(samePath(alternative,path,shareWeight))
                                return true;
                        }
                        // for now do not add alternatives to set, if we do we need to remove then on alternatives.clear too (see below)
                        // AtomicInteger tid = addToMap(traversalIDMap, path);
                        // int tid = traversalMode.createTraversalId(path.calcEdges().get(0), false);
                        alternatives.add(new AlternativeInfo(sortBy, path, fromEE, toEE, shareWeight, altNames));

                        Collections.sort(alternatives, ALT_COMPARATOR);
//                            if (alternatives.get(0) != bestAlt)
//                                throw new IllegalStateException("best path should be always first entry");

                        if (alternatives.size() > maxPaths)
                            alternatives.subList(maxPaths, alternatives.size()).clear();
                    }
                }
                return true;
            }
            private AlternativeSPTEntry simplifyPath(AlternativeSPTEntry spt,TIntSet nodesOnBestPath){
                boolean con=true;
                while(con&spt.parent!=null){
                    con=false;
                    int parentNode=spt.parent.adjNode;
                    if(nodesOnBestPath.contains(parentNode)){
                        con=true;
                        spt=(AlternativeSPTEntry)spt.parent;
                    }
                }
//                AlternativeSPTEntry parentSPTOnOptimal = spt.getParentSPTOnOptimal();
//                while(spt.parent!=parentSPTOnOptimal)spt=(AlternativeSPTEntry)spt.parent;
                return spt;
            }

            /**
             * Extract path until we stumble over an existing traversal id
             */
            SPTEntry getFirstShareEE(AlternativeSPTEntry startEE, boolean reverse) {
//                    while (startEE.parent != null) {
//                        // TODO we could make use of traversal ID directly if stored in SPTEntry
//                        int tid = traversalMode.createTraversalId(startEE.adjNode, startEE.parent.adjNode, startEE.edge, reverse);
//                        if (isAlreadyExisting(tid))
//                            return startEE;
//
//                        startEE = startEE.parent;
//                    }
//
//                    return startEE;
                    return startEE.getParentSPTOnOptimal();
            }
            
            /**
             * This method returns true if the specified tid is already existent in the
             * traversalIDMap
             */
            boolean isAlreadyExisting(final int tid) {
                return !traversalIDMap.forEachValue(new TObjectProcedure<TIntSet>() {
                    @Override
                    public boolean execute(TIntSet set) {
                        return !set.contains(tid);
                    }
                });
            }

            /**
             * Return the current worst weight for all alternatives
             */
            double getWorstSortBy() {
                if (alternatives.isEmpty())
                    throw new IllegalStateException("Empty alternative list cannot happen");
                return alternatives.get(alternatives.size() - 1).sortBy;
            }

            // returns true if fromSPTEntry is identical to the specified best path
            boolean isBestPath(SPTEntry fromSPTEntry, Path bestPath) {
                if (traversalMode.isEdgeBased()) {
                    if (GHUtility.getEdgeFromEdgeKey(startTID.get()) == fromSPTEntry.edge) {
                        if (fromSPTEntry.parent == null)
                            throw new IllegalStateException("best path must have no parent but was non-null: " + fromSPTEntry);
                        return true;
                    }

                } else if (fromSPTEntry.parent == null) {
                    bestPathEntries.add(fromSPTEntry);
                    if (bestPathEntries.size() > 1)
                        throw new IllegalStateException("There is only one best path but was: " + bestPathEntries);

                    if (startTID.get() != fromSPTEntry.adjNode)
                        throw new IllegalStateException("Start traversal ID has to be identical to root edge entry "
                                + "which is the plateau start of the best path but was: " + startTID + " vs. adjNode: " + fromSPTEntry.adjNode);

                    return true;
                }

                return false;
            }

            private double calculatePlateauWeight(SPTEntry toSPTEntry) {
                double plateauWeight=0;
                SPTEntry prevToSPTEntry = toSPTEntry;
                // List<Integer> plateauEdges = new ArrayList<Integer>();
                loop:
                while (prevToSPTEntry.parent != null) {
                    int nextFromTraversalId = traversalMode.createTraversalId(prevToSPTEntry.adjNode, prevToSPTEntry.parent.adjNode,
                            prevToSPTEntry.edge, false);

                    SPTEntry nextFromSPTEntry = bestAlternativeWeightMapFrom.get(nextFromTraversalId);
                    // end of a plateau
                    if (nextFromSPTEntry == null)
                        break;
// the paths will need to be unpacked here as the shortcuts can be intertwined and dont overlap literally
//should decompose the current edge to its basic ones untill eiter a match is found (jeuj, then move on further), both links are at the basic level and don't match              
                    // is the next from-SPTEntry on the plateau?
                    if (prevToSPTEntry.edge != nextFromSPTEntry.edge){
                        if(prevToSPTEntry.edge==-1||nextFromSPTEntry.edge==-1){
                            break;
                        }
                        CHEdgeIteratorState chTo=(CHEdgeIteratorState)graph.getEdgeIteratorState(prevToSPTEntry.edge, prevToSPTEntry.parent.adjNode);
                        CHEdgeIteratorState chFrom=(CHEdgeIteratorState)graph.getEdgeIteratorState(nextFromSPTEntry.edge, nextFromSPTEntry.adjNode);

//                        double toWeight=getWeight(chTo,false);
//                        double fromWeight=getWeight(chFrom,false);
                        boolean chtoIsShortcut=chTo.isShortcut();
                        boolean chfromIsShortcut=chFrom.isShortcut();
                        if(//toWeight>fromWeight&&
                                chtoIsShortcut){
                            boolean success=unpackSPT((AlternativeSPTEntry)prevToSPTEntry,chTo,true);
                            if(success) continue loop;
                            return 0;

                        }else if(chfromIsShortcut){
                            boolean success=unpackSPT((AlternativeSPTEntry)nextFromSPTEntry,chFrom,false);
                            if(success) continue loop;
                            return 0;

                        }
                        return plateauWeight;
                    }
                    // plateauEdges.add(prevToSPTEntry.edge);
                    double cost=prevToSPTEntry.getWeightOfVisitedPath() - prevToSPTEntry.parent.getWeightOfVisitedPath();
                    if(cost<0){
                        return 0;
                    }
                    plateauWeight += cost;
                    prevToSPTEntry = prevToSPTEntry.parent;
                }
                return plateauWeight; 
            }
            private boolean unpackSPT(AlternativeSPTEntry spt, CHEdgeIteratorState chState,boolean toDirection){

                int skippedEdge1 = chState.getSkippedEdge1();
                int skippedEdge2 = chState.getSkippedEdge2();

                int from = chState.getBaseNode(), to = chState.getAdjNode();
                CHEdgeIteratorState ch1=(CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, from);//but needs to be reversed
                CHEdgeIteratorState ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, to);

//                    CHEdgeIteratorState chSE1 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, Integer.MIN_VALUE);
//                    CHEdgeIteratorState chSE2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, Integer.MIN_VALUE);
                if(ch1==null||ch2==null){
                    ch1 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, from);//but needs to be reversed
                    ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, to);
                    skippedEdge1=chState.getSkippedEdge2();
                    skippedEdge2=chState.getSkippedEdge1();
                }
                int intermediate=ch2.getBaseNode();

                int se=toDirection?skippedEdge2:skippedEdge1;
                int edgeIDForOldSPT=toDirection?skippedEdge1:skippedEdge2;
                double w=getWeight(toDirection?ch1:ch2,toDirection);
                double tw=spt.getWeightOfVisitedPath()-w;
                TIntObjectMap<AlternativeSPTEntry> map = toDirection?bestAlternativeWeightMapTo:bestAlternativeWeightMapFrom;
                AlternativeSPTEntry get =map.get(intermediate);
                if(get!=null){
                    double ww=get.getWeightOfVisitedPath();
                    if((int)ww<(int)tw){
                        return false;
                    }
                }
//                AlternativeSPTEntry newAltSPT=new AlternativeSPTEntry(se, intermediate,tw,
                AlternativeSPTEntry newAltSPT=new AlternativeSPTEntry(se, intermediate,
                        spt.weightOffOriginal-w, spt.weightOnOriginal, spt.nrHopsOfOriginal);

                newAltSPT.setParentSPTOnOptimal(spt.getParentSPTOnOptimal());
                newAltSPT.parent=spt.parent;
                newAltSPT.verifiedSPT=spt.verifiedSPT;
                spt.parent=newAltSPT;
                spt.edge=edgeIDForOldSPT;

                map.put(intermediate,newAltSPT);
                return true;
            }                

            private boolean samePath(AlternativeInfo alternative, Path path,double shareWeight) {
                if(Math.abs(path.distance-alternative.getPath().distance)>1){
//                    logger.info("different distance:" +path.distance+", "+alternative.getPath().distance);
                    return false;
                }
                if(Math.abs(path.time-alternative.getPath().time)>10){
//                    logger.info("different time:" +path.time+", "+alternative.getPath().time);
                    return false;
                }
//                    if(shareWeight!=alternative.getShareWeight()){
//                        logger.info("different shareWeight:" +shareWeight+", "+alternative.getShareWeight());
//                        return false;
//                    }
                if(path.getEdgeCount()!=alternative.getPath().getEdgeCount()){
//                    logger.info("different edgeCount:" +path.getEdgeCount()+", "+alternative.getPath().getEdgeCount());
                    return false;
                }
                List<EdgeIteratorState> edges1 = path.calcEdges();
                List<EdgeIteratorState> edges2 = alternative.getPath().calcEdges();
                if(edges1.size()!=edges2.size()){
//                    logger.info("Different number of edge "+edges1.size()+","+edges2.size());
                    return false;
                }
                for(int i=0;i<edges1.size();i++){
                    if(edges1.get(i).getEdge()!=edges2.get(i).getEdge()){
//                        logger.info("Edge "+i+" is different "+edges1.get(i).getEdge()+", "+edges2.get(i).getEdge());
                        return false;
                    }
                }
                return true;
            }

            private void verifySPT(AlternativeSPTEntry fromSPTEntry, AlternativeSPTEntry toSPTEntry) {
                setVerifiedSPT(fromSPTEntry,false);
                setVerifiedSPT(toSPTEntry,true);
            }
            private void setVerifiedSPT(AlternativeSPTEntry spt,boolean toDirection){
                TIntObjectMap<AlternativeSPTEntry> map=toDirection?bestAlternativeWeightMapTo:bestAlternativeWeightMapFrom;
                AlternativeSPTEntry optimalParent = spt.getParentSPTOnOptimal();
                AStarBidirection verifier=new AStarBidirection(graph, weighting, traversalMode);
                int startNode;
                int endNode;
                double originalWeight;
                while(optimalParent!=null&&optimalParent.verifiedSPT==false&&optimalParent.weightOffOriginal!=0){
                    optimalParent=optimalParent.getParentSPTOnOptimal();
                }
                if(optimalParent==null){
                    logger.info("No parent for "+spt.adjNode+", treating as origin/destination");
                    startNode=toDirection?spt.adjNode:from;
                    endNode=toDirection?to:spt.adjNode;
                    originalWeight=0;
                } else{
                    startNode=toDirection?spt.adjNode:optimalParent.adjNode;
                    endNode=toDirection?optimalParent.adjNode:spt.adjNode;
                    originalWeight=optimalParent.weightOnOriginal;

                }
                 

                Path path = verifier.calcPath(startNode,endNode);
                List<EdgeIteratorState> edges = path.calcEdges();
                int index=toDirection?edges.size()-1:0;
                for (int i=0;i<edges.size();i++) {
                    EdgeIteratorState eis=edges.get(index);

                    CHEdgeIteratorState ceis=(CHEdgeIteratorState)graph.getEdgeIteratorState(eis.getEdge(), eis.getAdjNode());
                    int dest=eis.getAdjNode();
                    int origin=eis.getBaseNode();
                    int nodeIDToUpdate=toDirection?origin:dest;
                    int otherNodeID=!toDirection?origin:dest;
                    double edgeCost=weighting.calcWeight(ceis, false, -1);//This is not entirely correct but does not matter here
                    AlternativeSPTEntry toUpdate=map.get(nodeIDToUpdate);
                    AlternativeSPTEntry otherSPT=map.get(otherNodeID);

                    if(toUpdate==null){
                        toUpdate=new AlternativeSPTEntry(eis.getEdge(), nodeIDToUpdate, otherSPT.weightOffOriginal, otherSPT.weightOnOriginal+originalWeight, otherSPT.getNrHopsOfOriginal()+1);
//                        toUpdate=new AlternativeSPTEntry(eis.getEdge(), nodeIDToUpdate, otherSPT.weight+edgeCost+originalWeight, otherSPT.weightOffOriginal, otherSPT.weightOnOriginal+originalWeight, otherSPT.getNrHopsOfOriginal()+1);
                        
                        toUpdate.setParentSPTOnOptimal(otherSPT.getParentSPTOnOptimal());
                        toUpdate.verifiedSPT=true;
                        toUpdate.parent=otherSPT;
                        map.put(nodeIDToUpdate, toUpdate);
                    }
                    else if(toUpdate.verifiedSPT==false){
                        toUpdate.setParentSPTOnOptimal(otherSPT.getParentSPTOnOptimal());

                        toUpdate.edge=eis.getEdge();
                        toUpdate.parent=otherSPT;
                        toUpdate.weightOnOriginal=otherSPT.weightOnOriginal;
                        toUpdate.verifiedSPT=true;

//                        toUpdate.weight=otherSPT.weight+edgeCost;
                        toUpdate.weightOffOriginal=otherSPT.weightOffOriginal+edgeCost;
                        toUpdate.nrHopsOfOriginal=otherSPT.getNrHopsOfOriginal()+1;
                    }
                    if(toDirection)index--;
                    else index++;
                }
            }
        });
        logger.info("Count crossings = "+countCrossings.count);

        if(weightedEdgeFilter!=null)weightedEdgeFilter.printSummary();
        return alternatives;
    }

    /**
     * This method adds the traversal IDs of the specified path as set to the specified map.
     */
    AtomicInteger addToMap(TIntObjectHashMap<TIntSet> map, Path path) {
        TIntSet set = new TIntHashSet();
        final AtomicInteger startTID = new AtomicInteger(-1);
        for (EdgeIteratorState iterState : path.calcEdges()) {
            int tid = traversalMode.createTraversalId(iterState, false);
            set.add(tid);
            if (startTID.get() < 0) {
                // for node based traversal we need to explicitely add base node as starting node and to list
                if (!traversalMode.isEdgeBased()) {
                    tid = iterState.getBaseNode();
                    set.add(tid);
                }

                startTID.set(tid);
            }
        }
        map.put(startTID.get(), set);
        return startTID;
    }
    List<String> getAltNames(Graph graph, SPTEntry ee) {
        if (ee == null || !EdgeIterator.Edge.isValid(ee.edge))
            return Collections.emptyList();

        EdgeIteratorState iter = graph.getEdgeIteratorState(ee.edge, Integer.MIN_VALUE);
        if (iter == null||((CHEdgeIteratorState)iter).isShortcut())
            return Collections.emptyList();

        String str = iter.getName();
        if (str.isEmpty())
            return Collections.emptyList();

        return Collections.singletonList(str);
    }
    PriorityQueue<AlternativeSPTEntry> pqFrom;
    TIntObjectMap<AlternativeSPTEntry> bestAlternativeWeightMapFrom;
    TIntObjectMap<AlternativeSPTEntry> bestAlternativeWeightMapTo;
    PriorityQueue<AlternativeSPTEntry> pqTo;
    int maxNrHops;
    double maxWeightOffOriginal;
    double maxInitialWeightRadius;
    private double getWeight(CHEdgeIteratorState eis,boolean reverse) {
        if(eis.isShortcut()){
            return eis.getWeight();
        }
        else return weighting.calcWeight(eis, reverse, EdgeIterator.NO_EDGE);
    }
    private QueueMapCouple initAlternativeWeightMapAndQueue(TIntObjectMap<AStar.AStarEntry> bestWeightMap
            ,int nrHops,SPTEntry root,boolean toDirection){
        bestWeightMap.clear();

        final Map<AStar.AStarEntry,AStar.AStarEntry> optEntries=new HashMap<>();
        AStar.AStarEntry temp=(AStar.AStarEntry)root;
        //unpack root
        unpackRoot(root,bestWeightMap,toDirection);

        Set<AStar.AStarEntry>allOnBest=optEntries.keySet();
        int nrInit=bestWeightMap.size()*nrHops;
        final PriorityQueue<AlternativeSPTEntry> pq = new PriorityQueue<>(nrInit);
        final TIntObjectMap<AlternativeSPTEntry> map= new TIntObjectHashMap<>();
        final Map<AStar.AStarEntry,AlternativeSPTEntry> alternativeForOriginal=new HashMap<>();
        while(temp!=null){                
//                logger.info(""+temp.weightOfVisitedPath);
            optEntries.put(temp, temp);
            AlternativeSPTEntry x = new AlternativeSPTEntry(temp);
            x.verifiedSPT=true;
            x.setParentSPTOnOptimal(x,false);
                alternativeForOriginal.put(temp,x);
                map.put(temp.adjNode, x);
                pq.add(x);
            temp=(AStar.AStarEntry)(temp.parent);

        }

//            bestWeightMap.forEachEntry(new TIntObjectProcedure<AStarEntry>() {
//                @Override
//                public boolean execute(final int traversalId, final AStarEntry entry) {
//                    AlternativeSPTEntry x = new AlternativeSPTEntry(entry);
//                    mm.put(entry,x);
//                    if(!optEntries.containsKey(entry))optEntries.put(entry, (AStarEntry)entry.parent);
//                    map.put(traversalId, x);
//                    pq.add(x);
//                    return true;
//                }
//            });

//            findOptimalEntries(optEntries,mm,toDirection,allOnBest);
        for (Map.Entry<AStar.AStarEntry, AlternativeSPTEntry> entry : alternativeForOriginal.entrySet()) {
            AStar.AStarEntry original=entry.getKey();
            AlternativeSPTEntry alternative=entry.getValue();
            AStar.AStarEntry originalParent = (AStar.AStarEntry)original.parent;
            AStar.AStarEntry parentOnOptimal=optEntries.get(originalParent);
            AlternativeSPTEntry alternativeOriginalParent=alternativeForOriginal.get(originalParent);
            alternative.parent=alternativeOriginalParent;
//                if(alternativeOriginalParent==null){
//                    System.out.println("No alternativeOriginalParent for "+alternative.adjNode);
//                }

//            AlternativeSPTEntry alternativeOptimalParent=alternativeForOriginal.get(parentOnOptimal);
//            alternative.setParentSPTOnOptimal(alternativeOptimalParent);
//                if(alternativeOptimalParent==null){
//                    System.out.println("No alternativeOptimalParent for "+alternative.adjNode);
//                }
        }
        checkMapForNullOptimalPointers(map);
        return new QueueMapCouple(pq,map);
}
    private void findOptimalEntries(Map<AStar.AStarEntry,AStar.AStarEntry> optimalEntries,Map<AStar.AStarEntry,
            AlternativeSPTEntry> tr,boolean toDirection,Set<AStar.AStarEntry> best){
        TIntObjectMap<AStar.AStarEntry> map=toDirection?bestWeightMapTo:bestWeightMapFrom;
        Map<AStar.AStarEntry,AlternativeSPTEntry> extra=new HashMap<>();
        for (AStar.AStarEntry a : tr.keySet()) {
            AStar.AStarEntry temp=a;

            while(temp.edge!=-1){
                CHEdgeIteratorState chState=(CHEdgeIteratorState)graph.getEdgeIteratorState(temp.edge, temp.adjNode);
                if(!chState.isShortcut()){
                    break;
                }
                double totalWeight=chState.getWeight();
                double tt=a.parent.getWeightOfVisitedPath();
                int base=chState.getBaseNode();
                AStar.AStarEntry sptBase=map.get(base);
//                    if(temp.parent!=sptBase){
//                        logger.info("Different parent");
//                    }
                if(!(optimalEntries.containsKey(sptBase)&&optimalEntries.get(sptBase)==sptBase)){
                    break;
                }

                int skippedEdge1 = chState.getSkippedEdge1();
                int skippedEdge2 = chState.getSkippedEdge2();

                int fromPhysicalDirection = toDirection?chState.getAdjNode():chState.getBaseNode();
                int toPhysicalDirection =  toDirection?chState.getBaseNode():chState.getAdjNode();
                CHEdgeIteratorState ch1=(CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, fromPhysicalDirection);//but needs to be reversed
                CHEdgeIteratorState ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, toPhysicalDirection);
                if(ch1==null||ch2==null){
                    ch1 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, fromPhysicalDirection);//but needs to be reversed
                    ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, toPhysicalDirection);
                    skippedEdge1=chState.getSkippedEdge2();
                    skippedEdge2=chState.getSkippedEdge1();
                }
                int intermediate=ch2.getBaseNode();
                int edgeIDForOldSPT= toDirection?skippedEdge1:skippedEdge2;
                int edgeIDForNewSPT=toDirection?skippedEdge2:skippedEdge1;

                AStar.AStarEntry intermediateSPT = map.get(intermediate);
                double w=getWeight(toDirection?ch1:ch2,toDirection);
//                    double otherw=getWeight(toDirection?ch2:ch1,!toDirection);
                double nw=temp.weightOfVisitedPath-w;
                if(intermediateSPT==null){
                    intermediateSPT=new AStar.AStarEntry(edgeIDForNewSPT, intermediate, 
                            temp.weight,nw);
                    intermediateSPT.parent=temp.parent;

                    map.put(intermediate,intermediateSPT);
                    AlternativeSPTEntry x = new AlternativeSPTEntry(intermediateSPT);
//                        x.parentSPTOnOptimal=
                    extra.put(intermediateSPT,x);
                    if(!optimalEntries.containsKey(intermediateSPT))optimalEntries.put(intermediateSPT, (AStar.AStarEntry)intermediateSPT.parent);
//                        pq.add(x);
                }
                else{
                    if(intermediateSPT.weightOfVisitedPath>=nw*1.05){

                        intermediateSPT.weight=temp.weight;
                        intermediateSPT.edge=edgeIDForNewSPT;
                        intermediateSPT.weightOfVisitedPath=nw;
                        intermediateSPT.parent=temp.parent; 
                        AlternativeSPTEntry alt=tr.get(intermediateSPT);
                        if(alt==null)alt=extra.get(intermediateSPT);
                        alt.update(intermediateSPT);
                    }
                }
                temp.parent=intermediateSPT;
                temp.edge=edgeIDForOldSPT;
                AlternativeSPTEntry alt=tr.get(temp);
                if(alt==null)alt=extra.get(temp);
                alt.update(temp);
                if(!(optimalEntries.containsKey(intermediateSPT)&&optimalEntries.get(intermediateSPT)==intermediateSPT)){
                    temp=intermediateSPT;
                }else{
                    temp=temp;
                }
                if(!best.contains(intermediateSPT))
                    optimalEntries.put(intermediateSPT,(AStar.AStarEntry)intermediateSPT.parent);
                if(!best.contains(temp))
                    optimalEntries.put(temp,(AStar.AStarEntry)temp.parent);

            }

        }
        tr.putAll(extra);
        logger.debug("Found "+extra.size()+" extra AlternativeSPTEntries");
        Set<AStar.AStarEntry> changing=new HashSet<>(optimalEntries.keySet());
        while(!changing.isEmpty()){
            Set<AStar.AStarEntry> tc=new HashSet<>();
            for (AStar.AStarEntry key : changing) {
                AStar.AStarEntry inSettled=optimalEntries.get(key);
                AStar.AStarEntry nextInSettled=optimalEntries.get(inSettled);
                if(inSettled!=nextInSettled){
                    optimalEntries.put(key,nextInSettled);
                    tc.add(key);
                }
            }
            changing=tc;
        }
        Collection<AStar.AStarEntry>ss=optimalEntries.values();
        HashSet<AStar.AStarEntry> set=new HashSet<>(ss);
        logger.debug("Got "+set.size()+" entries on optimal, "+best.size()+" on best");
    }
    private double getMinRadiusWeight(Weighting w){
        if(w instanceof FastestWeighting){
            return 5*60;
        }else if (w instanceof ShortestWeighting){
            return 2000;
        }
        return 0;
    }
    private double getMaxRadiusWeight(Weighting w){
        if(w instanceof FastestWeighting){
            return 25*60;
        }else if (w instanceof ShortestWeighting){
            return 10000;
        }
        return Double.MAX_VALUE;
    }
    private double getBounded(double min,double max,double v){
        return Math.min(max,Math.max(min,v));
    }
    private void expandBestWeightMaps(double maxOffWeight, double maxInitialRadius, int nrHops,PathBidirRef bestPath) {
        this.maxNrHops=nrHops;
        this.maxInitialWeightRadius=getBounded(getMinRadiusWeight(weighting),getMaxRadiusWeight(weighting),maxInitialRadius);
        this.maxWeightOffOriginal=maxOffWeight;
        
        extendRootSPTEntries(bestPath);
        QueueMapCouple qmcFrom=initAlternativeWeightMapAndQueue(bestWeightMapFrom,maxNrHops,bestPath.sptEntry,false);
        bestAlternativeWeightMapFrom=qmcFrom.map;
        pqFrom=qmcFrom.queue;
        checkMapForNullOptimalPointers(bestAlternativeWeightMapFrom);

        QueueMapCouple qmcTo=initAlternativeWeightMapAndQueue(bestWeightMapTo,maxNrHops,bestPath.edgeTo,true);
        bestAlternativeWeightMapTo=qmcTo.map;
        pqTo=qmcTo.queue;
        checkMapForNullOptimalPointers(bestAlternativeWeightMapTo);
        logger.info("Starting while iterations for expanding best weight maps");
        boolean finFrom=pqFrom.isEmpty();
        boolean finTo=pqTo.isEmpty();
        int it=0;
        while (!finFrom|| !finTo) {
            if (!finFrom)
                finFrom =fillAlternativeEdgesFrom();
            if (!finTo)
                finTo = fillAlternativeEdgesTo();
            it++;
        }
        logger.info("Done "+it+" while iterations, accepted "+accepted+
                ", acceptedweight "+acceptedweight+", acceptedhops "+acceptedhops+", acceptedRadius "+acceptedRadius);
        logger.info("BestWeightMap = "+bestWeightMapFrom.size()+" expanded to "+bestAlternativeWeightMapFrom.size());
        logger.info("BestWeightTo = "+bestWeightMapTo.size()+" expanded to "+bestAlternativeWeightMapTo.size());
        checkMapForNullOptimalPointers(bestAlternativeWeightMapTo);

    }
    private boolean fillAlternativeEdgesFrom(){
        AlternativeSPTEntry poll = pqFrom.poll();
        fillAlternativeEdges(poll,pqFrom, bestAlternativeWeightMapFrom, outEdgeExplorer, false);
        return pqFrom.isEmpty();
    }
    private boolean fillAlternativeEdgesTo(){
        AlternativeSPTEntry poll = pqTo.poll();
        fillAlternativeEdges(poll, pqTo,bestAlternativeWeightMapTo, inEdgeExplorer, true);
        return pqTo.isEmpty();
    }
    int accepted=0;
    int acceptedhops=0;
    int acceptedweight=0;
    int acceptedRadius=0;
    void fillAlternativeEdges(AlternativeSPTEntry currEdge,PriorityQueue<AlternativeSPTEntry> pq,
                   TIntObjectMap<AlternativeSPTEntry> bestWeightMap, EdgeExplorer explorer, boolean reverse) {

        int currNode = currEdge.adjNode;
        EdgeIterator iter = explorer.setBaseNode(currNode);
        double weightOnOriginal=currEdge.getWeightOnOriginal();
        double weightOffOriginal=currEdge.getWeightOffOriginal();
        int newNrHops=currEdge.getNrHopsOfOriginal()+1;
        boolean acceptHop=newNrHops<=maxNrHops&&acceptedhops<100000;
        if(acceptHop){
            acceptedhops++;
        }
        boolean acceptWeight=(weightOffOriginal<=maxWeightOffOriginal)&&acceptedweight<100000;
        if(acceptWeight){
            acceptedweight++;
        }
        boolean acceptRadius=(weightOnOriginal+weightOffOriginal<maxInitialWeightRadius)&&acceptedRadius<10000;
        if(acceptRadius){
            acceptedRadius++;
        }
        boolean accept=acceptHop||acceptWeight||acceptRadius;
        if(accept)accepted++;

//            boolean accept=(newNrHops<=maxNrHops);
        while (iter.next()) {
            
            if (!accept&&!accept(iter, currEdge.edge))
                continue;
            int neighborNode = iter.getAdjNode();
            if((reverse&&neighborNode==to)||(!reverse && neighborNode==from))continue;
//            if(neighborNode==68380){
//                double lat=graph.getNodeAccess().getLat(neighborNode);
//                double lon=graph.getNodeAccess().getLon(neighborNode);
////                    logger.info(""+neighborNode+" "+lat+","+lon+",");
//            }

            int traversalId = traversalMode.createTraversalId(iter, reverse);
            // TODO performance: check if the node is already existent in the opposite direction
            // then we could avoid the approximation as we already know the exact complete path!
            double weightEdge=weighting.calcWeight(iter, reverse, currEdge.edge);
            double rw=(int)(weightEdge*1000+1)/1000.0;
            double totalWeight = currEdge.getWeightOfVisitedPath()+rw; 
            if (Double.isInfinite(totalWeight))
                continue;

            AlternativeSPTEntry ase = bestWeightMap.get(traversalId);
//                if((ase!=null)&&(neighborNode==68380||neighborNode==128848||neighborNode==460322||neighborNode==26162)){
//                    double lat=graph.getNodeAccess().getLat(neighborNode);
//                    double lon=graph.getNodeAccess().getLon(neighborNode);
//                    logger.info(""+lat+","+lon+",");
//                }
            double totalWeightPreviousEntry = ase!=null?ase.getWeightOfVisitedPath():0;
            if (ase == null ||  
                    totalWeightPreviousEntry> totalWeight//||
//                        (totalWeightPreviousEntry==totalWeight&&newNrHops<ase.nrHopsOfOriginal)
                ) {
//                    double currWeightToGoal = weightApprox.approximate(neighborNode, reverse);
//                    double estimationFullWeight = alreadyVisitedWeight + currWeightToGoal;
                if (ase == null) {
//                    ase = new AlternativeSPTEntry(iter.getEdge(), neighborNode,totalWeight,
                    ase = new AlternativeSPTEntry(iter.getEdge(), neighborNode,
                            weightOffOriginal+rw,weightOnOriginal,newNrHops);
                    ase.verifiedSPT=currEdge.verifiedSPT&&!accept;
                    ase.setParentSPTOnOptimal(currEdge.getParentSPTOnOptimal());

                    bestWeightMap.put(traversalId, ase);
                } else {
//                        assert (ase.weight > 0.999999 * estimationFullWeight) : "Inconsistent distance estimate "
//                                + ase.weight + " vs " + estimationFullWeight + " (" + ase.weight / estimationFullWeight + "), and:"
//                                + ase.getWeightOfVisitedPath() + " vs " + alreadyVisitedWeight + " (" + ase.getWeightOfVisitedPath() / alreadyVisitedWeight + ")";
                    pq.remove(ase);
                    ase.setParentSPTOnOptimal(currEdge.getParentSPTOnOptimal());

                    ase.edge = iter.getEdge();
//                    ase.weight = totalWeight;
                    ase.weightOffOriginal=weightOffOriginal+rw;
                    ase.nrHopsOfOriginal=newNrHops;
                    ase.weightOnOriginal=weightOnOriginal;
                    ase.verifiedSPT=currEdge.verifiedSPT&&!accept;

                }
                
                ase.parent = currEdge;
//                if(neighborNode==68380&&reverse){
//                    double lat=graph.getNodeAccess().getLat(neighborNode);
//                    double lon=graph.getNodeAccess().getLon(neighborNode);
//                    double latp=graph.getNodeAccess().getLat(currEdge.adjNode);
//                    double lonp=graph.getNodeAccess().getLon(currEdge.adjNode);
//
//                    logger.info(""+lat+","+lon+", parent "+latp+","+lonp);
//                }
                pq.add(ase);
            }
//            else if(neighborNode==68380&&reverse){
//                double lat=graph.getNodeAccess().getLat(neighborNode);
//                double lon=graph.getNodeAccess().getLon(neighborNode);
//                double latp=graph.getNodeAccess().getLat(currEdge.adjNode);
//                double lonp=graph.getNodeAccess().getLon(currEdge.adjNode);
//
//                logger.info(""+lat+","+lon+", tentative parent "+latp+","+lonp+ " but weight "+totalWeightPreviousEntry+"<"+totalWeight);
//            }
//            if(ase.getParentSPTOnOptimal()!=null&&ase.getParentSPTOnOptimal().weightOffOriginal!=0){
//                AlternativeSPTEntry supposedParent=ase.getParentSPTOnOptimal();
//                AlternativeSPTEntry pp=ase.getParentSPTOnOptimal();
//                ase.setParentSPTOnOptimal(pp);
//                double t=ase.weightOnOriginal+ase.weightOffOriginal;
//                ase.weightOnOriginal=supposedParent.weightOnOriginal;
//                ase.weightOffOriginal=t-ase.weightOnOriginal;
//            }

        }
//        checkMapForNullOptimalPointers(bestWeightMap);
    }

    private void unpackRoot(SPTEntry root, TIntObjectMap<AStar.AStarEntry> map,boolean toDirection) {

        AStar.AStarEntry temp=(AStar.AStarEntry)root;
        while(temp!=null&&temp.edge!=-1){
            CHEdgeIteratorState chState=(CHEdgeIteratorState)graph.getEdgeIteratorState(temp.edge, temp.adjNode);//treat as invert depending on toDirection

            if(chState.isShortcut()){
                int skippedEdge1 = chState.getSkippedEdge1();
                int skippedEdge2 = chState.getSkippedEdge2();

                int fromPhysicalDirection = toDirection?chState.getAdjNode():chState.getBaseNode();
                int toPhysicalDirection =  toDirection?chState.getBaseNode():chState.getAdjNode();
                CHEdgeIteratorState ch1=(CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, fromPhysicalDirection);//but needs to be reversed
                CHEdgeIteratorState ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, toPhysicalDirection);
                if(ch1==null||ch2==null){
                    ch1 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge2, fromPhysicalDirection);//but needs to be reversed
                    ch2 = (CHEdgeIteratorState)graph.getEdgeIteratorState(skippedEdge1, toPhysicalDirection);
                    skippedEdge1=chState.getSkippedEdge2();
                    skippedEdge2=chState.getSkippedEdge1();
                }
                int intermediate=ch2.getBaseNode();
                int edgeIDForOldSPT= toDirection?skippedEdge1:skippedEdge2;
                int edgeIDForNewSPT=toDirection?skippedEdge2:skippedEdge1;

                AStar.AStarEntry prev = map.get(intermediate);

                double w=getWeight(toDirection?ch1:ch2,toDirection);

                double nw=temp.weightOfVisitedPath-w;
                if(prev==null){
                    prev=new AStar.AStarEntry(edgeIDForNewSPT, intermediate, 
                            temp.weight,nw);
                    prev.parent=temp.parent;
                    map.put(intermediate,prev);
                    temp.parent=prev;
                    temp.edge=edgeIDForOldSPT;            
                }
                else{
                    if(prev.weightOfVisitedPath>=nw){
                        prev.weight=temp.weight;
                        prev.edge=edgeIDForNewSPT;
                        prev.weightOfVisitedPath=nw;
                        prev.parent=temp.parent; 
                    }
//                    temp.parent=prev;
//                    temp.edge=edgeIDForOldSPT; 
                }

            }else{
                temp=(AStar.AStarEntry)temp.parent;
            }
        }
    }

    private void extendRootSPTEntries(PathBidirRef path) {
        AStar.AStarEntry f=(AStar.AStarEntry)path.sptEntry;
        AStar.AStarEntry b=(AStar.AStarEntry)path.edgeTo;

        //create new SPTEntries from f to destination
        AStar.AStarEntry tempf=f;
        AStar.AStarEntry tempb=b;
        while(tempb.parent!=null){
            AStar.AStarEntry tempbParent= (AStar.AStarEntry)tempb.parent;
            double wn=tempf.weightOfVisitedPath+(tempb.weightOfVisitedPath-tempbParent.weightOfVisitedPath);
            AStar.AStarEntry newf=new AStar.AStarEntry(tempb.edge, tempb.parent.adjNode,tempf.weight, wn);
            newf.parent=tempf;
            tempf=newf;
            tempb=tempbParent;
        }            
        AStar.AStarEntry endF=tempf;

        //create new SPTEntries from b to origin

        tempf=f;
        tempb=b;
        while(tempf.parent!=null){
            AStar.AStarEntry tempfParent= (AStar.AStarEntry)tempf.parent;
            double wn=tempb.weightOfVisitedPath+(tempf.weightOfVisitedPath-tempfParent.weightOfVisitedPath);
            AStar.AStarEntry newb=new AStar.AStarEntry(tempf.edge, tempf.parent.adjNode,tempb.weight, wn);
            newb.parent=tempb;
            tempb=newb;
            tempf=tempfParent;
        }
        AStar.AStarEntry endB=tempb;

        path.edgeTo=endB;
        path.sptEntry=endF;
    }

    private void checkMapForNullOptimalPointers(TIntObjectMap<AlternativeSPTEntry> map) {

    }

    private TIntSet getNodes(PathBidirRef bestPath) {
        TIntSet set=new TIntHashSet();
        SPTEntry temp = bestPath.edgeTo;
        while(temp!=null){
            set.add(temp.adjNode);
            temp=temp.parent;
        }
        temp = bestPath.sptEntry;
        while(temp!=null){
            set.add(temp.adjNode);
            temp=temp.parent;
        }
        set.add(to);
        set.add(from);
        return set;
    }
}

