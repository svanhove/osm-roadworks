package com.graphhopper.routing;

import com.graphhopper.storage.SPTEntry;
import java.util.List;

public class AlternativeInfo {
    protected final double sortBy;
    private final Path path;
    private final SPTEntry shareStart;
    private final SPTEntry shareEnd;
    private final double shareWeight;
    private final List<String> names;

    public AlternativeInfo(double sortBy, Path path, SPTEntry shareStart, SPTEntry shareEnd,
                           double shareWeight, List<String> altNames) {
        this.names = altNames;
        this.sortBy = sortBy;
        this.path = path;
        this.path.setDescription(names);
        this.shareStart = shareStart;
        this.shareEnd = shareEnd;
        this.shareWeight = shareWeight;
    }

    public Path getPath() {
        return path;
    }

    public SPTEntry getShareStart() {
        return shareStart;
    }

    public SPTEntry getShareEnd() {
        return shareEnd;
    }

    public double getShareWeight() {
        return shareWeight;
    }

    public double getSortBy() {
        return sortBy;
    }

    @Override
    public String toString() {
        return names + ", sortBy:" + sortBy + ", shareWeight:" + shareWeight + ", " + path;
    }
}