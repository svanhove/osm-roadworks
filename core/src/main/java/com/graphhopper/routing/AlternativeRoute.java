/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.routing;

import com.graphhopper.routing.AStar.AStarEntry;
import com.graphhopper.routing.ch.Path4CH;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.util.WeightedEdgeFilter;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.SPTEntry;
import com.graphhopper.util.CHEdgeIterator;
import com.graphhopper.util.CHEdgeIteratorState;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.GHUtility;
import com.graphhopper.util.Parameters;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TObjectProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the alternative paths search using the "plateau" and partially the
 * "penalty" method discribed in the following papers.
 * <p>
 * <ul>
 * <li>Choice Routing Explanation - Camvit 2009:
 * http://www.camvit.com/camvit-technical-english/Camvit-Choice-Routing-Explanation-english.pdf</li>
 * <li>and refined in: Alternative Routes in Road Networks 2010:
 * http://www.cs.princeton.edu/~rwerneck/papers/ADGW10-alternatives-sea.pdf</li>
 * <li>other ideas 'Improved Alternative Route Planning', 2013:
 * https://hal.inria.fr/hal-00871739/document</li>
 * <li>via point 'storage' idea 'Candidate Sets for Alternative Routes in Road Networks', 2013:
 * https://algo2.iti.kit.edu/download/s-csarrn-12.pdf</li>
 * <li>Alternative route graph construction 2011:
 * http://algo2.iti.kit.edu/download/altgraph_tapas_extended.pdf
 * </li>
 * </ul>
 *
 * @author Peter Karich
 */
public class AlternativeRoute implements RoutingAlgorithm {
    private final Logger logger = LoggerFactory.getLogger(getClass());


    private final Graph graph;
    private final FlagEncoder flagEncoder;
    private final Weighting weighting;
    private final TraversalMode traversalMode;
    private int visitedNodes;
    private int maxVisitedNodes = Integer.MAX_VALUE;
    private double maxWeightFactor = 1.4;
    // the higher the maxWeightFactor the higher the explorationFactor needs to be
    // 1 is default for bidir Dijkstra, 0.8 seems to be a very similar value for bidir A* but roughly 1/2 of the nodes explored
    private double maxExplorationFactor = 0.8;
    private double maxShareFactor = 0.6;
    private double minPlateauFactor = 0.2;
    private int maxPaths = 2;
    private EdgeFilter edgeFilter;

    public AlternativeRoute(Graph graph, Weighting weighting, TraversalMode traversalMode) {
        this.graph = graph;
        this.flagEncoder = weighting.getFlagEncoder();
        this.weighting = weighting;
        this.traversalMode = traversalMode;
    }
    public void setEdgeFilter(EdgeFilter edgeFilter){
        this.edgeFilter=edgeFilter;
    }
    

    static double calcSortBy(double weightInfluence, double weight,
                             double shareInfluence, double shareWeight,
                             double plateauInfluence, double plateauWeight) {
        return weightInfluence * weight + shareInfluence * shareWeight + plateauInfluence * plateauWeight;
    }

    @Override
    public void setMaxVisitedNodes(int numberOfNodes) {
        this.maxVisitedNodes = numberOfNodes;
    }

    /**
     * Increasing this factor results in returning more alternatives. E.g. if the factor is 2 than
     * all alternatives with a weight 2 times longer than the optimal weight are return. (default is
     * 1)
     */
    public void setMaxWeightFactor(double maxWeightFactor) {
        this.maxWeightFactor = maxWeightFactor;
    }

    /**
     * This parameter is used to avoid alternatives too similar to the best path. Specify 0.5 to
     * force a same paths of maximum 50%. The unit is the 'weight' returned in the Weighting.
     */
    public void setMaxShareFactor(double maxShareFactor) {
        this.maxShareFactor = maxShareFactor;
    }

    /**
     * This method sets the minimum plateau portion of every alternative path that is required.
     */
    public void setMinPlateauFactor(double minPlateauFactor) {
        this.minPlateauFactor = minPlateauFactor;
    }

    /**
     * This method sets the graph exploration percentage for alternative paths. Default is 1 (100%).
     * Specify a higher value to get more alternatives (especially if maxWeightFactor is higher than
     * 1.5) and a lower value to improve query time but reduces the possibility to find
     * alternatives.
     */
    public void setMaxExplorationFactor(double explorationFactor) {
        this.maxExplorationFactor = explorationFactor;
    }

    /**
     * Specifies how many paths (including the optimal) are returned. (default is 2)
     */
    public void setMaxPaths(int maxPaths) {
        this.maxPaths = maxPaths;
        if (this.maxPaths < 2)
            throw new IllegalStateException("Use normal algorithm with less overhead instead if no alternatives are required");
    }

    /**
     * This method calculates best paths (alternatives) between 'from' and 'to', where maxPaths-1
     * alternatives are searched and they are only accepted if they are not too similar but close to
     * the best path.
     */
    public List<AlternativeInfo> calcAlternatives(int from, int to) {
        AlternativeBidirSearch altBidirDijktra = new AlternativeBidirSearch(
                graph, weighting, traversalMode, maxExplorationFactor * 2);
        altBidirDijktra.setMaxVisitedNodes(maxVisitedNodes);
        altBidirDijktra.setEdgeFilter(edgeFilter);
        altBidirDijktra.searchBest(from, to);
        visitedNodes = altBidirDijktra.getVisitedNodes();
        
        List<AlternativeInfo> alternatives = altBidirDijktra.
                calcAlternatives(maxPaths, maxWeightFactor, 7, maxShareFactor, 0.8, minPlateauFactor, -0.2);
        logger.info("Done calculating alternatives with "+alternatives.size());
        return alternatives;
    }

    @Override
    public Path calcPath(int from, int to) {
        return calcPaths(from, to).get(0);
    }

    @Override
    public List<Path> calcPaths(int from, int to) {
        List<AlternativeInfo> alts = calcAlternatives(from, to);
        List<Path> paths = new ArrayList<Path>(alts.size());
        for (AlternativeInfo a : alts) {
            paths.add(a.getPath());
        }
        return paths;
    }

    @Override
    public String getName() {
        return Parameters.Algorithms.ALT_ROUTE;
    }

    @Override
    public int getVisitedNodes() {
        return visitedNodes;
    }

}
