package com.graphhopper.routing;

import com.graphhopper.storage.SPTEntry;

/**
 *
 * @author Maarten
 */
public  class AlternativeSPTEntry extends SPTEntry {
    protected double weightOffOriginal;
    protected int nrHopsOfOriginal;
    protected double weightOnOriginal;
    private AlternativeSPTEntry parentSPTOnOptimal;
    protected boolean verifiedSPT=false;

    public AlternativeSPTEntry(AStar.AStarEntry a) {
//        this(a.edge,a.adjNode,a.weight,
        this(a.edge,a.adjNode,
                0,a.weightOfVisitedPath,0);
    }
    public AlternativeSPTEntry(int edgeId, int adjNode, 
              double weightOffOriginal,double weightOnOriginal,int nrHopsOfOriginal) {
//    public AlternativeSPTEntry(int edgeId, int adjNode, double weightForHeap, 
//              double weightOffOriginal,double weightOnOriginal,int nrHopsOfOriginal) {
        super(edgeId, adjNode, -1);
//        super(edgeId, adjNode, weightForHeap);
        this.weightOffOriginal = weightOffOriginal;
        this.nrHopsOfOriginal = nrHopsOfOriginal;
        this.weightOnOriginal=weightOnOriginal;
    }
    public void setParentSPTOnOptimal(AlternativeSPTEntry parentSPTOnOptimal){
        this.setParentSPTOnOptimal(parentSPTOnOptimal,true);
    }
    public void setParentSPTOnOptimal(AlternativeSPTEntry parentSPTOnOptimal,boolean debug){
        this.parentSPTOnOptimal=parentSPTOnOptimal;
    }
    public AlternativeSPTEntry getParentSPTOnOptimal(){
        return parentSPTOnOptimal;
    }
    public final double getWeightOffOriginal() {
        return weightOffOriginal;
    }
    public final int getNrHopsOfOriginal(){
        return nrHopsOfOriginal;
    }

    public double getWeightOnOriginal() {
        return weightOnOriginal;
    }
    @Override
    public double getWeightOfVisitedPath() {
        return weightOnOriginal+weightOffOriginal;
    }

    protected void update(AStar.AStarEntry prev) {
        this.edge=prev.edge;
        this.adjNode=prev.adjNode;
        this.weight=prev.weight;
        this.weightOffOriginal=0;
        this.weightOnOriginal=prev.weightOfVisitedPath;
        this.nrHopsOfOriginal=0;
    }
}
