/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for
 *  additional information regarding copyright ownership.
 *
 *  GraphHopper GmbH licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.routing;


import com.graphhopper.reader.osm.construction.KmlHelper;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.SPTEntry;
import com.graphhopper.util.EdgeIteratorState;


import java.util.*;

// Implements a one to all Dijkstra algorithm within a certain radius or until the entire component of the network is reached.
public class OneToAllDijkstra extends Dijkstra {


    public OneToAllDijkstra(Graph graph, Weighting weighting, double maxWeight, boolean backward) {
        super(graph, weighting, TraversalMode.NODE_BASED, backward);
        this.maxWeight = maxWeight;
    }


    public Path run(int from) {
        return super.calcPath(from, -1);
    }

    @Override
    protected boolean finished() {
        return false;
    }


    public void writeVisitedEdgesToKml(String filename) {
        Set<EdgeIteratorState> edgeIteratorStatesForKML = new HashSet<>();
        for(SPTEntry sptEntry : fromMap.valueCollection()) {
            int edge = sptEntry.edge;
            if(edge != -1) {
                EdgeIteratorState edgeIteratorState = graph.getEdgeIteratorState(edge, Integer.MIN_VALUE);
                edgeIteratorStatesForKML.add(edgeIteratorState);
            }
        }
        KmlHelper.writeEdgeIteratorStates(edgeIteratorStatesForKML, graph, filename);
    }

    public Collection<Integer> getReachedNodes(){
        HashSet<Integer> result = new HashSet<>();
        for(int node: fromMap.keys())
            result.add(node);
        return result;
    }

    public Collection<SPTEntry> getReachedSPTEntries(){
        return fromMap.valueCollection();
    }

    public double getFurthestDistance() {
        return currEdge.getWeightOfVisitedPath();
    }
}
