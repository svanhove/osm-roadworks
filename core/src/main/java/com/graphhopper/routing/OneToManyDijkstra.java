package com.graphhopper.routing;

import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;

import java.util.Set;

/**
 * Created by Stéphanie Vanhove on 30/04/2018.
 */
public class OneToManyDijkstra extends OneToAllDijkstra {

    private Set<Integer> targetNodes;

    public OneToManyDijkstra(Graph graph, Weighting weighting, Set<Integer> targetNodes, double maxWeight, boolean backward) {
        super(graph, weighting, maxWeight, backward);
        this.targetNodes = targetNodes;
    }


    @Override
    protected boolean finished() {
        boolean finished = fromMap.keySet().containsAll(targetNodes);
        return finished;
    }
}
