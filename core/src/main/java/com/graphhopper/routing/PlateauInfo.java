package com.graphhopper.routing;

import java.util.List;

public class PlateauInfo {
    String name;
    List<Integer> edges;

    public PlateauInfo(String name, List<Integer> edges) {
        this.name = name;
        this.edges = edges;
    }

    @Override
    public String toString() {
        return name;
    }

    public List<Integer> getEdges() {
        return edges;
    }

    public String getName() {
        return name;
    }
}