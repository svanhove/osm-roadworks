package com.graphhopper.routing;

import gnu.trove.map.TIntObjectMap;
import java.util.PriorityQueue;

/**
 *
 * @author Maarten
 */
class QueueMapCouple{
    protected PriorityQueue<AlternativeSPTEntry> queue;
    protected TIntObjectMap<AlternativeSPTEntry> map;

    protected QueueMapCouple(PriorityQueue<AlternativeSPTEntry> queue, TIntObjectMap<AlternativeSPTEntry> map) {
        this.queue=queue;
        this.map=map;
    }
        
}