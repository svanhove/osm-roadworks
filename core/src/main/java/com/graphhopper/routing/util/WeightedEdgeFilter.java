package com.graphhopper.routing.util;

import com.graphhopper.routing.AStar;
import com.graphhopper.routing.weighting.FastestWeighting;
import com.graphhopper.routing.weighting.ShortestWeighting;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.util.EdgeIteratorState;
import gnu.trove.map.TIntDoubleMap;
import gnu.trove.map.hash.TIntDoubleHashMap;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class WeightedEdgeFilter {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private TIntDoubleMap maxDistancePerType;
    public int skipped=0;
    private WeightedEdgeFilter(){
//        initTypeMapping();
    }
    public WeightedEdgeFilter(TIntDoubleMap map){
        this();
        maxDistancePerType=map;
    }
    public WeightedEdgeFilter (String weighting){
        this();
        if(weighting.equalsIgnoreCase("fastest"))maxDistancePerType=getDefaultTravelTimeMap();
        else if(weighting.equalsIgnoreCase("shortest"))maxDistancePerType=getDefaultDistanceMap();
        else maxDistancePerType=new TIntDoubleHashMap();
    }
    public WeightedEdgeFilter (Weighting weighting){
        this();
        if(weighting instanceof FastestWeighting)maxDistancePerType=getDefaultTravelTimeMap();
        else if(weighting instanceof ShortestWeighting)maxDistancePerType=getDefaultDistanceMap();
        else maxDistancePerType=new TIntDoubleHashMap();
    }
    public boolean accept(EdgeIteratorState eis,AStar.AStarEntry entry){
        int type=eis.getType();
        if(!maxDistancePerType.containsKey(type))return true;
        double max=maxDistancePerType.get(type);
        double pathWeight=entry.getWeightOfVisitedPath();
        if(pathWeight>max){
//            logger.info("Skipping type "+reverseTypeMapping.get(type)+" as "+max+"<"+pathWeight);
            skipped++;
            return false; 
        }
        return true;
    }
    private static double minutesInLocalRoads=20;
    private static double hoursInCountyRoads=6;
    private static double factor3=6;
    private static TIntDoubleMap getDefaultTravelTimeMap(){
        TIntDoubleMap map=new TIntDoubleHashMap();
//        map.put(9, 3600*factor3);//trunk
//        map.put(10, 3600*factor3);//trunk_link
//        map.put(3, 3600*factor2);//primary
//        map.put(4, 3600*factor2);//primary_link
        map.put(5, 3600*hoursInCountyRoads);//secondary
        map.put(6, 3600*hoursInCountyRoads);//secondary_link
        map.put(7, 3600*hoursInCountyRoads);//tertiary
        map.put(8, 3600*hoursInCountyRoads);//tertiary_link
        map.put(11, 180*minutesInLocalRoads);//unclassified
        map.put(12, 60*minutesInLocalRoads);//residential
        map.put(13, 60*minutesInLocalRoads);//service
        map.put(14, 60*minutesInLocalRoads);//living_street
        map.put(15, 60*minutesInLocalRoads);//pedestrian
        map.put(16, 60*minutesInLocalRoads);//track
        map.put(17, 60*minutesInLocalRoads);//bus_guideway
        map.put(18, 60*minutesInLocalRoads);//escape
        map.put(19, 60*minutesInLocalRoads);//road
        map.put(20, 60*minutesInLocalRoads);//footway
        map.put(21, 60*minutesInLocalRoads);//bridleway
        map.put(22, 60*minutesInLocalRoads);//steps
        map.put(23, 60*minutesInLocalRoads);//path
        map.put(24, 60*minutesInLocalRoads);//cycleway
        
        return map;
    }
    private static TIntDoubleMap getDefaultDistanceMap(){
        TIntDoubleMap map=new TIntDoubleHashMap();
        map.put(3, 50000);//primary
        map.put(4, 50000);//primary_link
        map.put(5, 20000);//secondary
        map.put(6, 20000);//secondary_link
        map.put(7, 15000);//tertary
        map.put(8, 15000);//tertiary_link
        map.put(9, 10000);//trunk
        map.put(10, 10000);//trunk_link
        map.put(11, 10000);//unclassified
        map.put(12, 5000);//residential
        map.put(13, 5000);//service
        map.put(14, 2000);//living_street
        map.put(15, 2000);//pedestrian
        map.put(16, 2000);//track
        map.put(17, 2000);//bus_guideway
        map.put(18, 2000);//escape
        map.put(19, 5000);//road
        map.put(20, 2000);//footway
        map.put(21, 2000);//bridleway
        map.put(22, 2000);//steps
        map.put(23, 2000);//path
        map.put(24, 2000);//cycleway
        
        return map;
    }
//    private static Map<String,Integer> typeMapping;
//    private static Map<Integer,String> reverseTypeMapping;
//    private static void initTypeMapping(){
//        typeMapping=new HashMap<>();
//        reverseTypeMapping=new HashMap<>();
//        registerTypeMapping("motorway",1);
//        registerTypeMapping("motorway_link",2);
//        registerTypeMapping("primary",3);
//        registerTypeMapping("primary_link",4);
//        registerTypeMapping("secondary",5);
//        registerTypeMapping("secondary_link",6);
//        registerTypeMapping("tertiary",7);
//        registerTypeMapping("tertiary_link",8);
//        registerTypeMapping("trunk",9);
//        registerTypeMapping("trunk_link",10);
//        registerTypeMapping("unclassified",11);
//        registerTypeMapping("residential",12);
//        registerTypeMapping("service",13);
//        registerTypeMapping("living_street",14);
//        registerTypeMapping("pedestrian",15);
//        registerTypeMapping("track",16);
//        registerTypeMapping("bus_guideway",17);
//        registerTypeMapping("escape",18);
//        registerTypeMapping("road",19);
//        registerTypeMapping("footway",20);
//        registerTypeMapping("bridleway",21);
//        registerTypeMapping("steps",22);
//        registerTypeMapping("path",23);
//        registerTypeMapping("cycleway",24);
//        registerTypeMapping("",100);
//        registerTypeMapping(null,404);
//    }
//    private static void registerTypeMapping(String type, int index){
//        typeMapping.put(type,index);
//        reverseTypeMapping.put(index,type);
//    }
//    public static int getRoadTypeIndexForRoadType(String roadType){
//        if(typeMapping==null){
//            initTypeMapping();
//        }
//        Integer ri= typeMapping.get(roadType);
//        if(ri==null)return -1;
//        return ri;
//    }
//    public static String getRoadTypeForRoadTypeIndex(int roadTypeIndex){
//        if(reverseTypeMapping==null){
//            initTypeMapping();
//        }
//        return reverseTypeMapping.get(roadTypeIndex);
//    }

    public void printSummary() {
        logger.info("Skipped "+skipped+ " edges");
    }
}
