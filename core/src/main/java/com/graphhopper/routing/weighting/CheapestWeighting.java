package com.graphhopper.routing.weighting;

import com.graphhopper.routing.util.FlagEncoder;
import static com.graphhopper.routing.weighting.FastestWeighting.SPEED_CONV;
import com.graphhopper.util.EdgeIteratorState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class CheapestWeighting extends AbstractWeighting {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static double costBenzinePerKm=.36;
    private static double costDriverPerHour=36;
    private static double minSpeedInKmPerHour=30;

    public CheapestWeighting(FlagEncoder encoder) {
        super(encoder);
        logger.info("Using cheapestWeighting");
    }
    @Override
    public double getMinWeight(double distance) {//in meter
        //assume lowest driving speed 30 km/hour  
        //benzineprijs
        double distanceInKm=distance/1000.0;
        double benzineBijdrage=costBenzinePerKm*distanceInKm;
        double reistijd=costDriverPerHour*distanceInKm/minSpeedInKmPerHour;//1 cent per sec
//        double reistijd=(costDriverPerHour/3600.0)*distance/1000.0/minSpeedInKmPerHour*3600;//1 cent per sec
        return benzineBijdrage+reistijd;   
    }

    @Override
    public double calcWeight(EdgeIteratorState edge, boolean reverse, int prevOrNextEdgeId) {
        double speed = reverse ? flagEncoder.getReverseSpeed(edge.getFlags()) : flagEncoder.getSpeed(edge.getFlags());
        if (speed == 0)
            return Double.POSITIVE_INFINITY;
        double distanceInKm=edge.getDistance()/1000.0;
        double benzineBijdrage=costBenzinePerKm*distanceInKm;
        double reistijd=costDriverPerHour*distanceInKm/speed;
        return benzineBijdrage+reistijd;
    }

    @Override
    public String getName() {
        return "cheapest";
    }

}
