package com.graphhopper.routing.weighting;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.HintsMap;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.los.LinkSegmentation;
import com.graphhopper.util.los.TrafficDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class DelayWeighting implements Weighting{
    private static final Logger logger = LoggerFactory.getLogger(DelayWeighting.class);

    private final Weighting wrappedWeigthing;
    private final TrafficDataSet tds;
    
    public DelayWeighting(Weighting w,TrafficDataSet tds){
        this.wrappedWeigthing=w;
        this.tds=tds;
    }
    @Override
    public double getMinWeight(double distance) {
        return wrappedWeigthing.getMinWeight(distance);
    }

    @Override
    public double calcWeight(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
        double w=wrappedWeigthing.calcWeight(edgeState, reverse, prevOrNextEdgeId);
        return w+getDelay(edgeState, reverse);
    }

    @Override
    public long calcMillis(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
        long millis=wrappedWeigthing.calcMillis(edgeState, reverse, prevOrNextEdgeId);
        return millis+getDelay(edgeState, reverse);
    }
    private long getDelay(EdgeIteratorState edgeState, boolean reverse){
//        logger.info("Called getDelay");
        boolean dir=(edgeState.getAdjNode()>edgeState.getBaseNode())^!reverse;
        LinkSegmentation linkSegmentation = tds.getLinkSegmentation(edgeState.getEdge(), dir);
        if(linkSegmentation!=null){
            int delay=linkSegmentation.getTotalDelayInMsOnLink();
//            logger.info("Found "+delay+" on "+edgeState.getEdge());
            return delay;
        }else return 0;
    }
    @Override
    public FlagEncoder getFlagEncoder() {
        return wrappedWeigthing.getFlagEncoder();
    }

    @Override
    public String getName() {
        return "delay";
    }

    @Override
    public boolean matches(HintsMap map) {
        return wrappedWeigthing.matches(map);
    }

    public TrafficDataSet getActiveTrafficDataSet() {
        return tds;
    }
    

}
