/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.search;

import com.graphhopper.storage.DataAccess;
import com.graphhopper.storage.Directory;
import com.graphhopper.storage.Storable;
import com.graphhopper.util.BitUtil;
import com.graphhopper.util.Helper;
import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ottavio Campana
 * @author Peter Karich
 */
public class CountryCodeIndex implements Storable<CountryCodeIndex> {
    private static final Logger logger = LoggerFactory.getLogger(CountryCodeIndex.class);
    private static final long START_POINTER = 1;
    private final DataAccess countryCodes;
//    private TIntObjectMap<String> ccIDToFullName;
    private TObjectLongMap<String> fullNameToCCID;
    private long bytePointer = START_POINTER;
    // minor optimization for the previous stored name
//    private String lastCountryCode;
//    private long lastIndex;

    public CountryCodeIndex(Directory dir) {
        countryCodes = dir.find("countrycodes");
        fullNameToCCID=new TObjectLongHashMap<>();
    }

    @Override
    public CountryCodeIndex create(long cap) {
        countryCodes.create(cap);
        return this;
    }

    @Override
    public boolean loadExisting() {
        if (countryCodes.loadExisting()) {
            bytePointer = BitUtil.LITTLE.combineIntsToLong(countryCodes.getHeader(0), countryCodes.getHeader(4));
            return true;
        }

        return false;
    }

    /**
     * @return the byte pointer to the name
     */
    public long put(String countryCode) {
        if (countryCode == null || countryCode.isEmpty()) {
            return 0;
        }
        if (fullNameToCCID.containsKey(countryCode)) {
            return fullNameToCCID.get(countryCode);
        }
        byte[] bytes = getBytes(countryCode);
        long oldPointer = bytePointer;
        countryCodes.ensureCapacity(bytePointer + 1 + bytes.length);
        byte[] sizeBytes = new byte[]{
            (byte) bytes.length
        };
        countryCodes.setBytes(bytePointer, sizeBytes, sizeBytes.length);
        bytePointer++;
        countryCodes.setBytes(bytePointer, bytes, bytes.length);
        bytePointer += bytes.length;
//        lastCountryCode = countryCode;
//        lastIndex = oldPointer;
        return oldPointer;
    }

    private byte[] getBytes(String countryCode) {
        byte[] bytes = null;
        for (int i = 0; i < 2; i++) {
            bytes = countryCode.getBytes(Helper.UTF_CS);
            // we have to store the size of the array into *one* byte
            if (bytes.length > 255) {
                String newCode = countryCode.substring(0, 256 / 4);
                logger.info("Countrycode is too long: " + countryCode + " truncated to " + newCode);
                countryCode = newCode;
                continue;
            }
            break;
        }
        if (bytes.length > 255) {
            // really make sure no such problem exists
            throw new IllegalStateException("Countrycode is too long: " + countryCode);
        }
        return bytes;
    }

    public String get(long pointer) {
        if (pointer < 0)
            throw new IllegalStateException("Pointer to access NameIndex cannot be negative:" + pointer);

        // default
        if (pointer == 0)
            return "";

        byte[] sizeBytes = new byte[1];
        countryCodes.getBytes(pointer, sizeBytes, 1);
        int size = sizeBytes[0] & 0xFF;
        byte[] bytes = new byte[size];
        countryCodes.getBytes(pointer + sizeBytes.length, bytes, size);
        return new String(bytes, Helper.UTF_CS);
    }

    @Override
    public void flush() {
        countryCodes.setHeader(0, BitUtil.LITTLE.getIntLow(bytePointer));
        countryCodes.setHeader(4, BitUtil.LITTLE.getIntHigh(bytePointer));
        countryCodes.flush();
    }

    @Override
    public void close() {
        countryCodes.close();
    }

    @Override
    public boolean isClosed() {
        return countryCodes.isClosed();
    }

    public void setSegmentSize(int segments) {
        countryCodes.setSegmentSize(segments);
    }

    @Override
    public long getCapacity() {
        return countryCodes.getCapacity();
    }

    public void copyTo(CountryCodeIndex nameIndex) {
        countryCodes.copyTo(nameIndex.countryCodes);
    }
}
