/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.storage.index;

import com.graphhopper.storage.index.QueryResult.Position;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.GHPoint;
import com.graphhopper.util.shapes.GHPoint3D;

/**
 * Result of LocationIndex lookup.
 * <pre> X=query coordinates S=snapped coordinates: "snapping" real coords to road N=tower or pillar
 * node T=closest tower node XS=distance
 * X
 * |
 * T--S----N
 * </pre>
 * <p>
 *
 * @author Peter Karich
 */
public class MultiQueryResult implements QueryResultInterface {
    QueryResult [] qr;
    private final GHPoint queryPoint;


//    private final GHPoint queryPoint;
//    private double queryDistance = Double.MAX_VALUE;
//    private int wayIndex = -1;
//    private int closestNode = -1;
//    private EdgeIteratorState closestEdge;
//    private GHPoint3D snappedPoint;
//    private Position snappedPosition;

    public MultiQueryResult(double queryLat, double queryLon) {
        this(queryLat,queryLon,1);
    }

    public MultiQueryResult(double queryLat, double queryLon, int nrConnections){
        queryPoint = new GHPoint(queryLat, queryLon);
        qr=new QueryResult[nrConnections];
    }

    /**
     * @return the closest matching node. -1 if nothing found, this should be avoided via a call of
     * 'isValid'
     */
    public int getClosestNode() {
        return qr[0].getClosestNode();
    }

    public void setClosestNode(int node) {
        qr[0].setClosestNode(node);    
    }

    /**
     * @return the distance of the query to the snapped coordinates. In meter
     */
    public double getQueryDistance() {
        return qr[0].getQueryDistance();
    }

    public void setQueryDistance(double dist) {
        qr[0].setQueryDistance(dist);
    }

    /**
     * References to a tower node or the index of wayGeometry of the closest edge. If wayGeometry
     * has length L then the wayIndex 0 refers to the *base* node, 1 to L (inclusive) refer to the
     * wayGeometry indices (minus one) and L+1 to the *adjacent* node. Currently only initialized if
     * returned from Location2NodesNtree.
     */
    public int getWayIndex() {
        return qr[0].getWayIndex();
    }

    public void setWayIndex(int wayIndex) {
        qr[0].setWayIndex(wayIndex);
    }

    /**
     * @return 0 if on edge. 1 if on pillar node and 2 if on tower node.
     */
    public Position getSnappedPosition() {
        return qr[0].getSnappedPosition();
    }

    public void setSnappedPosition(Position pos) {
        qr[0].setSnappedPosition(pos);
    }

    /**
     * @return true if a close node was found
     */
    public boolean isValid() {
        return qr[0]!=null&&qr[0].isValid();
    }

    /**
     * @return the closest matching edge. Will be null if nothing found or call isValid before
     */
    public EdgeIteratorState getClosestEdge() {
        return qr[0].getClosestEdge();
    }

    public void setClosestEdge(EdgeIteratorState detach) {
        qr[0].setClosestEdge(detach);
    }

    public GHPoint getQueryPoint() {
        return qr[0].getQueryPoint();
    }

    /**
     * Calculates the position of the query point 'snapped' to a close road segment or node. Call
     * calcSnappedPoint before, if not, an IllegalStateException is thrown.
     */
    public GHPoint3D getSnappedPoint() {
        return qr[0].getSnappedPoint();
//        if (snappedPoint == null)
//            throw new IllegalStateException("Calculate snapped point before!");
//        return snappedPoint;
    }

    /**
     * Calculates the closet point on the edge from the query point.
     */
    public void calcSnappedPoint(DistanceCalc distCalc) {
        qr[0].calcSnappedPoint(distCalc);
//        if (closestEdge == null)
//            throw new IllegalStateException("No closest edge?");
//        if (snappedPoint != null)
//            throw new IllegalStateException("Calculate snapped point only once");
//
//        PointList fullPL = getClosestEdge().fetchWayGeometry(3);
//        double tmpLat = fullPL.getLatitude(wayIndex);
//        double tmpLon = fullPL.getLongitude(wayIndex);
//        double tmpEle = fullPL.getElevation(wayIndex);
//        if (snappedPosition != Position.EDGE) {
//            snappedPoint = new GHPoint3D(tmpLat, tmpLon, tmpEle);
//            return;
//        }
//
//        double queryLat = getQueryPoint().lat, queryLon = getQueryPoint().lon;
//        double adjLat = fullPL.getLatitude(wayIndex + 1), adjLon = fullPL.getLongitude(wayIndex + 1);
//        if (distCalc.validEdgeDistance(queryLat, queryLon, tmpLat, tmpLon, adjLat, adjLon)) {
//            GHPoint tmpPoint = distCalc.calcCrossingPointToEdge(queryLat, queryLon, tmpLat, tmpLon, adjLat, adjLon);
//            double adjEle = fullPL.getElevation(wayIndex + 1);
//            snappedPoint = new GHPoint3D(tmpPoint.lat, tmpPoint.lon, (tmpEle + adjEle) / 2);
//        } else
//            // outside of edge boundaries
//            snappedPoint = new GHPoint3D(tmpLat, tmpLon, tmpEle);
    }

    @Override
    public String toString() {
        return qr[0].toString();
//        if (closestEdge != null)
//            return closestEdge.getBaseNode() + "-" + closestEdge.getAdjNode() + "  " + snappedPoint;
//        return closestNode + ", " + queryPoint + ", " + wayIndex;
    }

    void updateWithNewMatch(double normedDist, int node, EdgeIteratorState detach, int wayIndex, Position pos) {
        int index=0;
        for(int i=0;i<qr.length;i++){
            if(qr[i]==null||qr[i].getQueryDistance()>normedDist){
                index=i;
                break;
            }
        }
        if(index==qr.length)return;
        QueryResult newQr=new QueryResult(this.queryPoint.lat, this.queryPoint.lon);
        newQr.setQueryDistance(normedDist);
        newQr.setClosestNode(node);
        newQr.setClosestEdge(detach);
        newQr.setWayIndex(wayIndex);
        newQr.setSnappedPosition(pos);
        for(int j=index;j<qr.length;j++){
            QueryResult qrr=qr[j];
            qr[j]=newQr;
            newQr=qrr;
        }
        
    }

    double getWorstQueryDistance() {
        for(int i=qr.length-1;i>=0;i--){
            if(qr[i]!=null)return qr[i].getQueryDistance();
        }
        return Double.MAX_VALUE;
    }

    void denormalizeDist(DistanceCalc distCalc) {
        for(int i=0;i<qr.length;i++){
            if(qr[i]==null)return;
            qr[i].setQueryDistance(distCalc.calcDenormalizedDist(this.getQueryDistance()));
        }
    }

    void calcSnappedPoints(DistanceCalc distCalc) {
        for(int i=0;i<qr.length;i++){
            if(qr[i]==null)return;
            qr[i].calcSnappedPoint(distCalc);
        }
    }

}
