package com.graphhopper.util.los;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.EdgeIteratorState;

import com.graphhopper.util.PointList;
import java.util.Collection;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author Maarten
 */
public class LOSEdgeIteratorState implements EdgeIteratorState{

    PointList pointList;
    int LOS;
    double delayInMs;
    Set<JSONObject>activeEvents;
    int edgeID;
    
    public LOSEdgeIteratorState(PointList pl, int LOS,double delayInMs,Set<JSONObject>activeEvents,int edgeID){
        this.pointList=pl;
        this.LOS=LOS;
        this.delayInMs=delayInMs;
        this.activeEvents=activeEvents;
        this.edgeID=edgeID;
    }
    @Override
    public int getEdge() {
        return edgeID;
    }

    @Override
    public int getBaseNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getAdjNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PointList fetchWayGeometry(int mode) {
        if (pointList.getSize() == 0)
            return PointList.EMPTY;
        // due to API we need to create a new instance per call!
        if (mode == 3)
            return pointList.clone(false);
        else if (mode == 1)
            return pointList.copy(0, pointList.getSize() - 1);
        else if (mode == 2)
            return pointList.copy(1, pointList.getSize());
        else if (mode == 0) {
            if (pointList.getSize() == 1)
                return PointList.EMPTY;
            return pointList.copy(1, pointList.getSize() - 1);
        }
        throw new UnsupportedOperationException("Illegal mode:" + mode);
    }

    @Override
    public EdgeIteratorState setWayGeometry(PointList list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getDistance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }
    public double getDistance(DistanceCalc distCalculator) {
        return pointList.calcDistance(distCalculator);
    }

    @Override
    public EdgeIteratorState setDistance(double dist) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long getFlags() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState setFlags(long flags) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getAdditionalField() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState setAdditionalField(int value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isForward(FlagEncoder encoder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isBackward(FlagEncoder encoder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getBool(int key, boolean _default) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState setName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState detach(boolean reverse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState copyPropertiesTo(EdgeIteratorState e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState setType(int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getLOS() {
        return LOS;
    }

    public double getDelayInMs() {
        return delayInMs;
    }

    public Collection<? extends JSONObject> getActiveEvents() {
        return activeEvents;
    }

    @Override
    public String getCountryCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EdgeIteratorState setCountryCode(String cc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
