package com.graphhopper.util.los;

import java.util.HashSet;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author Maarten
 */
public class LinkSegmentState {
    int startOffsetInMm;
    int endOffsetInMm;
    Set<JSONObject> activeEvents;
    int LOS=-1;
    int delayInMs=0;

    LinkSegmentState(LinkSegmentState lss) {
        startOffsetInMm=lss.startOffsetInMm;
        endOffsetInMm=lss.endOffsetInMm;
        LOS=lss.LOS;
        delayInMs=lss.delayInMs;
        activeEvents=new HashSet<>(lss.activeEvents);
    }

    public int getStartOffsetInMm() {
        return startOffsetInMm;
    }

    public void setStartOffsetInMm(int startOffsetInMm) {
        this.startOffsetInMm = startOffsetInMm;
    }

    public int getEndOffsetInMm() {
        return endOffsetInMm;
    }

    public void setEndOffsetInMm(int endOffsetInMm) {
        this.endOffsetInMm = endOffsetInMm;
    }

    public int getLOS() {
        return LOS;
    }

    public void setLOS(int LOS) {
        this.LOS = LOS;
    }
    public LinkSegmentState(){
        activeEvents=new HashSet<>();
    }

    LinkSegmentState(int startOffsetInMm, int endOffsetInMm) {
        this.startOffsetInMm=startOffsetInMm;
        this.endOffsetInMm=endOffsetInMm;
        activeEvents=new HashSet<>();
    }

    void addEvent(JSONObject event) {
        activeEvents.add(event);
    }

    LinkSegmentState copy() {
        LinkSegmentState ls=new LinkSegmentState(this.startOffsetInMm, this.endOffsetInMm);
        ls.LOS=this.LOS;
        ls.delayInMs=delayInMs;
        ls.activeEvents.addAll(this.activeEvents);
        return ls;
    }

    void updateLinkSegmentState(JSONObject event, int LOS,int delayInMs) {
        activeEvents.add(event);
        if(LOS!=-1)this.LOS=LOS;
        if(delayInMs!=-1)this.delayInMs=delayInMs;
    }
    void updateLinkSegmentState(Set<JSONObject> events, int LOS,int delayInMs) {
        activeEvents.addAll(events);
        if(LOS!=-1)this.LOS=LOS;
        if(delayInMs!=-1)this.delayInMs=delayInMs;
    }

    public int getDelayInMs() {
        return delayInMs;
    }
    public Set<JSONObject> getActiveEvents(){
        return activeEvents;
    }
    
}
