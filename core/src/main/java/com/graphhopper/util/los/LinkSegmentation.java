package com.graphhopper.util.los;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class LinkSegmentation {
    protected static final Logger logger = LoggerFactory.getLogger(LinkSegmentation.class);

    static LinkSegmentation merge(LinkSegmentation r, LinkSegmentation seg) {
        logger.info("Called merge on linksegmentation");
        LinkSegmentation ls=new LinkSegmentation(r);
        for (LinkSegmentState lss : seg.statesOnLink) {
            ls.addEvent(lss.activeEvents,lss.startOffsetInMm,lss.endOffsetInMm,lss.LOS,lss.delayInMs);
        }
        return ls;
    }

    List<LinkSegmentState> statesOnLink;
    int lengthInMM;

    public LinkSegmentation(LinkSegmentation ls){
        statesOnLink=new ArrayList<>(ls.statesOnLink.size());
        this.lengthInMM=ls.lengthInMM;
        for (LinkSegmentState lss : ls.statesOnLink) {
            statesOnLink.add(new LinkSegmentState(lss));
        }
    }

    public LinkSegmentation(int lengthInMM){
        statesOnLink=new ArrayList<>();
        this.lengthInMM=lengthInMM;
        LinkSegmentState initial=new LinkSegmentState(0,lengthInMM);
        statesOnLink.add(initial);
    }
    public void addEvent(JSONObject event, int startOffsetInMm, int endOffsetInMm,int LOS,int delayInMs) {
        //to add the event we start from a list of active events that always contains at least one part (=full link)
        //we look until we find the first part that has some overlap with the event
        //that part will be split and the furthest part will be given the event
        //then we should add the current event to all subsequent part until we find one that ends after the event
        //that part is split as well and the first part is given the new event
//        logger.info("Event received");
        int endOffSetInMM=endOffsetInMm;
        int startOffsetInMM=startOffsetInMm;
        if(startOffsetInMM>lengthInMM)return;
        if(endOffSetInMM>lengthInMM)endOffSetInMM=lengthInMM;
        if(endOffSetInMM<=startOffsetInMM)return;
        LinkSegmentState l=statesOnLink.get(0);
        int currentIndex=0;
        while(l.endOffsetInMm<=startOffsetInMM){
            l=statesOnLink.get(currentIndex+1);
            currentIndex++;
        }
        if(l.startOffsetInMm!=startOffsetInMM){
            LinkSegmentState newl=l.copy();
            newl.setStartOffsetInMm(startOffsetInMM);
            l.setEndOffsetInMm(startOffsetInMM);

            currentIndex++;
            statesOnLink.add(currentIndex,newl);
            l=newl;
        }
        if(endOffSetInMM==lengthInMM){
            //add to the end of all current segments
            for(int i=currentIndex;i<statesOnLink.size();i++){
                statesOnLink.get(i).updateLinkSegmentState(event,LOS,delayInMs);
            }
//            printState();
            return;
        }
        while(l.endOffsetInMm<endOffSetInMM){
            l.updateLinkSegmentState(event,LOS,delayInMs);
            currentIndex++;
            l=statesOnLink.get(currentIndex);
        }
        if(l.endOffsetInMm!=endOffSetInMM){
            LinkSegmentState e=l.copy();
            e.endOffsetInMm=endOffSetInMM;
            l.startOffsetInMm=endOffSetInMM;
            statesOnLink.add(currentIndex,e);
            l=e;
        }
        l.updateLinkSegmentState(event,LOS,delayInMs);

//        printState();
    }
    public void addEvent(Set<JSONObject> events, int startOffsetInMm, int endOffsetInMm,int LOS,int delayInMs) {
        //does the same als the addEvent function but split to avoid having to create a set for each country
        //only used when merging linkstates
        int endOffSetInMM=endOffsetInMm;
        int startOffsetInMM=startOffsetInMm;
        if(startOffsetInMM>lengthInMM)return;
        if(endOffSetInMM>lengthInMM)endOffSetInMM=lengthInMM;
        if(endOffSetInMM<=startOffsetInMM)return;
        LinkSegmentState l=statesOnLink.get(0);
        int currentIndex=0;
        while(l.endOffsetInMm<=startOffsetInMM){
            l=statesOnLink.get(currentIndex+1);
            currentIndex++;
        }
        if(l.startOffsetInMm!=startOffsetInMM){
            LinkSegmentState newl=l.copy();
            newl.setStartOffsetInMm(startOffsetInMM);
            l.setEndOffsetInMm(startOffsetInMM);

            currentIndex++;
            statesOnLink.add(currentIndex,newl);
            l=newl;
        }
        if(endOffSetInMM==lengthInMM){
            //add to the end of all current segments
            for(int i=currentIndex;i<statesOnLink.size();i++){
                statesOnLink.get(i).updateLinkSegmentState(events,LOS,delayInMs);
            }
//            printState();
            return;
        }
        while(l.endOffsetInMm<endOffSetInMM){
            l.updateLinkSegmentState(events,LOS,delayInMs);
            currentIndex++;
            l=statesOnLink.get(currentIndex);
        }
        if(l.endOffsetInMm!=endOffSetInMM){
            LinkSegmentState e=l.copy();
            e.endOffsetInMm=endOffSetInMM;
            l.startOffsetInMm=endOffSetInMM;
            statesOnLink.add(currentIndex,e);
            l=e;
        }
        l.updateLinkSegmentState(events,LOS,delayInMs);

//        printState();
    }
    public void printState(){
        logger.info("State:");
        for (LinkSegmentState linkSegmentState : statesOnLink) {
            logger.info(""+linkSegmentState.startOffsetInMm+"->"+linkSegmentState.endOffsetInMm+": "+linkSegmentState.LOS);
        }
    }

    List<LOSEdgeIteratorState> losEdgeIteratorStates;
    public List<LOSEdgeIteratorState> getLOSEdgeIteratorStates() {
        return losEdgeIteratorStates;
    
    }
    public void setLOSEdgeIteratorStates(List<LOSEdgeIteratorState>ll) {
        this.losEdgeIteratorStates=ll;
    }
    public List<LinkSegmentState>getStatesOnLink() {
        return statesOnLink;
    }

    public int getLengthOfLinkInMM() {
        return lengthInMM;
    }   
    public int getTotalDelayInMsOnLink(){
        int total=0;
        for (LinkSegmentState linkSegmentState : statesOnLink) {
            total+=linkSegmentState.delayInMs;
        }
        return total;
    }

}
