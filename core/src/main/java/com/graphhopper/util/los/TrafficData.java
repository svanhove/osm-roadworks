package com.graphhopper.util.los;

import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.util.EdgeIteratorState;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stores all traffic information for 1 specific datasetCode
 * @author Maarten
 */
public class TrafficData {
    protected static final Logger logger = LoggerFactory.getLogger(TrafficData.class);
    //the data
    private TLongObjectMap<LinkSegmentation> forwardDataForLinks;
    private TLongObjectMap<LinkSegmentation> backwardDataForLinks;
    //the graph
    private GraphHopperStorage g;
    //description of this dataset
    private String corpMapID;
    private DateTime creationTime;
    private DateTime expirationTime;
    private final String datasetCode;
    private final String countryCode;
    private final String url;
    //info about input processing
    protected int missingInfo=0;
    protected int missingWhen=0;
    protected int processedEvents=0;
    protected int processedSections=0;
    protected int errorsDuringProcessing=0;
    protected int skippedForWrongMapID=0;
    protected int skippedForAge=0;
    protected int processedLinks=0;

    
    public TrafficData(String datasetCode,String countryCode,DateTime creationTime,DateTime expirationTime,String url,GraphHopperStorage g){
        forwardDataForLinks=new TLongObjectHashMap<>();
        backwardDataForLinks=new TLongObjectHashMap<>();
        this.g=g;
        this.corpMapID=g.getProperties().get("graphid");
        this.creationTime=creationTime;
        this.expirationTime=expirationTime;
        this.datasetCode=datasetCode;
        this.countryCode=countryCode;
        this.url=url;
    }

    public LinkSegmentation getLinkSegmentation(long ghID, boolean forward) {
        return forward?forwardDataForLinks.get(ghID):backwardDataForLinks.get(ghID);
    }

    public void registerLinkSegmentation(long ghID, boolean forward, LinkSegmentation ls) {
        TLongObjectMap<LinkSegmentation> m=forward?forwardDataForLinks:backwardDataForLinks;
        m.put(ghID, ls);
    }

    public boolean has(long l, boolean forward) {
        return forward?forwardDataForLinks.containsKey(l):backwardDataForLinks.containsKey(l);
    }

    public void update(int ghID, boolean forward, JSONObject toSave, int offsetFrommm, int offsetTomm, int los, int delayInMs) {
//        logger.info("Accounting for "+ghID+","+forward+","+offsetFrommm+","+offsetTomm+","+los+","+delayInMs);
//        if(ghID==264708){
//            logger.info("Read Kennedy event");
//        }
        LinkSegmentation ls=getLinkSegmentation(ghID,forward);
        if(ls==null){
            //new segmentation
            EdgeIteratorState ee=g.getEdgeIteratorState(ghID,Integer.MIN_VALUE);
//            int nid=ee.getBaseNode();
//            double lat=g.getNodeAccess().getLat(nid);
//            double lon=g.getNodeAccess().getLon(nid);
            double distanceInM=ee.getDistance();
            
//            logger.info("lat = "+lat+", lon = "+lon);
            ls=new LinkSegmentation((int)(distanceInM*1000));
            registerLinkSegmentation(ghID,forward,ls);
        }
        ls.addEvent(toSave,offsetFrommm,offsetTomm,los,delayInMs);
//        logger.debug(printPointOnLink(ghID));
//        logger.debug(printPointOnLinkSegmentation(ls));

    }

    private String printPointOnLink(int ghID) {
        EdgeIteratorState eis = g.getEdgeIteratorState(ghID,Integer.MIN_VALUE);
        int baseNodeID=eis.getBaseNode();
        return "point "+g.getNodeAccess().getLat(baseNodeID)+","+g.getNodeAccess().getLon(baseNodeID);
    }

    String getCountryCode() {
        return countryCode;
    }

    boolean isExpired() {
        return this.expirationTime.isBeforeNow();
    }

    String printParsingSummary() {
        return "Read "+processedEvents+" traffic events from "+url+
                ", datasetCode "+datasetCode+
                ", processed "+processedSections+" sections"+
                ", processed "+processedLinks+" links"+
                ", got "+errorsDuringProcessing+" errors during processing"+
                ", skipped "+skippedForWrongMapID+" events for wrong CORP map ID"+
                ", skipped "+skippedForAge+" events for age"+
                ", got "+(backwardDataForLinks.size()+forwardDataForLinks.size())+" segmentations";
    }

    String getDatasetCode() {
        return datasetCode;
    }

    boolean isNewer(TrafficData td) {
        return creationTime.isAfter(td.creationTime);
    }
//    DateTime getCreationTime(){
//        return creationTime;
//    }

    boolean isConstructedAfter(DateTime time) {
        return this.creationTime.isAfter(time);
    }

    String getCorpMapID() {
        return corpMapID;
    }
}
