package com.graphhopper.util.los;

import com.graphhopper.storage.GraphHopperStorage;
import java.util.Collections;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

/**
 * Handles the Kafka listening for traffic data
 * @author Maarten
 */
public class TrafficDataKafkaListener implements Runnable{
    private static DateTimeFormatter dtfs= ISODateTimeFormat.dateTimeNoMillis();

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TrafficDataKafkaListener.class);
    
    private KafkaConsumer<String, String> consumer;
    private boolean run;
    private String kafkaSourceTopic;
    private boolean readyForKafka=false;
    
    public TrafficDataKafkaListener(GraphHopperStorage graphHopperStorage,String kafkaSourceTopic,String kafkaBrokers,String groupID) {
//        this(graphHopperStorage);
        try {
            this.kafkaSourceTopic=kafkaSourceTopic;
            Properties props = new Properties();
            props.put("bootstrap.servers", kafkaBrokers);
            props.put("group.id", groupID);
            props.put("enable.auto.commit", "true");
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            logger.info("Starting kafka with "+kafkaSourceTopic+" topic and "+kafkaBrokers+ " brokers");
            this.consumer = new KafkaConsumer<>(props);
            readyForKafka=true;
        } catch (NullPointerException ex) {
            logger.error("One or more config ENV vars are empty");   
        }
    }   

    @Override
    public void run() {
        logger.info("Started listening to kafka");
        this.run = true;
        while (run&&readyForKafka) {
            logger.debug("Doing while run");
            try {
                consumer.subscribe(Collections.singletonList(kafkaSourceTopic));
                ConsumerRecords<String, String> records = consumer.poll(1000);
                for (ConsumerRecord<String, String> record : records) {
                    String input=record.value();
//                    logger.info("Found kafka record:"+record.key()+","+input);
                    try{
//                        {
//                          "datasetCode"="europe",
//                          "countryCode"=""
//                          "creationTime"="2017-03-06T08:47:45Z"
//                          "expirationTime"="2017-03-06T08:57:45Z",
//                          "snapshotUrl"="/repository.be-mobile.biz/feeds/corp/Europe/TrafficEvents-decoded.json.gz"
//                        }
                        JSONObject jo=new JSONObject(input);
                        String datasetCode=jo.getString("datasetCode");
                        if(datasetCode==null)datasetCode="";
                        String creationTimeString=jo.getString("creationTime");
                        String expirationTimeString=jo.getString("expirationTime");
                        String countryCode=jo.has("countryCode")?jo.getString("countryCode"):"";
                        String snapshotUrl=jo.getString("snapshotUrl");
                        DateTime n=DateTime.now();
                        DateTime expirationTime=expirationTimeString!=null?dtfs.parseDateTime(expirationTimeString):n.plusMinutes(10);
                        DateTime creationTime=creationTimeString!=null?dtfs.parseDateTime(creationTimeString):n;
                        TrafficDataManager.getTrafficDataManager().updateFromUrl(datasetCode,countryCode,creationTime,expirationTime,snapshotUrl);

                    }catch(Exception e){
                        logger.error("Error while processing event from Kafka",e);
                    }
                }
            } catch (Exception ex) {
                logger.error("Error registering to Kafka",ex);
            }
        }
        logger.info("Stopped listening to kafka");
        if(consumer!=null)
            consumer.close();
    }
    public void stop() {
        this.run = false;
    }

    public boolean isValid() {
        return readyForKafka;
    }
}
