package com.graphhopper.util.los;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.joda.time.DateTime;

/**
 * Manages different traffic data object, reading and writing of the code specific instances
 * @author Maarten
 */
public class TrafficDataManager {
    private static TrafficDataManager singleton=new TrafficDataManager();
    private static final long cleanupCheckInSeconds=60;
    private TrafficDataSet dataForCalls;
    private Map<String,TrafficData> dataForUpdate;
    private Map<String,Set<String>> countryCodeMapping;
    private ReentrantReadWriteLock.WriteLock lock=new ReentrantReadWriteLock().writeLock();
    
    private TrafficDataManager(){
        dataForUpdate=new HashMap<>();
        countryCodeMapping=new HashMap<>();
        TimerTask tt=new TimerTask() {
            @Override
            public void run() {
                if(hasExpired())cleanupExpired();
            }
        };
        Timer cleaner=new Timer();
        cleaner.scheduleAtFixedRate(tt, 0, cleanupCheckInSeconds*1000);
    };
    
    public static TrafficDataManager getTrafficDataManager(){
        return singleton;
    }
    public void updateFromUrl(String datasetCode, String countryCode, DateTime creationTimeNewTrafficData,DateTime expirationTime, String snapshotUrl) {
        TrafficData oldTrafficData=dataForUpdate.get(datasetCode);
        if(oldTrafficData!=null&&oldTrafficData.isConstructedAfter(creationTimeNewTrafficData)){
            //don't bother updating if the old traffic data is newer
            return;
        }
        TrafficData td = TrafficDataReader.readDataFromURL(datasetCode, countryCode,creationTimeNewTrafficData,expirationTime,snapshotUrl);
        update(td);
    }
    public void update(TrafficData newTrafficData){
        if(newTrafficData.isExpired())return;
        String datasetCode=newTrafficData.getDatasetCode();
        lock.lock();
        try{
            TrafficData oldTrafficData = dataForUpdate.get(datasetCode);
            if(oldTrafficData==null){
                dataForUpdate.put(datasetCode,newTrafficData);
                Set<String> countries=countryCodeMapping.get(newTrafficData.getCountryCode());
                if(countries==null){
                    countries=new HashSet<>();
                    countryCodeMapping.put(newTrafficData.getCountryCode(), countries);
                }
                countries.add(datasetCode);
            }else{ 
                //if messages are received/processed out of order, ensure the most recent is used
                if(oldTrafficData.isNewer(newTrafficData)){
                    //another snapshot was created and received out of order, no update is needed/done
                    return;
                }
                dataForUpdate.put(datasetCode,newTrafficData);
            }
            TrafficDataSet newtds=new TrafficDataSet(dataForUpdate,countryCodeMapping);
            dataForCalls=newtds;
        }finally{
            lock.unlock();
        }
    }
    
    public TrafficDataSet getActiveTrafficDataSet() {
        return dataForCalls;
    }
    public TrafficDataSet getActiveTrafficDataSet(Set<String> countryCodes){
        if(dataForCalls==null)return null;
        return dataForCalls.getSubSetForCountryCodes(countryCodes);
    }
    protected void cleanupExpired(){
        Map<String,TrafficData> newdataForUpdate= new HashMap<>();
        Map<String,Set<String>> newCountryCodeMapping=new HashMap<>();
        lock.lock();
        try{
            for (Map.Entry<String, TrafficData> entry : dataForUpdate.entrySet()) {
                String datasetCode=entry.getKey();
                TrafficData td= entry.getValue();
                if(!td.isExpired()){
                    newdataForUpdate.put(datasetCode,td);
                    Set<String> countries=newCountryCodeMapping.get(td.getCountryCode());
                    if(countries==null){
                        countries=new HashSet<>();
                        newCountryCodeMapping.put(td.getCountryCode(), countries);
                    }
                    countries.add(datasetCode);
                }
            }
            this.dataForUpdate=newdataForUpdate;
            this.countryCodeMapping=newCountryCodeMapping;
            TrafficDataSet newtds=new TrafficDataSet(dataForUpdate,countryCodeMapping);
            dataForCalls=newtds;
        }finally{
            lock.unlock();
        }
    }
    protected boolean hasExpired(){
        if(dataForCalls==null)return false;
        return dataForCalls.hasExpired();
    }

}
