package com.graphhopper.util.los;

import com.graphhopper.storage.GraphHopperStorage;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

/**
 * Handles the incoming traffic data (parsing, downloading)
 * @author Maarten
 */
public class TrafficDataReader {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TrafficDataKafkaListener.class);

    public static DateTimeFormatter dtfs= ISODateTimeFormat.dateTimeNoMillis();
    private static String userName;
    private static String password;
    private static GraphHopperStorage graphHopperStorage;

    private TrafficDataReader(){};
    
    public static void initTrafficDataImporter(String userName,String password,GraphHopperStorage g){
        TrafficDataReader.userName=userName;
        TrafficDataReader.password=password;
        TrafficDataReader.graphHopperStorage=g;
    }

    public static TrafficData readDataFromURL(String datasetCode,String countryCode,DateTime creationTime,DateTime expirationDate,String url){
        logger.info("Called update traffic info");
        if(url==null)return null;
        if (auth==null)initAuthenticator();
        try {
            InputStream is;
            String u=url;
//            u="https://repository.be-mobile.biz/feeds/test/events-snapshot-2017-01-19T18:17:38Z.json";
//            u="https://repository.be-mobile.biz/feeds/test/events-snapshot-2016-12-20T10_18_16Z.JSON";
//            if(u.startsWith(x))u=u.substring(x.length(),u.length());
//            if(u.contains("http://"))u.replace("http://", "https://");
            if(u.startsWith("http://"))u="https://"+u.substring("http://".length(),u.length());
            logger.debug("Reading from url "+u);
            TrafficData td=new TrafficData(datasetCode,countryCode,creationTime,expirationDate,url,graphHopperStorage);

            is = new URL(u).openStream();
            if(u.endsWith(".gz"))is=new GZIPInputStream(is);
            String json=IOUtils.toString(is);
            JSONArray ja=new JSONArray(json);
            for (Object object : ja) {
                JSONObject event=(JSONObject)object;
                try{
                    registerTrafficEvent(event, td);
                }catch(Exception e){
                    td.errorsDuringProcessing++;
                    logger.error("Could not parse traffic event");
                    try{
                        logger.error(event.toString());
                    }catch(Exception ee){
                        logger.error("error turning traffic event into string");
                    }
                    logger.error("Error:",e);
                    e.printStackTrace();                
                }
            }
            logger.info(td.printParsingSummary());
            return td;
        } catch (Exception ex) {
            Logger.getLogger(TrafficDataKafkaListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private static Authenticator auth;
    
    private static void initAuthenticator(){
        auth=new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {          
                return new PasswordAuthentication(userName, password.toCharArray());
            }
        };
        Authenticator.setDefault(auth);
    }
    private static void registerTrafficEvent(JSONObject event,TrafficData td){
        //create json event to refer to for all links
        JSONObject toSave=getJSONEventToSave(event,td);
        JSONObject corpEvent = (JSONObject)event.get("corpEvent");
        String sourceXMLTime=(String)corpEvent.get("sourceXMLTime");
        DateTime sourceTime=dtfs.parseDateTime(sourceXMLTime);
        if(td.isConstructedAfter(sourceTime.plusMinutes(10))){
//        if(sourceTime.plusMinutes(10).isBefore(td.creationTime)){
            td.skippedForAge++;
            return;
        }
        String corpMapID=(String)corpEvent.get("corpMapID");
        if(!corpMapID.equalsIgnoreCase(td.getCorpMapID())){
            td.skippedForWrongMapID++;
            return;
        }
//        String wkt=corpEvent.has("wkt")?(String)corpEvent.get("wkt"):null;
//        int totalEncodedLengthMm=corpEvent.getInt("totalEncodedLengthMm");
        JSONArray corpDistribution=(JSONArray)corpEvent.getJSONArray("corpDistribution");
        for (Object object : corpDistribution) {
            JSONObject cd=(JSONObject)object;
//            int sectionIndex=cd.has("sectionIndex")?cd.getInt("sectionIndex"):-1;
            JSONArray links=(JSONArray)cd.get("links");
            int los=cd.has("los")?cd.getInt("los"):-1;
            int beginOffsetMm=cd.getInt("beginOffsetMm");
            int endOffsetMm=cd.getInt("endOffsetMm");
//            int delayMs=cd.getInt("delayMs");
            for(int i=0;i<links.length();i++){
                JSONObject jl=(JSONObject)links.get(i);
                int linkID=jl.getInt("linkID");
                boolean direction=jl.getBoolean("forward");
//                int lengthMm=jl.getInt("lengthMm");
                int delayMSOnLink=jl.has("delayMs")?jl.getInt("delayMs"):-1;
                int linkStartOffsetMm=i==0?beginOffsetMm:0;
                int linkEndOffsetMm=i==links.length()-1?endOffsetMm:jl.getInt("lengthMm");
                td.update(linkID, direction, toSave, linkStartOffsetMm, linkEndOffsetMm, los, delayMSOnLink);
                td.processedLinks++;
            }
            td.processedSections++;
        }
        td.processedEvents++;
    }
    private static JSONObject getJSONEventToSave(JSONObject event,TrafficData td) {
        JSONObject toSave=new JSONObject();
        JSONObject whereInformation = (JSONObject)event.get("where");
        JSONObject whatInformation = (JSONObject)event.get("what");
        String metadata = (String)event.get("metadata");
        JSONObject toSaveWhere=getFilteredWhere(whereInformation);
        JSONObject toSaveWhat=getFilteredWhat(whatInformation);
        if(event.has("info")){toSave.put("info", event.get("info"));}else td.missingInfo++;
        if(event.has("when")){toSave.put("when", event.get("when"));}else td.missingWhen++;
        toSave.put("where", toSaveWhere);
        toSave.put("what", toSaveWhat);
        toSave.put("metadata",metadata);
        return toSave;
    }
        
    private static JSONObject getFilteredWhere(JSONObject whereInformation) {
        JSONObject toSaveWhere = new JSONObject();
        JSONArray wn=whereInformation.names();
        for (Object object : wn) {
            String jo=(String)object;
//            if(jo.equalsIgnoreCase("graphhopper"))continue;
            toSaveWhere.put(jo, whereInformation.get(jo));
        }
        return toSaveWhere;
    }
    private static JSONObject getFilteredWhat(JSONObject whatInformation) {
        JSONObject toSaveWhat = new JSONObject();
        JSONArray wn=whatInformation.names();

        for (Object object : wn) {
            String jo=(String)object;
//            if(jo.equalsIgnoreCase("absoluteDelayDistributions")||jo.equalsIgnoreCase("absoluteDelayDistribution"))continue;
//            if(jo.equalsIgnoreCase("relativeDelayDistributions")||jo.equalsIgnoreCase("relativeDelayDistributions"))continue;
            toSaveWhat.put(jo, whatInformation.get(jo));
            
        }
        return toSaveWhat;
    }
}
