package com.graphhopper.util.los;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Consists of a set of active traffic data
 * @author Maarten
 */
public class TrafficDataSet {
    private final Map<String,TrafficData> dataPerDatasetCode;
    private TrafficData[]allTrafficData;
    private final Map<String,Set<String>> datasetCodesPerCountry;
    
    /**
     * Provides a TrafficDataSet without any data, to be used by the internal filter method
     */
    private TrafficDataSet() {
        dataPerDatasetCode=new HashMap<>();
        datasetCodesPerCountry=new HashMap<>();
        allTrafficData=new TrafficData[0];
    }
    
    /**
     * Provides a TrafficDataSet with all data supplied in the arguments
     * @param dataForUpdate
     * @param datasetCodesPerCountry 
     */
    TrafficDataSet(Map<String, TrafficData> dataForUpdate,Map<String,Set<String>> datasetCodesPerCountry) {
        this.dataPerDatasetCode=new HashMap<>(dataForUpdate);
        this.allTrafficData=makeAllArray(dataPerDatasetCode);
        this.datasetCodesPerCountry=new HashMap<>(datasetCodesPerCountry.size());
        for (Map.Entry<String, Set<String>> entry : datasetCodesPerCountry.entrySet()) {
            this.datasetCodesPerCountry.put(entry.getKey(),new HashSet<>(entry.getValue()));
        }
    }
    
    TrafficDataSet getSubSetForCountryCodes(Set<String> countryCodes) {
        TrafficDataSet tds=new TrafficDataSet();
        if(countryCodes!=null&&!countryCodes.isEmpty()){
            for (String countryCode : countryCodes) {
                Set<String> get = this.datasetCodesPerCountry.get(countryCode);
                tds.datasetCodesPerCountry.put(countryCode,get);
                for (String dataSetCode : get) {
                    tds.dataPerDatasetCode.put(dataSetCode,dataPerDatasetCode.get(dataSetCode));
                }
            }
        }
        if(this.datasetCodesPerCountry.containsKey("")){
            for (String dataSetCode : this.datasetCodesPerCountry.get("")) {
                tds.dataPerDatasetCode.put(dataSetCode,dataPerDatasetCode.get(dataSetCode));
            }
        }
        tds.allTrafficData=makeAllArray(tds.dataPerDatasetCode);
        return tds;
    }
    
    public boolean has(long l, boolean forward) {
        for (TrafficData value : allTrafficData) {
            if(value.has(l, forward))return true;
        }
        return false;
    }

    public LinkSegmentation getLinkSegmentation(long edgeID, boolean forward) {
        LinkSegmentation r=null;
        for (TrafficData value : allTrafficData) {
            if(value.has(edgeID, forward)){
                LinkSegmentation seg=value.getLinkSegmentation(edgeID, forward);
                if(r==null){
                    r=seg;
                }
                else{
                    r=LinkSegmentation.merge(r,seg);
                }
            }
        }
        return r;
    }
    /**
     * Quick check to decide if the cleaner should go over the datasets to remove outdated traffic content
     * @return 
     */
    boolean hasExpired() {
        for (TrafficData value : allTrafficData) {
            if(value.isExpired())return true;
        }
        return false;
    }

    private TrafficData[] makeAllArray(Map<String, TrafficData> dataPerDatasetCode) {
        TrafficData[] r=new TrafficData[dataPerDatasetCode.size()];
        int i=0;
        for (TrafficData value : dataPerDatasetCode.values()) {
            r[i++]=value;
        }
        return r;
    }
}