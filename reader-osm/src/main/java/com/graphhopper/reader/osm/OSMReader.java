/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.reader.osm;

import com.graphhopper.coll.GHLongIntBTree;
import com.graphhopper.coll.LongIntMap;
import com.graphhopper.reader.*;
import com.graphhopper.reader.dem.ElevationProvider;
import com.graphhopper.reader.osm.OSMTurnRelation.TurnCostTableEntry;
import com.graphhopper.reader.osm.construction.ConstructionHandler;
import com.graphhopper.reader.osm.construction.DefaultConstructionHandler;
import com.graphhopper.reader.osm.construction.WritingConstructionHandler;
import com.graphhopper.routing.util.DefaultEdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.weighting.TurnWeighting;
import com.graphhopper.storage.*;
import com.graphhopper.util.*;
import com.graphhopper.util.shapes.GHPoint;
import com.graphhopper.reader.osm.construction.KmlHelper;
import gnu.trove.list.TLongList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntLongMap;
import gnu.trove.map.TLongLongMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TLongLongHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.set.TLongSet;
import gnu.trove.set.hash.TLongHashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.graphhopper.util.Helper.nf;
import gnu.trove.iterator.TLongObjectIterator;

/**
 * This class parses an OSM xml or pbf file and creates a graph from it. It does so in a two phase
 * parsing processes in order to reduce memory usage compared to a single parsing processing.
 * <p>
 * 1. a) Reads ways from OSM file and stores all associated node ids in osmNodeIdToIndexMap. If a
 * node occurs once it is a pillar node and if more it is a tower node, otherwise
 * osmNodeIdToIndexMap returns EMPTY.
 * <p>
 * 1. b) Reads relations from OSM file. In case that the relation is a route relation, it stores
 * specific relation attributes required for routing into osmWayIdToRouteWeigthMap for all the ways
 * of the relation.
 * <p>
 * 2.a) Reads nodes from OSM file and stores lat+lon information either into the intermediate
 * datastructure for the pillar nodes (pillarLats/pillarLons) or, if a tower node, directly into the
 * graphStorage via setLatitude/setLongitude. It can also happen that a pillar node needs to be
 * transformed into a tower node e.g. via barriers or different speed values for one way.
 * <p>
 * 2.b) Reads ways OSM file and creates edges while calculating the speed etc from the OSM tags.
 * When creating an edge the pillar node information from the intermediate datastructure will be
 * stored in the way geometry of that edge.
 * <p>
 *
 * @author Peter Karich
 */
public class OSMReader implements DataReader {
    protected static final int EMPTY = -1;
    // pillar node is >= 3
    protected static final int PILLAR_NODE = 1;
    // tower node is <= -3
    protected static final int TOWER_NODE = -2;
    private static final Logger LOGGER = LoggerFactory.getLogger(OSMReader.class);
    private final GraphStorage ghStorage;
    private final Graph graph;
    private final NodeAccess nodeAccess;
    private final TLongList barrierNodeIds = new TLongArrayList();
    private final DistanceCalc distCalc = Helper.DIST_EARTH;
    private final DistanceCalc3D distCalc3D = Helper.DIST_3D;
    private final DouglasPeucker simplifyAlgo = new DouglasPeucker();
    private final boolean exitOnlyPillarNodeException = true;
    private final Map<FlagEncoder, EdgeExplorer> outExplorerMap = new HashMap<FlagEncoder, EdgeExplorer>();
    private final Map<FlagEncoder, EdgeExplorer> inExplorerMap = new HashMap<FlagEncoder, EdgeExplorer>();
    protected long zeroCounter = 0;
    protected PillarInfo pillarInfo;
    private long locations;
    private long skippedLocations;
    private EncodingManager encodingManager = null;
    private TLongObjectMap<String>ccToAdjust=new TLongObjectHashMap<String>();
    private Map<Integer, Long> edgeIDsToWayIDs;

    private int workerThreads = -1;
    // Using the correct Map<Long, Integer> is hard. We need a memory efficient and fast solution for big data sets!
    //
    // very slow: new SparseLongLongArray
    // only append and update possible (no unordered storage like with this doubleParse): new OSMIDMap
    // same here: not applicable as ways introduces the nodes in 'wrong' order: new OSMIDSegmentedMap
    // memory overhead due to open addressing and full rehash:
    //        nodeOsmIdToIndexMap = new BigLongIntMap(expectedNodes, EMPTY);
    // smaller memory overhead for bigger data sets because of avoiding a "rehash"
    // remember how many times a node was used to identify tower nodes
    private LongIntMap osmNodeIdToInternalNodeMap;
    private TLongLongHashMap osmNodeIdToNodeFlagsMap;
    private TLongLongHashMap osmWayIdToRouteWeightMap;
    // stores osm way ids used by relations to identify which edge ids needs to be mapped later
    private TLongHashSet osmWayIdSet = new TLongHashSet();
    private TIntLongMap edgeIdToOsmWayIdMap;
    private boolean doSimplify = true;
    private int nextTowerId = 0;
    private int nextPillarId = 0;
    // negative but increasing to avoid clash with custom created OSM files
    private long newUniqueOsmId = -Long.MAX_VALUE;
    private ElevationProvider eleProvider = ElevationProvider.NOOP;
    private File osmFile;
    private Date osmDataDate;

    public static String targetFileIntermediate = "";
    public static String targetFileTagOverrides = "";

    public static boolean WRITE_INTERMEDIARY_FORMAT;
    public static boolean WRITE_TAG_OVERRIDES;

    public OSMReader(GraphHopperStorage ghStorage) {
        this.ghStorage = ghStorage;
        this.graph = ghStorage;
        this.nodeAccess = graph.getNodeAccess();

        osmNodeIdToInternalNodeMap = new GHLongIntBTree(200);
        osmNodeIdToNodeFlagsMap = new TLongLongHashMap(200, .5f, 0, 0);
        osmWayIdToRouteWeightMap = new TLongLongHashMap(200, .5f, 0, 0);
        pillarInfo = new PillarInfo(nodeAccess.is3D(), ghStorage.getDirectory());
        // all edges which are marked as under construction
        edgesUnderConstruction = new ArrayList<>();
        // all edges which are marked as "access"="no"
        accessNoEdges = new HashSet<>();
        // mapping of the graphhopper edge IDs to their osm way IDs
        edgeIDsToWayIDs = new HashMap<Integer, Long>();

    }

    @Override
    public void readGraph() throws IOException {
        if (encodingManager == null)
            throw new IllegalStateException("Encoding manager was not set.");

        if (osmFile == null)
            throw new IllegalStateException("No OSM file specified");

        if (!osmFile.exists())
            throw new IllegalStateException("Your specified OSM file does not exist:" + osmFile.getAbsolutePath());

        StopWatch sw1 = new StopWatch().start();
        preProcess(osmFile);
        sw1.stop();

        StopWatch sw2 = new StopWatch().start();
        writeOsm2Graph(osmFile);
        sw2.stop();

        LOGGER.info("time pass1:" + (int) sw1.getSeconds() + "s, "
                + "pass2:" + (int) sw2.getSeconds() + "s, "
                + "total:" + (int) (sw1.getSeconds() + sw2.getSeconds()) + "s");

        ConstructionHandler handler = WRITE_INTERMEDIARY_FORMAT ? new WritingConstructionHandler(targetFileIntermediate) : new DefaultConstructionHandler();
        Set<Long> wayIDsUnderConstruction = handler.handleConstruction(graph, edgesUnderConstruction, accessNoEdges, edgeIDsToWayIDs, encodingManager.getEncoder("car"));
        if(WRITE_TAG_OVERRIDES) {
            System.out.println("Writing tag overrides to "+targetFileTagOverrides+" ...");
            writeTagOverrides(osmFile, wayIDsUnderConstruction);
        }
//        writeEdgesUnderConstructionKML();
    }



    public void writeEdgesUnderConstructionKML() {
        KmlHelper.writeEdgeIteratorStates(edgesUnderConstruction, graph, "C:/Stephanie/UnderConstruction_Official");
    }


    /**
     * Reads the file listing the way IDs under construction (both listed as such in osm and added by be-mobile)
     * This file is the intermediary format as discussed with Maarten Houbraken.
     * @param filename
     * @deprecated was when we converted from the intermediary format to the tag overrided - keeping it
     * in case we ever want to do this again
     * @return
     */
    private Set<Long> readConstructionFile(String filename)  {
        Set<Long> wayIDs = new HashSet<Long>();
        try {
            Scanner sc = new Scanner(new File(filename));
            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(";");
                String first = lineScanner.next();
                while(lineScanner.hasNext()) {
                    String wayIDstring = lineScanner.next();
                    Scanner wayIDScanner = new Scanner(wayIDstring);
                    wayIDScanner.useDelimiter(",");
                    while(wayIDScanner.hasNext()) {
                        String wayID = wayIDScanner.next();
                        Long wayIDparsed = Long.parseLong(wayID);
                        wayIDs.add(wayIDparsed);
                    }
                }
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return wayIDs;
    }

    // Writes one line containing the new value for a given tag for a given way id
    private void writeOverride(PrintWriter pw, Long id, String field, String value, String timestamp) {
        pw.println(id+";"+field+";"+value+";"+timestamp);
    }



    private void writeTagOverrides(File osmFile, Set<Long> wayIDs) {
        if(typeMapping==null){
            initTypeMapping();
        }

        String timestamp = new SimpleDateFormat("yyyy-MM-dd'T'h:m:ssZZZZZ").format(new Date());

        OSMInputFile in = null;
        PrintWriter printWriter = null;
        try {
            in = new OSMInputFile(osmFile).setWorkerThreads(workerThreads).open();
            printWriter = new PrintWriter(targetFileTagOverrides);

            Set<String> tagsToIgnore = createTagsToIgnore();
            Set<String> tagPrefixesToIgnore = createTagPrefixesToIgnore();

            SortedMap<String, TreeSet<String>> allRemainingtags = new TreeMap<String, TreeSet<String>>();

            ReaderElement item;

            StringBuilder bmRoadworkLines = new StringBuilder();
            printWriter.println("WAY_ID;TAG;VALUE;TIMESTAMP");
            while ((item = in.getNext()) != null) {

                if (item.isType(ReaderElement.WAY)) {

                    ReaderWay way = (ReaderWay) item;

                    Map<String, Object> tagsToWrite = new HashMap<>();

                    if(wayIDs.contains(way.getId())) {
                        Map<String, Object> tags = way.getTags();


                        // this is code to determine which other tags might be interesting for us to look at to include in future implementations
                        // does not affect the current result
                        for(String tag: tags.keySet()) {
                            if(!tagsToIgnore.contains(tag) && !containsPrefixToIgnore(tag, tagPrefixesToIgnore)) {
                                if(!tag.equals("highway") || (!typeMapping.containsKey(way.getTag(tag)) && !way.getTag(tag).equals("construction")))
                                    tagsToWrite.put(tag, tags.get(tag));
                                if(!allRemainingtags.containsKey(tag))
                                    allRemainingtags.put(tag, new TreeSet<String>());
                                allRemainingtags.get(tag).add(way.getTag(tag));
                            }
                        }

//                        if(!tagsToWrite.isEmpty()) {
//                            System.out.println("\nLooking at way "+item.getId());
//                            for(String tag: tagsToWrite.keySet())
//                                System.out.println(tag+":   "+tagsToWrite.get(tag));
//                        }

                        /***********
                         * CHANGES *
                         ***********/

                        // access should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "access", timestamp);

                        // hgv should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "hgv", timestamp);

                        // motorcar should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "motorcar", timestamp);

                        // motor_vehicle should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "motor_vehicle", timestamp);

                        // vehicle should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "vehicle", timestamp);

                        // temporary:access should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "temporary:access", timestamp);

                        // car should be "yes" instead of "no"
                        replaceNoByYes(printWriter, way, "car", timestamp);



                        // "highway"="construction" becomes "highway"=<value for "construction" field> OR <value for "construction:highway" field>  OR "unclassified"
                        if(containsTagWithValue(way, "highway", "construction")) {
                            String newValue = "unclassified";
                            if(containsNonNullTag(way, "construction") && !way.getTag("construction").isEmpty() && typeMapping.containsKey(way.getTag("construction"))) {
                                newValue = way.getTag("construction");
                             }
                            else if(containsNonNullTag(way, "construction:highway") && !way.getTag("construction:highway").isEmpty() && typeMapping.containsKey(way.getTag("construction:highway"))) {
                                newValue = way.getTag("construction:highway");
                            }
                            writeOverride(printWriter, way.getId(), "highway", newValue, timestamp);
                        }

                        // if "highway" is "unclassified" and "construction" is "yes", try if you can get the highway value from the "construction:highway" field
                        if(containsTagWithValue(way, "highway", "unclassified") && containsTagWithValue(way, "construction", "yes")) {
                            if(containsNonNullTag(way, "construction:highway")) {
                                String value = way.getTag("construction:highway");
                                if(typeMapping.containsKey(value) && !value.equals("unclassified")) {
                                    writeOverride(printWriter, way.getId(), "highway", value, timestamp);
                                }
                            }
                        }

                        // "construction" should always be "no"
                        if(containsNonNullTag(way, "construction")) {
                            if(!way.getTag("construction").equals("no"))
                                writeOverride(printWriter, way.getId(), "construction", "no", timestamp);
                        }

                        bmRoadworkLines.append(way.getId()+";BM_ROADWORK;1;"+timestamp+"\n");
                    }

                }

            }
            printWriter.println(bmRoadworkLines);

        } catch (Exception ex) {
            throw new RuntimeException("Problem while parsing file", ex);
        } finally {
            Helper.close(in);
            if(printWriter != null)
                printWriter.close();
        }
    }

    // This does not affect the final result. This is for investigation purposes to see if any other tags are left which need our attention.
    // All the tags listed here are considered "not requiring action" (or we have already implemented an override for them).
    private Set<String> createTagsToIgnore() {
        Set<String> tagsToIgnore = new HashSet<>();

        tagsToIgnore.add("bicycle");
        tagsToIgnore.add("surface");
        tagsToIgnore.add("maxspeed");

        tagsToIgnore.add("oneway");
        tagsToIgnore.add("maxspeed:hgv:conditional");
        tagsToIgnore.add("maxspeed:conditional");
        tagsToIgnore.add("ref");
//            tagsToIgnore.add("hgv");
        tagsToIgnore.add("lit");
        tagsToIgnore.add("minspeed");
        tagsToIgnore.add("gauge");
        tagsToIgnore.add("junction");
        tagsToIgnore.add("name");
        tagsToIgnore.add("waterway");
        tagsToIgnore.add("natural");
        tagsToIgnore.add("electrified");
        tagsToIgnore.add("usage");
        tagsToIgnore.add("crop");
        tagsToIgnore.add("layer");
        tagsToIgnore.add("tunnel");
        tagsToIgnore.add("building");
        tagsToIgnore.add("amenity");
        tagsToIgnore.add("cables");
        tagsToIgnore.add("power");

        tagsToIgnore.add("funicular");
        tagsToIgnore.add("circuits");
        tagsToIgnore.add("height");
        tagsToIgnore.add("meadow");
        tagsToIgnore.add("opening_date");

        tagsToIgnore.add("public_transport");
        tagsToIgnore.add("frequency");
        tagsToIgnore.add("voltage");

        tagsToIgnore.add("cycleway");
        tagsToIgnore.add("horse");
        tagsToIgnore.add("aeroway");

        tagsToIgnore.add("width");
        tagsToIgnore.add("fixme");
        tagsToIgnore.add("is_in:city");


        tagsToIgnore.add("leaf_cycle");
        tagsToIgnore.add("leaf_type");
        tagsToIgnore.add("landuse");
        tagsToIgnore.add("railway");
        tagsToIgnore.add("railway:ects");
        tagsToIgnore.add("operator");
        tagsToIgnore.add("service");
        tagsToIgnore.add("moped");
        tagsToIgnore.add("boat");
        tagsToIgnore.add("tracktype");
//            tagsToIgnore.add("motorcar");
        tagsToIgnore.add("postal_code");
        tagsToIgnore.add("motorcycle");
        tagsToIgnore.add("step_count");
        tagsToIgnore.add("train");
        tagsToIgnore.add("mapping_status");
        tagsToIgnore.add("is_sidepath");
        tagsToIgnore.add("est_width");
        tagsToIgnore.add("priority_road");
        tagsToIgnore.add("note_de");
        tagsToIgnore.add("bench");
        tagsToIgnore.add("residential");
        tagsToIgnore.add("Name");
        tagsToIgnore.add("ford");
        tagsToIgnore.add("alt_ref");
        tagsToIgnore.add("disused");
        tagsToIgnore.add("abandoned");
        tagsToIgnore.add("detail");
        tagsToIgnore.add("path");

        tagsToIgnore.add("tmc");

        tagsToIgnore.add("escalator");
        tagsToIgnore.add("trolley_wire");
        tagsToIgnore.add("culvert");
        tagsToIgnore.add("name_1");
        tagsToIgnore.add("rtc_rate");
        tagsToIgnore.add("traffic");
        tagsToIgnore.add("abutters");
        tagsToIgnore.add("rcn_ref");
        tagsToIgnore.add("note_old");
        tagsToIgnore.add("adfc_hl_net");
        tagsToIgnore.add("todo");
        tagsToIgnore.add("except");
        tagsToIgnore.add("noname");
        tagsToIgnore.add("ramp");
        tagsToIgnore.add("floating");
        tagsToIgnore.add("converted_by");
        tagsToIgnore.add("check_date");
        tagsToIgnore.add("maxaxleload");
        tagsToIgnore.add("loc_ref");
        tagsToIgnore.add("info");
        tagsToIgnore.add("paved");
        tagsToIgnore.add("reg_name");
        tagsToIgnore.add("hazard");
        tagsToIgnore.add("cars");
        tagsToIgnore.add("stroller");
        tagsToIgnore.add("winter_service");

        tagsToIgnore.add("bike");
        tagsToIgnore.add("remark");
        tagsToIgnore.add("goods");
        tagsToIgnore.add("acc");

        tagsToIgnore.add("date_of_creation");


        tagsToIgnore.add("hide");
        tagsToIgnore.add("last_renovation");

        tagsToIgnore.add("material");
        tagsToIgnore.add("hov");
        tagsToIgnore.add("Note");

        tagsToIgnore.add("bicycle_road");
        tagsToIgnore.add("designation");
        tagsToIgnore.add("boduble");
        tagsToIgnore.add("track");
        tagsToIgnore.add("network");
        tagsToIgnore.add("last_checked");
        tagsToIgnore.add("end_date");
        tagsToIgnore.add("shelter");
        tagsToIgnore.add("tourist_bus");
        tagsToIgnore.add("url");
        tagsToIgnore.add("lit_1");
        tagsToIgnore.add("priority");
        tagsToIgnore.add("speisebezirk");
        tagsToIgnore.add("bus_bay");
        tagsToIgnore.add("source_ref");
        tagsToIgnore.add("hist_name");
        tagsToIgnore.add("condition");
        tagsToIgnore.add("substation");
        tagsToIgnore.add("kerb");
        tagsToIgnore.add("workrules");
        tagsToIgnore.add("bdouble");
        tagsToIgnore.add("trailer");
        tagsToIgnore.add("type");

        tagsToIgnore.add("traffic_calming");
        tagsToIgnore.add("levels");
        tagsToIgnore.add("demolished");
        tagsToIgnore.add("heritage");
        tagsToIgnore.add("notes");
        tagsToIgnore.add("tactile_paving");
        tagsToIgnore.add("secondary");
        tagsToIgnore.add("man_made");

//            tagsToIgnore.add("barrier");
        tagsToIgnore.add("area");
        tagsToIgnore.add("bridge");
        tagsToIgnore.add("created_by");
        tagsToIgnore.add("railway:etcs");
        tagsToIgnore.add("lanes");
        tagsToIgnore.add("foot");
        tagsToIgnore.add("note");
        tagsToIgnore.add("source");

        tagsToIgnore.add("endpoint:busbar");
        tagsToIgnore.add("line");
        tagsToIgnore.add("fenced");
        tagsToIgnore.add("ref:caclr");
        tagsToIgnore.add("website");
        tagsToIgnore.add("fence_type");

        tagsToIgnore.add("shop");
        tagsToIgnore.add("parking");
        tagsToIgnore.add("water");
        tagsToIgnore.add("railway:traffic_mode");
//            tagsToIgnore.add("noexit");
        tagsToIgnore.add("opening_hours");
        tagsToIgnore.add("recycling:glass");
        tagsToIgnore.add("recycling:batteries");
        tagsToIgnore.add("recycling:shoes");
        tagsToIgnore.add("recycling:plastic_packaging");

        tagsToIgnore.add("recycling_type");
        tagsToIgnore.add("recycling:magazines");
        tagsToIgnore.add("recycling:plastic");
        tagsToIgnore.add("recycling:glass_bottles");
        tagsToIgnore.add("recycling:scrap_metal");
        tagsToIgnore.add("recycling:small_appliances");


        tagsToIgnore.add("leisure");
        tagsToIgnore.add("sidewalk");
        tagsToIgnore.add("location");
        tagsToIgnore.add("maxspeed:hgv");
        tagsToIgnore.add("trail_visibility");

        tagsToIgnore.add("level");
        tagsToIgnore.add("cuisine");
        tagsToIgnore.add("country");
        tagsToIgnore.add("office");
        tagsToIgnore.add("tourism");
        tagsToIgnore.add("alt_name");
        tagsToIgnore.add("wheelchair");
        tagsToIgnore.add("religion");
        tagsToIgnore.add("denomination");

        tagsToIgnore.add("busway");
        tagsToIgnore.add("railway");
        tagsToIgnore.add("railway:track_class");
        tagsToIgnore.add("passenger_lines");
        tagsToIgnore.add("park_ride");
        tagsToIgnore.add("axle:load");

        tagsToIgnore.add("boundary");
        tagsToIgnore.add("admin_level");
        tagsToIgnore.add("description");
        tagsToIgnore.add("psv");

        tagsToIgnore.add("incline");
        tagsToIgnore.add("sas_scale");
        tagsToIgnore.add("phone");
        tagsToIgnore.add("fax");
        tagsToIgnore.add("");
        tagsToIgnore.add("");


        tagsToIgnore.add("internet_access");
        tagsToIgnore.add("int_ref");
        tagsToIgnore.add("lcn");
        tagsToIgnore.add("e-mail");
        tagsToIgnore.add("shoulder");
        tagsToIgnore.add("length");
        tagsToIgnore.add("maxheight");
        tagsToIgnore.add("smoothness");
        tagsToIgnore.add("lcn");
        tagsToIgnore.add("fee");
        tagsToIgnore.add("sport");
        tagsToIgnore.add("ncn_ref");
        tagsToIgnore.add("image");
        tagsToIgnore.add("direction");

        tagsToIgnore.add("museum");
        tagsToIgnore.add("loc_name");
        tagsToIgnore.add("old_operator");
        tagsToIgnore.add("start_date");
        tagsToIgnore.add("smoking");
        tagsToIgnore.add("brand");

        tagsToIgnore.add("embankment");
        tagsToIgnore.add("hazmat");
        tagsToIgnore.add("golf");
        tagsToIgnore.add("abbreviation");
        tagsToIgnore.add("destination");
        tagsToIgnore.add("history");

//            tagsToIgnore.add("proposed");
        tagsToIgnore.add("placement");
        tagsToIgnore.add("cutting");
        tagsToIgnore.add("supervised");
        tagsToIgnore.add("min_age");
        tagsToIgnore.add("old_name");
        tagsToIgnore.add("email");
        tagsToIgnore.add("official_name");


        tagsToIgnore.add("public_building");


        tagsToIgnore.add("nudism");
        tagsToIgnore.add("tents");
        tagsToIgnore.add("ncn_ref");
        tagsToIgnore.add("indoor");
        tagsToIgnore.add("traffic_sign");
        tagsToIgnore.add("overtaking");
        tagsToIgnore.add("historic");
        tagsToIgnore.add("tram");
        tagsToIgnore.add("wikidata");
        tagsToIgnore.add("taxi");
        tagsToIgnore.add("alt_name_1");

        tagsToIgnore.add("emergency");
        tagsToIgnore.add("covered");
        tagsToIgnore.add("ruins");

        tagsToIgnore.add("nat_ref");
        tagsToIgnore.add("turn");
        tagsToIgnore.add("postcode");
        tagsToIgnore.add("recommended_speed");
        tagsToIgnore.add("FIXME");
        tagsToIgnore.add("maxweight");
        tagsToIgnore.add("sac_scale");
        tagsToIgnore.add("is_in");
        tagsToIgnore.add("intermittent");
        tagsToIgnore.add("craft");
        tagsToIgnore.add("capacity");
        tagsToIgnore.add("boules");
        tagsToIgnore.add("fixed");
        tagsToIgnore.add("rcn");
        tagsToIgnore.add("segregated");
        tagsToIgnore.add("odbl");
        tagsToIgnore.add("bus");
        tagsToIgnore.add("agricultural");
        tagsToIgnore.add("maxstay");
        tagsToIgnore.add("mofa");
        tagsToIgnore.add("conveying");
        tagsToIgnore.add("old_ref");
        tagsToIgnore.add("footway");
        tagsToIgnore.add("crossing_ref");
        tagsToIgnore.add("handrail");
        tagsToIgnore.add("crossing");
        tagsToIgnore.add("construction:bicycle");
        tagsToIgnore.add("construction:cycleway");
        tagsToIgnore.add("construction:embankment:");
        tagsToIgnore.add("construction:foot");
        tagsToIgnore.add("construction:maxspeed");
        tagsToIgnore.add("construction:note");
        tagsToIgnore.add("construction:service");
        tagsToIgnore.add("construction:traffic_sign");
        tagsToIgnore.add("construction:surface");

        tagsToIgnore.add("maxlength");
        tagsToIgnore.add("maxwidth");
        tagsToIgnore.add("comment");
        tagsToIgnore.add("short_name");
        tagsToIgnore.add("wetland");
        tagsToIgnore.add("wood");
        tagsToIgnore.add("monument");

        tagsToIgnore.add("y");
        tagsToIgnore.add("year");
        tagsToIgnore.add("wood");
        tagsToIgnore.add("yelp");
        tagsToIgnore.add("yer");
        tagsToIgnore.add("yes");
        tagsToIgnore.add("z-index");
        tagsToIgnore.add("zone");
        tagsToIgnore.add("zoo");
        tagsToIgnore.add("zu");


        tagsToIgnore.add("tracks");
        tagsToIgnore.add("bridge_name");
        tagsToIgnore.add("wikipedia");
        tagsToIgnore.add("x");
        tagsToIgnore.add("wtb_description");
        tagsToIgnore.add("wrong_name");
        tagsToIgnore.add("tracks");

        tagsToIgnore.add("steps");
        tagsToIgnore.add("ele");

        tagsToIgnore.add("shelter_type");

        tagsToIgnore.add("access");
//            tagsToIgnore.add("highway");
        tagsToIgnore.add("construction");
        return tagsToIgnore;
    }

    // This does not affect the final result. This is for investigation purposes to see if any other tags are left which need our attention.
    // All the tag prefixes listed here are considered "not requiring action" (or we have already implemented an override for them).
    private Set<String> createTagPrefixesToIgnore() {
        Set<String> tagPrefixesToIgnore = new HashSet<>();

        tagPrefixesToIgnore.add("addr:");
        tagPrefixesToIgnore.add("year_");
        tagPrefixesToIgnore.add("recycling:");
        tagPrefixesToIgnore.add("name:");
        tagPrefixesToIgnore.add("is_in:");
        tagPrefixesToIgnore.add("roof:");
        tagPrefixesToIgnore.add("x-");
        tagPrefixesToIgnore.add("CLC:");
        tagPrefixesToIgnore.add("contact:");
        tagPrefixesToIgnore.add("object:");
        tagPrefixesToIgnore.add("oam:");
        tagPrefixesToIgnore.add("notes:");
        tagPrefixesToIgnore.add("not:");
        tagPrefixesToIgnore.add("no:");

        tagPrefixesToIgnore.add("building:");
        tagPrefixesToIgnore.add("zone:");
        tagPrefixesToIgnore.add("bridge:");
        tagPrefixesToIgnore.add("capacity:");
        tagPrefixesToIgnore.add("generator:");
        tagPrefixesToIgnore.add("lanes:");
        tagPrefixesToIgnore.add("monitoring:");
        tagPrefixesToIgnore.add("old_name:");
        tagPrefixesToIgnore.add("surveillance:");
        tagPrefixesToIgnore.add("oof:");
        tagPrefixesToIgnore.add("source:");
        tagPrefixesToIgnore.add("abandoned:");
        tagPrefixesToIgnore.add("fuel:");
        tagPrefixesToIgnore.add("note:");
        tagPrefixesToIgnore.add("his:");
//            tagPrefixesToIgnore.add("highway:");
        tagPrefixesToIgnore.add("gauge:");
        tagPrefixesToIgnore.add("health_specialty:");
        tagPrefixesToIgnore.add("hgv:");
        tagPrefixesToIgnore.add("parking:");
        tagPrefixesToIgnore.add("ramp:");
        tagPrefixesToIgnore.add("foot:");
        tagPrefixesToIgnore.add("disused:");
        tagPrefixesToIgnore.add("maxstay:");
        tagPrefixesToIgnore.add("turn:");
        tagPrefixesToIgnore.add("historic:");
        tagPrefixesToIgnore.add("tunnel:");
//            tagPrefixesToIgnore.add("motor_vehicle:");
        tagPrefixesToIgnore.add("mtb:");
        tagPrefixesToIgnore.add("footway:");
        tagPrefixesToIgnore.add("fixme:");
        tagPrefixesToIgnore.add("railing:");
        tagPrefixesToIgnore.add("razed:");
        tagPrefixesToIgnore.add("official:");
        tagPrefixesToIgnore.add("augsburg_stadt:");
        tagPrefixesToIgnore.add("bridge.source:");
        tagPrefixesToIgnore.add("opening_date:");
        tagPrefixesToIgnore.add("operator:");
        tagPrefixesToIgnore.add("overtaking:");
        tagPrefixesToIgnore.add("shoulder:");
        tagPrefixesToIgnore.add("sidepath:");
        tagPrefixesToIgnore.add("snowplowing:");
        tagPrefixesToIgnore.add("starting_date:");
        tagPrefixesToIgnore.add("strassen-nrw:");
        tagPrefixesToIgnore.add("survey:");
        tagPrefixesToIgnore.add("maxspeed:");
        tagPrefixesToIgnore.add("wikipedia");
        tagPrefixesToIgnore.add("start_date:");
        tagPrefixesToIgnore.add("smoking:");
        tagPrefixesToIgnore.add("opening_hours:");
        tagPrefixesToIgnore.add("openGeoDB:");
        tagPrefixesToIgnore.add("destination:");
        tagPrefixesToIgnore.add("kerb:");
        tagPrefixesToIgnore.add("oneway:");
        tagPrefixesToIgnore.add("handrail:");
        tagPrefixesToIgnore.add("it:");
        tagPrefixesToIgnore.add("railway:");
        tagPrefixesToIgnore.add("psv:");
        tagPrefixesToIgnore.add("ref:");
        tagPrefixesToIgnore.add("payment:");
        tagPrefixesToIgnore.add("3D:");
        tagPrefixesToIgnore.add("3d:");
        tagPrefixesToIgnore.add("3dr:");
        tagPrefixesToIgnore.add("3dshapes:");
        tagPrefixesToIgnore.add("bicycle:");
        tagPrefixesToIgnore.add("bus:");
        tagPrefixesToIgnore.add("BLfD:");
        tagPrefixesToIgnore.add("service:");
        tagPrefixesToIgnore.add("width:");
        tagPrefixesToIgnore.add("BLfD:");
        tagPrefixesToIgnore.add("maxweight:");
        tagPrefixesToIgnore.add("maxheight:");
        tagPrefixesToIgnore.add("CEMT:");
        tagPrefixesToIgnore.add("Brandschutzstreifen:");
        tagPrefixesToIgnore.add("placement:");
        tagPrefixesToIgnore.add("plant:");
        tagPrefixesToIgnore.add("Class:");
        tagPrefixesToIgnore.add("EA:");
        tagPrefixesToIgnore.add("sidewalk:");
        tagPrefixesToIgnore.add("FFH_ID:");
        tagPrefixesToIgnore.add("FIXME:");
        tagPrefixesToIgnore.add("service_");
        tagPrefixesToIgnore.add("File:");
        tagPrefixesToIgnore.add("plant_community:");
        tagPrefixesToIgnore.add("removed:");
        tagPrefixesToIgnore.add("school:");
        tagPrefixesToIgnore.add("internet_access:");
        tagPrefixesToIgnore.add("heritage:");
        tagPrefixesToIgnore.add("donation:");
        tagPrefixesToIgnore.add("GoeVB:");
        tagPrefixesToIgnore.add("description:");
        tagPrefixesToIgnore.add("demolished:");
        tagPrefixesToIgnore.add("was:");
        tagPrefixesToIgnore.add("HGK:");
        tagPrefixesToIgnore.add("wheelchair:");
        tagPrefixesToIgnore.add("alt_name:");
        tagPrefixesToIgnore.add("Karlsruhe:");
        tagPrefixesToIgnore.add("franchise:");
        tagPrefixesToIgnore.add("fraternity:");
        tagPrefixesToIgnore.add("free_flying:");
        tagPrefixesToIgnore.add("gaelic_games:");
        tagPrefixesToIgnore.add("golf:");
        tagPrefixesToIgnore.add("LANUV-NRW:");
        tagPrefixesToIgnore.add("LANUV_ID:");
        tagPrefixesToIgnore.add("LANUV_NRW:");
        tagPrefixesToIgnore.add("goods:");
        tagPrefixesToIgnore.add("government:");
        tagPrefixesToIgnore.add("harbour:");
        tagPrefixesToIgnore.add("hazmat:");
        tagPrefixesToIgnore.add("LfDH:");
        tagPrefixesToIgnore.add("music");
        tagPrefixesToIgnore.add("SGD_ID:");
        tagPrefixesToIgnore.add("SGD-ID:");
        tagPrefixesToIgnore.add("healthcare");
        tagPrefixesToIgnore.add("image:");
        tagPrefixesToIgnore.add("hiking");
        tagPrefixesToIgnore.add("health");
        tagPrefixesToIgnore.add("left");
        tagPrefixesToIgnore.add("TMC:");
        tagPrefixesToIgnore.add("minspeed");
        tagPrefixesToIgnore.add("protection_");
        tagPrefixesToIgnore.add("provided_");
        tagPrefixesToIgnore.add("seamark:");
        tagPrefixesToIgnore.add("diet:");
        tagPrefixesToIgnore.add("moped:");
        tagPrefixesToIgnore.add("monument:");
        tagPrefixesToIgnore.add("motorcycle:");
        tagPrefixesToIgnore.add("motorboat:");
        tagPrefixesToIgnore.add("ref_");
        tagPrefixesToIgnore.add("cycleway:");
        tagPrefixesToIgnore.add("UntereDenkmalbehörde:");
        tagPrefixesToIgnore.add("xmas:");
        tagPrefixesToIgnore.add("VRS:");
        tagPrefixesToIgnore.add("WDPA-ID:");
        tagPrefixesToIgnore.add("WDPA_ID:");
        tagPrefixesToIgnore.add("WPDA_ID:");
        tagPrefixesToIgnore.add("WroclawGIS:");
        tagPrefixesToIgnore.add("access:");
        tagPrefixesToIgnore.add("add:");
        tagPrefixesToIgnore.add("addr.source:");
        tagPrefixesToIgnore.add("addr2:");
        tagPrefixesToIgnore.add("admin_level:");
        tagPrefixesToIgnore.add("administrative:");
        tagPrefixesToIgnore.add("adress:");
        tagPrefixesToIgnore.add("advertising:");
        tagPrefixesToIgnore.add("aed:");
        tagPrefixesToIgnore.add("construction:surface:");
        tagPrefixesToIgnore.add("construction:traffic_sign:");
        tagPrefixesToIgnore.add("construction:ref:");
        tagPrefixesToIgnore.add("construction:segregated:");
        tagPrefixesToIgnore.add("construction:service:");
        tagPrefixesToIgnore.add("construction:sidewalk:");
        tagPrefixesToIgnore.add("highway:");
        tagPrefixesToIgnore.add("motor_vehicle:conditional:");
        tagPrefixesToIgnore.add("motor_vehicle:conditional:");
        tagPrefixesToIgnore.add("aeowery:");
        tagPrefixesToIgnore.add("de:strassenschluessel:");
        tagPrefixesToIgnore.add("traffic_sign:");
        tagPrefixesToIgnore.add("construction:width:");
        tagPrefixesToIgnore.add("construction:bicycle:");
        tagPrefixesToIgnore.add("construction:bridge:");
        tagPrefixesToIgnore.add("construction:cycleway:");
        tagPrefixesToIgnore.add("construction:embankment:");
        tagPrefixesToIgnore.add("construction:foot:");
        tagPrefixesToIgnore.add("construction:lanes:");
        tagPrefixesToIgnore.add("construction:maxspeed:");
        tagPrefixesToIgnore.add("construction:note:");
        tagPrefixesToIgnore.add("usability:skate:");
        tagPrefixesToIgnore.add("aerasize:");
        tagPrefixesToIgnore.add("aerialway:");
        tagPrefixesToIgnore.add("aerodrome:");
        tagPrefixesToIgnore.add("aeroway:");
        tagPrefixesToIgnore.add("agricultural:");
        tagPrefixesToIgnore.add("aircraft:");
        tagPrefixesToIgnore.add("akn:");
        tagPrefixesToIgnore.add("all_you_can_eat:");
        tagPrefixesToIgnore.add("allotment:");
        tagPrefixesToIgnore.add("alt:");
        tagPrefixesToIgnore.add("alt_addr:");
        tagPrefixesToIgnore.add("alt_ele:");
        tagPrefixesToIgnore.add("amenity:");
        tagPrefixesToIgnore.add("animal:");
        tagPrefixesToIgnore.add("animal_");
        tagPrefixesToIgnore.add("animals:");
        tagPrefixesToIgnore.add("animals_name:");
        tagPrefixesToIgnore.add("antenna:");
        tagPrefixesToIgnore.add("apartment:");
        tagPrefixesToIgnore.add("architect:");
        tagPrefixesToIgnore.add("area:");
        tagPrefixesToIgnore.add("canoe:");
        tagPrefixesToIgnore.add("climbing:");
        tagPrefixesToIgnore.add("class:");
        tagPrefixesToIgnore.add("colour:");
        tagPrefixesToIgnore.add("currency:");
        tagPrefixesToIgnore.add("culvert:");
        return tagPrefixesToIgnore;
    }

    /**
     * Writes a tag override where a value that was "no" now becomes "yes"
     * @param printWriter
     * @param way
     * @param tag
     * @param timestamp
     */
    private void replaceNoByYes(PrintWriter printWriter, ReaderWay way, String tag, String timestamp) {
        if(containsTagWithValue(way, tag, "no")) {
            writeOverride(printWriter, way.getId(), tag, "yes", timestamp);
        }

    }

    private boolean containsPrefixToIgnore(String value, Set<String> prefixesToIgnore) {
        for(String prefix: prefixesToIgnore)
            if(value.startsWith(prefix))
                return true;
        return false;
    }

    /**
     * Preprocessing of OSM file to select nodes which are used for highways. This allows a more
     * compact graph data structure.
     */
    void preProcess(File osmFile) {
        OSMInputFile in = null;
        try {
            in = new OSMInputFile(osmFile).setWorkerThreads(workerThreads).open();

            long tmpWayCounter = 1;
            long tmpRelationCounter = 1;
            ReaderElement item;
            int nr = 0;
            while ((item = in.getNext()) != null) {
                if (item.isType(ReaderElement.WAY)) {
                    if(++nr%100000 == 0) System.out.println("Reading way "+nr);
                    final ReaderWay way = (ReaderWay) item;
                    boolean construction = isUnderConstruction(way);
                    boolean isNoAccess = containsTagWithValue(way, "access", "no");
                    boolean valid = filterWay(way);
                    if (valid || construction || isNoAccess) {

                        TLongList wayNodes = way.getNodes();
                        int s = wayNodes.size();
                        for (int index = 0; index < s; index++) {
                            prepareHighwayNode(wayNodes.get(index));
                        }
                        String splitPoints=way.getTag("BM_IntersectionPoint");
                        if(splitPoints!=null&&!splitPoints.isEmpty()){
                            if(!splitPoints.contains(".")){
//                                System.out.println("Warning: no splitpoints in string");
                            }else
                                SplitPointManager.registerSplit(way,splitPoints);
                        }
                        if (++tmpWayCounter % 5000000 == 0) {
                            LOGGER.info(nf(tmpWayCounter) + " (preprocess), osmIdMap:" + nf(getNodeMap().getSize()) + " ("
                                    + getNodeMap().getMemoryUsage() + "MB) " + Helper.getMemInfo());
                        }
                    }
                } else if (item.isType(ReaderElement.RELATION)) {
                    final ReaderRelation relation = (ReaderRelation) item;
                    if (!relation.isMetaRelation() && relation.hasTag("type", "route"))
                        prepareWaysWithRelationInfo(relation);

                    if (relation.hasTag("type", "restriction"))
                        prepareRestrictionRelation(relation);

                    if (++tmpRelationCounter % 50000 == 0) {
                        LOGGER.info(nf(tmpRelationCounter) + " (preprocess), osmWayMap:" + nf(getRelFlagsMap().size())
                                + " " + Helper.getMemInfo());
                    }
                } else if (item.isType(ReaderElement.FILEHEADER)) {
                    final OSMFileHeader fileHeader = (OSMFileHeader) item;
                    osmDataDate = Helper.createFormatter().parse(fileHeader.getTag("timestamp"));
                }

            }
            SplitPointManager.prepareSplitNodes(this);
        } catch (Exception ex) {
            throw new RuntimeException("Problem while parsing file", ex);
        } finally {
            Helper.close(in);
        }
    }

    private void prepareRestrictionRelation(ReaderRelation relation) {
        OSMTurnRelation turnRelation = createTurnRelation(relation);
        if (turnRelation != null) {
            getOsmWayIdSet().add(turnRelation.getOsmIdFrom());
            getOsmWayIdSet().add(turnRelation.getOsmIdTo());
        }
    }

    /**
     * @return all required osmWayIds to process e.g. relations.
     */
    private TLongSet getOsmWayIdSet() {
        return osmWayIdSet;
    }

    private TIntLongMap getEdgeIdToOsmWayIdMap() {
        if (edgeIdToOsmWayIdMap == null)
            edgeIdToOsmWayIdMap = new TIntLongHashMap(getOsmWayIdSet().size(), 0.5f, -1, -1);

        return edgeIdToOsmWayIdMap;
    }

    /**
     * Filter ways but do not analyze properties wayNodes will be filled with participating node
     * ids.
     * <p>
     *
     * @return true the current xml entry is a way entry and has nodes
     */
    boolean filterWay(ReaderWay item) {
        // ignore broken geometry
        if (item.getNodes().size() < 2)
            return false;

        // ignore multipolygon geometry
        if (!item.hasTags())
            return false;

        return encodingManager.acceptWay(item) > 0;
    }

    /**
     * Creates the edges and nodes files from the specified osm file.
     */
    private void writeOsm2Graph(File osmFile) {
        int tmp = (int) Math.max(getNodeMap().getSize() / 50, 100);
        LOGGER.info("creating graph. Found nodes (pillar+tower):" + nf(getNodeMap().getSize()) + ", " + Helper.getMemInfo());
        ghStorage.create(tmp);
        long wayStart = -1;
        long relationStart = -1;
        long counter = 1;
        OSMInputFile in = null;
        try {
            in = new OSMInputFile(osmFile).setWorkerThreads(workerThreads).open();
            LongIntMap nodeFilter = getNodeMap();
            SplitPointManager.registerNodes(this);
            ReaderElement item;
            while ((item = in.getNext()) != null) {
                switch (item.getType()) {
                    case ReaderElement.NODE:
                        if (nodeFilter.get(item.getId()) != -1) {
                            processNode((ReaderNode) item);
                        }
                        break;

                    case ReaderElement.WAY:
                        if (wayStart < 0) {
                            LOGGER.info(nf(counter) + ", now parsing ways");
                            wayStart = counter;
                        }
                        processWay((ReaderWay) item);
                        break;
                    case ReaderElement.RELATION:
                        if (relationStart < 0) {
                            LOGGER.info(nf(counter) + ", now parsing relations");
                            relationStart = counter;
                        }
                        processRelation((ReaderRelation) item);
                        break;
                    case ReaderElement.FILEHEADER:
                        break;
                    default:
                        throw new IllegalStateException("Unknown type " + item.getType());
                }
                if (++counter % 100000000 == 0) {
                    LOGGER.info(nf(counter) + ", locs:" + nf(locations) + " (" + skippedLocations + ") " + Helper.getMemInfo());
                }
            }

            // logger.info("storage nodes:" + storage.nodes() + " vs. graph nodes:" + storage.getGraph().nodes());
        } catch (Exception ex) {
            throw new RuntimeException("Couldn't process file " + osmFile + ", error: " + ex.getMessage(), ex);
        } finally {
            Helper.close(in);
        }

        finishedReading();
        if (graph.getNodes() == 0)
            throw new IllegalStateException("osm must not be empty. read " + counter + " lines and " + locations + " locations");
    }

    private List<EdgeIteratorState> edgesUnderConstruction;
    private Set<Integer> accessNoEdges;

    private boolean containsNonNullTag(ReaderWay way, String tagname) {
        Map<String, Object> tags = way.getTags();
        return tags.containsKey(tagname) && tags.get(tagname) != null;
    }

    private boolean containsTagWithValue(ReaderWay way, String tagname, String value) {
        Map<String, Object> tags = way.getTags();
        return tags.containsKey(tagname) && tags.get(tagname) != null && tags.get(tagname).equals(value);
    }

    private boolean isUnderConstruction(ReaderWay way) {
        return containsNonNullTag(way, "highway") && (containsTagWithValue(way,"highway", "construction") || containsTagWithValue(way, "construction", "yes") || containsTagWithValue(way, "construction:oneway", "yes"));
    }
    /**
     * Process properties, encode flags and create edges for the way.
     */
    void processWay(ReaderWay way) {
        if (way.getNodes().size() < 2)
            return;

        // ignore multipolygon geometry
        if (!way.hasTags())
            return;


        boolean underConstruction = isUnderConstruction(way);
        boolean accessNo = containsTagWithValue(way, "access", "no");

        long wayOsmId = way.getId();
        long includeWay = encodingManager.acceptWay(way);

        // normally such edges are not included but for this specific application we are adding them so we can route over them
        if(underConstruction || accessNo)
            includeWay = 1;

        if (includeWay == 0)
            return;

        long relationFlags = getRelFlagsMap().get(way.getId());

        // TODO move this after we have created the edge and know the coordinates => encodingManager.applyWayTags
        TLongList osmNodeIds = way.getNodes();
        
        // Estimate length of ways containing a route tag e.g. for ferry speed calculation
        if (osmNodeIds.size() > 1) {
            int first = getNodeMap().get(osmNodeIds.get(0));
            int last = getNodeMap().get(osmNodeIds.get(osmNodeIds.size() - 1));
            double firstLat = getTmpLatitude(first), firstLon = getTmpLongitude(first);
            double lastLat = getTmpLatitude(last), lastLon = getTmpLongitude(last);
            if (!Double.isNaN(firstLat) && !Double.isNaN(firstLon) && !Double.isNaN(lastLat) && !Double.isNaN(lastLon)) {
                double estimatedDist = distCalc.calcDist(firstLat, firstLon, lastLat, lastLon);
                // Add artificial tag for the estimated distance and center
                way.setTag("estimated_distance", estimatedDist);
                way.setTag("estimated_center", new GHPoint((firstLat + lastLat) / 2, (firstLon + lastLon) / 2));
            }
        }

        if (way.getTag("duration") != null) {
            try {
                long dur = OSMTagParser.parseDuration(way.getTag("duration"));
                // Provide the duration value in seconds in an artificial graphhopper specific tag:
                way.setTag("duration:seconds", Long.toString(dur));
            } catch (Exception ex) {
                LOGGER.warn("Parsing error in way with OSMID=" + way.getId() + " : " + ex.getMessage());
            }
        }
//        if(way.getTag("BM_IntersectionPoint")!=null){
//            ccToAdjust.put(way.getId(),way.getTag("BM_IntersectionPoint"));
//        }
//        if(way.getTag("BM_CC")!=null){
//            String cc=way.getTag("BM_CC");
//            if(cc.contains(";"))cc=cc.substring(0,cc.indexOf(";"));
//            if(cc.contains(","))cc=cc.substring(0,cc.indexOf(","));
//            way.setTag("countryCode",cc);
//        }
        long wayFlags = encodingManager.handleWayTags(way, includeWay, relationFlags);
        if (wayFlags == 0)
            return;
        boolean hasSplitNodes=SplitPointManager.hasSplitNodes(way.getId());
        if(hasSplitNodes){
            osmNodeIds=SplitPointManager.injectSplitNodes(osmNodeIds,way.getId());
        }
        String typeString=way.getTag("highway");
        int type=getRoadTypeIndexForRoadType(typeString);
        List<EdgeIteratorState> createdEdges = new ArrayList<EdgeIteratorState>();
        // look for barriers along the way
        final int size = osmNodeIds.size();
        int lastBarrier = -1;
        for (int i = 0; i < size; i++) {
            long nodeId = osmNodeIds.get(i);
            long nodeFlags = getNodeFlagsMap().get(nodeId);
            // barrier was spotted and way is otherwise passable for that mode of travel
            if (nodeFlags > 0) {
                if ((nodeFlags & wayFlags) > 0) {
                    // remove barrier to avoid duplicates
                    getNodeFlagsMap().put(nodeId, 0);

                    // create shadow node copy for zero length edge
                    long newNodeId = addBarrierNode(nodeId);
                    if (i > 0) {
                        // start at beginning of array if there was no previous barrier
                        if (lastBarrier < 0)
                            lastBarrier = 0;

                        // add way up to barrier shadow node
                        long transfer[] = osmNodeIds.toArray(lastBarrier, i - lastBarrier + 1);
                        transfer[transfer.length - 1] = newNodeId;
                        TLongList partIds = new TLongArrayList(transfer);
                        createdEdges.addAll(addOSMWay(partIds, wayFlags, wayOsmId,type));

                        // create zero length edge for barrier
                        createdEdges.addAll(addBarrierEdge(newNodeId, nodeId, wayFlags, nodeFlags, wayOsmId,type));
                    } else {
                        // run edge from real first node to shadow node
                        createdEdges.addAll(addBarrierEdge(nodeId, newNodeId, wayFlags, nodeFlags, wayOsmId,type));

                        // exchange first node for created barrier node
                        osmNodeIds.set(0, newNodeId);
                    }
                    int newGHNodeID=getNodeMap().get(newNodeId);
                    SplitPointManager.registerAliasNode(nodeId,newNodeId,getNodeMap().get(nodeId),newGHNodeID);

                    // remember barrier for processing the way behind it
                    lastBarrier = i;
                }
            }
        }

        // just add remainder of way to graph if barrier was not the last node
        if (lastBarrier >= 0) {
            if (lastBarrier < size - 1) {
                long transfer[] = osmNodeIds.toArray(lastBarrier, size - lastBarrier);
                TLongList partNodeIds = new TLongArrayList(transfer);
                createdEdges.addAll(addOSMWay(partNodeIds, wayFlags, wayOsmId,type));
            }
        } else {
            // no barriers - simply add the whole way
            createdEdges.addAll(addOSMWay(osmNodeIds, wayFlags, wayOsmId,type));
//            createdEdges.addAll(addOSMWay(way.getNodes(), wayFlags, wayOsmId,type));
        }

        for (EdgeIteratorState edge : createdEdges) {
            encodingManager.applyWayTags(way, edge);
        }

        // because we need to store both the edges which are under construction or have access no now
        // (this information is not stored by default with the edges)
        if(underConstruction) {
            edgesUnderConstruction.addAll(createdEdges);
        }
        if(accessNo) {
            for(EdgeIteratorState edgeIteratorState: createdEdges) {
                accessNoEdges.add(edgeIteratorState.getEdge());
            }
        }

        if(hasSplitNodes){
            SplitPointManager.correctCountryCodes(createdEdges,way.getId(),this);
//        }else{
//            String cc=way.getTag("BM_CC");
//            if(cc.contains(","))cc=cc.substring(0,cc.indexOf(","));
//            for (EdgeIteratorState createdEdge : createdEdges) {
//                createdEdge.setCountryCode(cc);
//            }
//            
        }
    }

    public void processRelation(ReaderRelation relation) throws XMLStreamException {
        if (relation.hasTag("type", "restriction")) {
            OSMTurnRelation turnRelation = createTurnRelation(relation);
            if (turnRelation != null) {
                GraphExtension extendedStorage = graph.getExtension();
                if (extendedStorage instanceof TurnCostExtension) {
                    TurnCostExtension tcs = (TurnCostExtension) extendedStorage;
                    Collection<TurnCostTableEntry> entries = analyzeTurnRelation(turnRelation);
                    for (TurnCostTableEntry entry : entries) {
                        tcs.addTurnInfo(entry.edgeFrom, entry.nodeVia, entry.edgeTo, entry.flags);
                    }
                }
            }
        }
    }

    public Collection<TurnCostTableEntry> analyzeTurnRelation(OSMTurnRelation turnRelation) {
        TLongObjectMap<TurnCostTableEntry> entries = new TLongObjectHashMap<OSMTurnRelation.TurnCostTableEntry>();

        for (FlagEncoder encoder : encodingManager.fetchEdgeEncoders()) {
            for (TurnCostTableEntry entry : analyzeTurnRelation(encoder, turnRelation)) {
                TurnCostTableEntry oldEntry = entries.get(entry.getItemId());
                if (oldEntry != null) {
                    // merging different encoders
                    oldEntry.flags |= entry.flags;
                } else {
                    entries.put(entry.getItemId(), entry);
                }
            }
        }

        return entries.valueCollection();
    }

    public Collection<TurnCostTableEntry> analyzeTurnRelation(FlagEncoder encoder, OSMTurnRelation turnRelation) {
        if (!encoder.supports(TurnWeighting.class))
            return Collections.emptyList();

        EdgeExplorer edgeOutExplorer = outExplorerMap.get(encoder);
        EdgeExplorer edgeInExplorer = inExplorerMap.get(encoder);

        if (edgeOutExplorer == null || edgeInExplorer == null) {
            edgeOutExplorer = graph.createEdgeExplorer(new DefaultEdgeFilter(encoder, false, true));
            outExplorerMap.put(encoder, edgeOutExplorer);

            edgeInExplorer = graph.createEdgeExplorer(new DefaultEdgeFilter(encoder, true, false));
            inExplorerMap.put(encoder, edgeInExplorer);
        }
        return turnRelation.getRestrictionAsEntries(encoder, edgeOutExplorer, edgeInExplorer, this);
    }

    /**
     * @return OSM way ID from specified edgeId. Only previously stored OSM-way-IDs are returned in
     * order to reduce memory overhead.
     */
    public long getOsmIdOfInternalEdge(int edgeId) {
        return getEdgeIdToOsmWayIdMap().get(edgeId);
    }

    public int getInternalNodeIdOfOsmNode(long nodeOsmId) {
        int id = getNodeMap().get(nodeOsmId);
        if (id < TOWER_NODE)
            return -id - 3;

        return EMPTY;
    }

    // TODO remove this ugly stuff via better preparsing phase! E.g. putting every tags etc into a helper file!
    double getTmpLatitude(int id) {
        if (id == EMPTY)
            return Double.NaN;
        if (id < TOWER_NODE) {
            // tower node
            id = -id - 3;
            return nodeAccess.getLatitude(id);
        } else if (id > -TOWER_NODE) {
            // pillar node
            id = id - 3;
            return pillarInfo.getLatitude(id);
        } else
            // e.g. if id is not handled from preparse (e.g. was ignored via isInBounds)
            return Double.NaN;
    }

    double getTmpLongitude(int id) {
        if (id == EMPTY)
            return Double.NaN;
        if (id < TOWER_NODE) {
            // tower node
            id = -id - 3;
            return nodeAccess.getLongitude(id);
        } else if (id > -TOWER_NODE) {
            // pillar node
            id = id - 3;
            return pillarInfo.getLon(id);
        } else
            // e.g. if id is not handled from preparse (e.g. was ignored via isInBounds)
            return Double.NaN;
    }

    protected void processNode(ReaderNode node) {
        if (isInBounds(node)) {
            addNode(node);

            // analyze node tags for barriers
            if (node.hasTags()) {
                long nodeFlags = encodingManager.handleNodeTags(node);
                if (nodeFlags != 0)
                    getNodeFlagsMap().put(node.getId(), nodeFlags);
            }
            locations++;
        } else {
            skippedLocations++;
        }
    }

    boolean addNode(ReaderNode node) {
        int nodeType = getNodeMap().get(node.getId());
        if (nodeType == EMPTY)
            return false;

        double lat = node.getLat();
        double lon = node.getLon();
        double ele = getElevation(node);
        if (nodeType == TOWER_NODE) {
            addTowerNode(node.getId(), lat, lon, ele);
        } else if (nodeType == PILLAR_NODE) {
            pillarInfo.setNode(nextPillarId, lat, lon, ele);
            getNodeMap().put(node.getId(), nextPillarId + 3);
            nextPillarId++;
        }
        return true;
    }

    protected double getElevation(ReaderNode node) {
        return eleProvider.getEle(node.getLat(), node.getLon());
    }

    void prepareWaysWithRelationInfo(ReaderRelation osmRelation) {
        // is there at least one tag interesting for the registed encoders?
        if (encodingManager.handleRelationTags(osmRelation, 0) == 0)
            return;

        int size = osmRelation.getMembers().size();
        for (int index = 0; index < size; index++) {
            ReaderRelation.Member member = osmRelation.getMembers().get(index);
            if (member.getType() != ReaderRelation.Member.WAY)
                continue;

            long osmId = member.getRef();
            long oldRelationFlags = getRelFlagsMap().get(osmId);

            // Check if our new relation data is better comparated to the the last one
            long newRelationFlags = encodingManager.handleRelationTags(osmRelation, oldRelationFlags);
            if (oldRelationFlags != newRelationFlags)
                getRelFlagsMap().put(osmId, newRelationFlags);
        }
    }

    void prepareHighwayNode(long osmId) {
        int tmpIndex = getNodeMap().get(osmId);
        if (tmpIndex == EMPTY) {
            // osmId is used exactly once
            getNodeMap().put(osmId, PILLAR_NODE);
        } else if (tmpIndex > EMPTY) {
            // mark node as tower node as it occured at least twice times
            getNodeMap().put(osmId, TOWER_NODE);
        } else {
            // tmpIndex is already negative (already tower node)
        }
    }

    int addTowerNode(long osmId, double lat, double lon, double ele) {
        if (nodeAccess.is3D())
            nodeAccess.setNode(nextTowerId, lat, lon, ele);
        else
            nodeAccess.setNode(nextTowerId, lat, lon);

        int id = -(nextTowerId + 3);
        getNodeMap().put(osmId, id);
        nextTowerId++;
        return id;
    }

    /**
     * This method creates from an OSM way (via the osm ids) one or more edges in the graph.
     */
    Collection<EdgeIteratorState> addOSMWay(final TLongList osmNodeIds, final long flags, final long wayOsmId,int type) {
        PointList pointList = new PointList(osmNodeIds.size(), nodeAccess.is3D());
        List<EdgeIteratorState> newEdges = new ArrayList<EdgeIteratorState>(5);
        int firstNode = -1;
        int lastIndex = osmNodeIds.size() - 1;
        int lastInBoundsPillarNode = -1;

        try {
            for (int i = 0; i < osmNodeIds.size(); i++) {
                long osmId = osmNodeIds.get(i);
                int tmpNode = getNodeMap().get(osmId);
                if (tmpNode == EMPTY)
                    continue;

                // skip osmIds with no associated pillar or tower id (e.g. !OSMReader.isBounds)
                if (tmpNode == TOWER_NODE)
                    continue;

                if (tmpNode == PILLAR_NODE) {
                    // In some cases no node information is saved for the specified osmId.
                    // ie. a way references a <node> which does not exist in the current file.
                    // => if the node before was a pillar node then convert into to tower node (as it is also end-standing).
                    if (!pointList.isEmpty() && lastInBoundsPillarNode > -TOWER_NODE) {
                        // transform the pillar node to a tower node
                        tmpNode = lastInBoundsPillarNode;
                        tmpNode = handlePillarNode(tmpNode, osmId, null, true);
                        tmpNode = -tmpNode - 3;
                        if (pointList.getSize() > 1 && firstNode >= 0) {
                            // TOWER node
                            newEdges.add(addEdge(firstNode, tmpNode, pointList, flags, wayOsmId,type));
                            pointList.clear();
                            pointList.add(nodeAccess, tmpNode);
                        }
                        firstNode = tmpNode;
                        lastInBoundsPillarNode = -1;
                    }
                    continue;
                }

                if (tmpNode <= -TOWER_NODE && tmpNode >= TOWER_NODE)
                    throw new AssertionError("Mapped index not in correct bounds " + tmpNode + ", " + osmId);

                if (tmpNode > -TOWER_NODE) {
                    boolean convertToTowerNode = i == 0 || i == lastIndex;
                    if (!convertToTowerNode) {
                        lastInBoundsPillarNode = tmpNode;
                    }

                    // PILLAR node, but convert to towerNode if end-standing
                    tmpNode = handlePillarNode(tmpNode, osmId, pointList, convertToTowerNode);
                }

                if (tmpNode < TOWER_NODE) {
                    // TOWER node
                    tmpNode = -tmpNode - 3;
                    pointList.add(nodeAccess, tmpNode);
                    if (firstNode >= 0) {
                        newEdges.add(addEdge(firstNode, tmpNode, pointList, flags, wayOsmId,type));
                        pointList.clear();
                        pointList.add(nodeAccess, tmpNode);
                    }
                    firstNode = tmpNode;
                }
            }
        } catch (RuntimeException ex) {
            LOGGER.error("Couldn't properly add edge with osm ids:" + osmNodeIds, ex);
            if (exitOnlyPillarNodeException)
                throw ex;
        }
        return newEdges;
    }

    EdgeIteratorState addEdge(int fromIndex, int toIndex, PointList pointList, long flags, long wayOsmId,int type) {
        // sanity checks
        if (fromIndex < 0 || toIndex < 0)
            throw new AssertionError("to or from index is invalid for this edge " + fromIndex + "->" + toIndex + ", points:" + pointList);
        if (pointList.getDimension() != nodeAccess.getDimension())
            throw new AssertionError("Dimension does not match for pointList vs. nodeAccess " + pointList.getDimension() + " <-> " + nodeAccess.getDimension());

        double towerNodeDistance = 0;
        double prevLat = pointList.getLatitude(0);
        double prevLon = pointList.getLongitude(0);
        double prevEle = pointList.is3D() ? pointList.getElevation(0) : Double.NaN;
        double lat, lon, ele = Double.NaN;
        PointList pillarNodes = new PointList(pointList.getSize() - 2, nodeAccess.is3D());
        int nodes = pointList.getSize();
        for (int i = 1; i < nodes; i++) {
            // we could save some lines if we would use pointList.calcDistance(distCalc);
            lat = pointList.getLatitude(i);
            lon = pointList.getLongitude(i);
            if (pointList.is3D()) {
                ele = pointList.getElevation(i);
                if (!distCalc.isCrossBoundary(lon, prevLon))
                    towerNodeDistance += distCalc3D.calcDist(prevLat, prevLon, prevEle, lat, lon, ele);
                prevEle = ele;
            } else if (!distCalc.isCrossBoundary(lon, prevLon))
                towerNodeDistance += distCalc.calcDist(prevLat, prevLon, lat, lon);

            prevLat = lat;
            prevLon = lon;
            if (nodes > 2 && i < nodes - 1) {
                if (pillarNodes.is3D())
                    pillarNodes.add(lat, lon, ele);
                else
                    pillarNodes.add(lat, lon);
            }
        }
        if (towerNodeDistance < 0.0001) {
            // As investigation shows often two paths should have crossed via one identical point 
            // but end up in two very close points.
            zeroCounter++;
            towerNodeDistance = 0.0001;
        }

        double maxDistance = (Integer.MAX_VALUE - 1) / 1000d;
        if (Double.isNaN(towerNodeDistance)) {
            LOGGER.warn("Bug in OSM or GraphHopper. Illegal tower node distance " + towerNodeDistance + " reset to 1m, osm way " + wayOsmId);
            towerNodeDistance = 1;
        }

        if (Double.isInfinite(towerNodeDistance) || towerNodeDistance > maxDistance) {
            // Too large is very rare and often the wrong tagging. See #435 
            // so we can avoid the complexity of splitting the way for now (new towernodes would be required, splitting up geometry etc)
            LOGGER.warn("Bug in OSM or GraphHopper. Too big tower node distance " + towerNodeDistance + " reset to large value, osm way " + wayOsmId);
            towerNodeDistance = maxDistance;
        }

        EdgeIteratorState iter = graph.edge(fromIndex, toIndex).setDistance(towerNodeDistance).setFlags(flags).setType(type);

        if (nodes > 2) {
            if (doSimplify)
                simplifyAlgo.simplify(pillarNodes);

            iter.setWayGeometry(pillarNodes);
        }
        storeOsmWayID(iter.getEdge(), wayOsmId);
        return iter;
    }

    /**
     * Stores only osmWayIds which are required for relations
     */
    protected void storeOsmWayID(int edgeId, long osmWayId) {
        edgeIDsToWayIDs.put(edgeId, osmWayId);
        if (getOsmWayIdSet().contains(osmWayId)) {
            getEdgeIdToOsmWayIdMap().put(edgeId, osmWayId);
        }
    }

    /**
     * @return converted tower node
     */
    private int handlePillarNode(int tmpNode, long osmId, PointList pointList, boolean convertToTowerNode) {
        tmpNode = tmpNode - 3;
        double lat = pillarInfo.getLatitude(tmpNode);
        double lon = pillarInfo.getLongitude(tmpNode);
        double ele = pillarInfo.getElevation(tmpNode);
        if (lat == Double.MAX_VALUE || lon == Double.MAX_VALUE)
            throw new RuntimeException("Conversion pillarNode to towerNode already happended!? "
                    + "osmId:" + osmId + " pillarIndex:" + tmpNode);

        if (convertToTowerNode) {
            // convert pillarNode type to towerNode, make pillar values invalid
            pillarInfo.setNode(tmpNode, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
            tmpNode = addTowerNode(osmId, lat, lon, ele);
        } else if (pointList.is3D())
            pointList.add(lat, lon, ele);
        else
            pointList.add(lat, lon);

        return (int) tmpNode;
    }

    protected void finishedReading() {
        adjustWayCountryCodes();
        printInfo("way");
        pillarInfo.clear();
        eleProvider.release();
        osmNodeIdToInternalNodeMap = null;
        osmNodeIdToNodeFlagsMap = null;
        osmWayIdToRouteWeightMap = null;
        osmWayIdSet = null;
        edgeIdToOsmWayIdMap = null;
    }

    /**
     * Create a copy of the barrier node
     */
    long addBarrierNode(long nodeId) {
        ReaderNode newNode;
        int graphIndex = getNodeMap().get(nodeId);
        if (graphIndex < TOWER_NODE) {
            graphIndex = -graphIndex - 3;
            newNode = new ReaderNode(createNewNodeId(), nodeAccess, graphIndex);
        } else {
            graphIndex = graphIndex - 3;
            newNode = new ReaderNode(createNewNodeId(), pillarInfo, graphIndex);
        }

        final long id = newNode.getId();
        prepareHighwayNode(id);
        addNode(newNode);
        return id;
    }

    private long createNewNodeId() {
        return newUniqueOsmId++;
    }

    /**
     * Add a zero length edge with reduced routing options to the graph.
     */
    Collection<EdgeIteratorState> addBarrierEdge(long fromId, long toId, long flags, long nodeFlags, long wayOsmId,int type) {
        // clear barred directions from routing flags
        flags &= ~nodeFlags;
        // add edge
        barrierNodeIds.clear();
        barrierNodeIds.add(fromId);
        barrierNodeIds.add(toId);
        return addOSMWay(barrierNodeIds, flags, wayOsmId,type);
    }

    /**
     * Creates an OSM turn relation out of an unspecified OSM relation
     * <p>
     *
     * @return the OSM turn relation, <code>null</code>, if unsupported turn relation
     */
    OSMTurnRelation createTurnRelation(ReaderRelation relation) {
        OSMTurnRelation.Type type = OSMTurnRelation.Type.getRestrictionType(relation.getTag("restriction"));
        if (type != OSMTurnRelation.Type.UNSUPPORTED) {
            long fromWayID = -1;
            long viaNodeID = -1;
            long toWayID = -1;

            for (ReaderRelation.Member member : relation.getMembers()) {
                if (ReaderElement.WAY == member.getType()) {
                    if ("from".equals(member.getRole())) {
                        fromWayID = member.getRef();
                    } else if ("to".equals(member.getRole())) {
                        toWayID = member.getRef();
                    }
                } else if (ReaderElement.NODE == member.getType() && "via".equals(member.getRole())) {
                    viaNodeID = member.getRef();
                }
            }
            if (fromWayID >= 0 && toWayID >= 0 && viaNodeID >= 0) {
                return new OSMTurnRelation(fromWayID, viaNodeID, toWayID, type);
            }
        }
        return null;
    }

    /**
     * Filter method, override in subclass
     */
    boolean isInBounds(ReaderNode node) {
        return true;
    }

    /**
     * Maps OSM IDs (long) to internal node IDs (int)
     */
    protected LongIntMap getNodeMap() {
        return osmNodeIdToInternalNodeMap;
    }

    protected TLongLongMap getNodeFlagsMap() {
        return osmNodeIdToNodeFlagsMap;
    }

    TLongLongHashMap getRelFlagsMap() {
        return osmWayIdToRouteWeightMap;
    }

    /**
     * Specify the type of the path calculation (car, bike, ...).
     */
    @Override
    public OSMReader setEncodingManager(EncodingManager em) {
        this.encodingManager = em;
        return this;
    }

    @Override
    public OSMReader setWayPointMaxDistance(double maxDist) {
        doSimplify = maxDist > 0;
        simplifyAlgo.setMaxDistance(maxDist);
        return this;
    }

    @Override
    public OSMReader setWorkerThreads(int numOfWorkers) {
        this.workerThreads = numOfWorkers;
        return this;
    }

    @Override
    public OSMReader setElevationProvider(ElevationProvider eleProvider) {
        if (eleProvider == null)
            throw new IllegalStateException("Use the NOOP elevation provider instead of null or don't call setElevationProvider");

        if (!nodeAccess.is3D() && ElevationProvider.NOOP != eleProvider)
            throw new IllegalStateException("Make sure you graph accepts 3D data");

        this.eleProvider = eleProvider;
        return this;
    }

    @Override
    public DataReader setFile(File osmFile) {
        this.osmFile = osmFile;
        return this;
    }

    private void printInfo(String str) {
        LOGGER.info("finished " + str + " processing." + " nodes: " + graph.getNodes()
                + ", osmIdMap.size:" + getNodeMap().getSize() + ", osmIdMap:" + getNodeMap().getMemoryUsage() + "MB"
                + ", nodeFlagsMap.size:" + getNodeFlagsMap().size() + ", relFlagsMap.size:" + getRelFlagsMap().size()
                + ", zeroCounter:" + zeroCounter
                + " " + Helper.getMemInfo());
    }

    @Override
    public Date getDataDate() {
        return osmDataDate;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
    private static Map<String,Integer> typeMapping;
    private static Map<Integer,String> reverseTypeMapping;
    private static void initTypeMapping(){
        typeMapping=new HashMap<>();
        reverseTypeMapping=new HashMap<>();
        registerTypeMapping("motorway",1);
        registerTypeMapping("motorway_link",2);
        registerTypeMapping("primary",3);
        registerTypeMapping("primary_link",4);
        registerTypeMapping("secondary",5);
        registerTypeMapping("secondary_link",6);
        registerTypeMapping("tertiary",7);
        registerTypeMapping("tertiary_link",8);
        registerTypeMapping("trunk",9);
        registerTypeMapping("trunk_link",10);
        registerTypeMapping("unclassified",11);
        registerTypeMapping("residential",12);
        registerTypeMapping("service",13);
        registerTypeMapping("living_street",14);
        registerTypeMapping("pedestrian",15);
        registerTypeMapping("track",16);
        registerTypeMapping("bus_guideway",17);
        registerTypeMapping("escape",18);
        registerTypeMapping("road",19);
        registerTypeMapping("footway",20);
        registerTypeMapping("bridleway",21);
        registerTypeMapping("steps",22);
        registerTypeMapping("path",23);
        registerTypeMapping("cycleway",24);
        registerTypeMapping("",100);
        registerTypeMapping(null,404);
    }
    private static void registerTypeMapping(String type, int index){
        typeMapping.put(type,index);
        reverseTypeMapping.put(index,type);
    }
    public static int getRoadTypeIndexForRoadType(String roadType){
        if(typeMapping==null){
            initTypeMapping();
        }
        Integer ri= typeMapping.get(roadType);
        if(ri==null)return -1;
        return ri;
    }
    public static String getRoadTypeForRoadTypeIndex(int roadTypeIndex){
        if(reverseTypeMapping==null){
            initTypeMapping();
        }
        return reverseTypeMapping.get(roadTypeIndex);
    }

    private void adjustWayCountryCodes() {
        TLongObjectIterator<String> iterator = ccToAdjust.iterator();
        while(iterator.hasNext()){
            iterator.advance();
            long wayID= iterator.key();
            String splitpoint=iterator.value();
            
        }
    }
}
