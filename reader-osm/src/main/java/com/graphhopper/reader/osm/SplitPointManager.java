package com.graphhopper.reader.osm;

import com.graphhopper.coll.LongIntMap;
import com.graphhopper.reader.ReaderNode;
import com.graphhopper.reader.ReaderWay;
import static com.graphhopper.reader.osm.OSMReader.EMPTY;
import static com.graphhopper.reader.osm.OSMReader.TOWER_NODE;
import com.graphhopper.util.EdgeIteratorState;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.iterator.TLongDoubleIterator;
import gnu.trove.iterator.TLongIterator;
import gnu.trove.iterator.TLongObjectIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.TLongList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongDoubleMap;
import gnu.trove.map.TLongIntMap;
import gnu.trove.map.TLongLongMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongDoubleHashMap;
import gnu.trove.map.hash.TLongIntHashMap;
import gnu.trove.map.hash.TLongLongHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import java.util.List;

/**
 * Handles country codes around borders where extra nodes are needed for splitting
 * @author Maarten
 */
class SplitPointManager {
    static long addedIDs=0;
    static TLongObjectMap<String>map=new TLongObjectHashMap<>();
    static TLongObjectMap<String>mapcodes=new TLongObjectHashMap<>();
    static TLongObjectMap<TLongList>nodesForWay=new TLongObjectHashMap<>();
    static TLongObjectMap<TLongList>transitionsForWay=new TLongObjectHashMap<>();
    static TLongLongMap aliasesOSM=new TLongLongHashMap();
    static TIntIntMap aliasesGH=new TIntIntHashMap();
    //store info of new nodes here temporarily
    static TLongDoubleMap lats=new TLongDoubleHashMap();
    static TLongDoubleMap lons=new TLongDoubleHashMap();
    static void registerSplit(ReaderWay way,String splitPoints) {
        map.put(way.getId(), splitPoints);
        mapcodes.put(way.getId(),way.getTag("BM_CC"));
        
    }

    static void prepareSplitNodes(OSMReader osmr) {
        LongIntMap nodeMap = osmr.getNodeMap();
        long temp=1;
        TLongObjectIterator<String> iterator = map.iterator();
        
        while(iterator.hasNext()){
            TLongList nodes=new TLongArrayList();
            iterator.advance();
            long wayID=iterator.key();
            String splitPoints=iterator.value();
            
            String[] s=splitPoints.split(",");
            
            for (int i = 0; i < s.length; i++) {
                String string = s[i];
                long id;
                if(string.contains(".")){
                    //found a lat
                    double lat=Double.parseDouble(string);
                    double lon=Double.parseDouble(s[i+1]);
                    do{
                        int tmpIndex = nodeMap.get(temp);
                        if (tmpIndex == EMPTY) break;
                        else temp++;
                    }while(true);
                    
                    addedIDs++;
                    lats.put(temp,lat);
                    lons.put(temp,lon);
                    nodeMap.put(temp, TOWER_NODE);
                    id=temp;
                    i++;
                }else{
                    id=Long.parseLong(string);
                }
                nodes.add(id);  
            }            
            nodesForWay.put(wayID, nodes);

//            String cc=mapCodes.get(wayID);
//            String[] countrycodes=cc.split(",");
            
           
        }
    }

    static boolean hasSplitNodes(long id) {
//        return false;
        return map.containsKey(id);
    }
    static void registerNodes(OSMReader osmr){
        TLongDoubleIterator iterator = lats.iterator();
        while(iterator.hasNext()){
            iterator.advance();;
            long nodeID=iterator.key();
            double lat=iterator.value();
            double lon=lons.get(nodeID);
            ReaderNode rn=new ReaderNode(nodeID, lat, lon);
            osmr.processNode(rn);
        }
        lats.clear();
        lons.clear();
    }

    static TLongList injectSplitNodes(TLongList osmNodeIds, long id) {
        return nodesForWay.get(id);
    }

    static void correctCountryCodes(List<EdgeIteratorState> createdEdges,long id,OSMReader osmr) {

        String codes=mapcodes.get(id);
        TLongList nodes=nodesForWay.get(id);
        TIntList ghIDsForNodes=getNodeIDs(osmr,nodes);
        TIntIntMap nodeIDToIndex=convert(ghIDsForNodes);
        String splitPoints=map.get(id);
        TIntObjectMap<String> countryCodeForLinkInNode=combine(ghIDsForNodes,codes);
//        if(createdEdges.size()!=ghIDsForNodes.size()){
//            System.out.println("Warning: edges ("+createdEdges.size()+") != original nodes("+ghIDsForNodes.size()+")");
//        }
        for (EdgeIteratorState createdEdge : createdEdges) {
            int bn=createdEdge.getBaseNode();
            int adjn=createdEdge.getAdjNode();
            int indexBaseNode=nodeIDToIndex.get(bn);
            int indexAdjacentNode=nodeIDToIndex.get(adjn);
            int minId=indexBaseNode<indexAdjacentNode?bn:adjn;
            String code=countryCodeForLinkInNode.get(minId);
            if(code==null||code.isEmpty()||code.equalsIgnoreCase("")){
                System.out.println("Warning: setting empty code");
            }
            createdEdge.setCountryCode(code);
        }
    }

    private static TIntList getNodeIDs(OSMReader osmr, TLongList osmNodeIDs) {
        LongIntMap nodeMap = osmr.getNodeMap();
        TIntList l=new TIntArrayList();
        TLongIterator it=osmNodeIDs.iterator();
        while(it.hasNext()){
            long osmNodeID=it.next();
//            if(aliasesOSM.containsKey(osmNodeID)){
//                osmNodeID=aliasesOSM.get(osmNodeID);
//            }
            int ghID=nodeMap.get(osmNodeID);
            
            if(ghID<0){
                ghID=-ghID-3;
            }
            
            l.add(ghID);
        }
        return l;
    }

    private static TIntObjectMap<String> combine(TIntList ghIDsForNodes, String codes) {
        String[]cc=codes.split(",");
        TIntObjectMap<String> r=new TIntObjectHashMap<>();
//        TIntIterator iterator = ghIDsForNodes.iterator();
//        int index=0;
        for (int i = 0; i < cc.length; i++) {
            String ccForLink = cc[i];
            r.put(ghIDsForNodes.get(i),ccForLink);
        }
        r.put(ghIDsForNodes.get(cc.length),cc[cc.length-1]);
//        while(iterator.hasNext()){
//            r.put(iterator.next(),cc[index]);
//            index++;
//        }
        return r;
    }

    private static TIntIntMap convert(TIntList ghIDsForNodes) {
        TIntIntMap r=new TIntIntHashMap();
        TIntIterator iterator = ghIDsForNodes.iterator();
        int index=0;
        while(iterator.hasNext()){
            int ghID=iterator.next();
            r.put(ghID,index);
            if(aliasesGH.containsKey(ghID)){
                r.put(aliasesGH.get(ghID),index);
            }
            index++;
        }
        return r;
    }

    static void registerAliasNode(long nodeId, long newNodeId,int ghNodeID,int newGHNodeID) {
        aliasesOSM.put(nodeId,newNodeId);
        int a=-ghNodeID-3;
        int b=-newGHNodeID-3;
        aliasesGH.put(a,b);
        
    }

}
