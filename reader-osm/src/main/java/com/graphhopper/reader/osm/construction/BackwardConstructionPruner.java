package com.graphhopper.reader.osm.construction;

import com.graphhopper.routing.util.DefaultEdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;

import java.util.Set;

/**
 * Created by Stéphanie Vanhove on 18/04/2018.
 */
public class BackwardConstructionPruner implements Pruner {

    private FlagEncoder flagEncoder;

    private Set<Integer> edgesUnderConstruction;
    private Set<Integer> accessNoEdges;

    public BackwardConstructionPruner(FlagEncoder flagEncoder, Set<Integer> edgesUnderConstruction, Set<Integer> accessNoEdges) {
        this.flagEncoder = flagEncoder;
        this.edgesUnderConstruction = edgesUnderConstruction;
        this.accessNoEdges = accessNoEdges;
    }

    private boolean isUnderConstruction(EdgeIteratorState edgeIteratorState){
        return edgesUnderConstruction.contains(edgeIteratorState.getEdge());
    }

    /**
     * Prunes whenever there is another backward edge which is not blocked or forbidden incident with the adjacent node of the edgeIteratorstate.
     * The idea is that this edge can be used to possibly re-enter the original path after circumventing a blockage.
     * @param g
     * @param edgeIteratorState
     * @return
     */
    @Override
    public boolean prune(Graph g, EdgeIteratorState edgeIteratorState) {

        EdgeExplorer edgeExplorer = g.createEdgeExplorer(new DefaultEdgeFilter(flagEncoder, true, false));

        EdgeIterator edgeIterator = edgeExplorer.setBaseNode(edgeIteratorState.getAdjNode());
        while(edgeIterator.next()) {
            if(edgeIterator.getEdge() != edgeIteratorState.getEdge()) {
                if (!isUnderConstruction(edgeIterator.detach(false)) && !accessNoEdges.contains(edgeIterator.getEdge())) { // T
                    return true; // outgoing edge found ("escape" forward)
                }
            }
        }
        return false;
    }
}
