package com.graphhopper.reader.osm.construction;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.EdgeIteratorState;


import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Stéphanie Vanhove on 18/04/2018.
 */
public interface ConstructionHandler {

    /**
     * Returns the OSM way ID's which are under construction (can be more than the one who were marked in OSM as under construction,
     * eg. for connectivity reasons)
     * @param graph
     * @param edgesUnderConstruction
     * @param accessNoEdges
     * @param edgeIDsToWayIDs
     * @param flagEncoder
     * @return
     */
    Set<Long> handleConstruction(Graph graph, List<EdgeIteratorState> edgesUnderConstruction, Set<Integer> accessNoEdges, Map<Integer, Long> edgeIDsToWayIDs, FlagEncoder flagEncoder);

}
