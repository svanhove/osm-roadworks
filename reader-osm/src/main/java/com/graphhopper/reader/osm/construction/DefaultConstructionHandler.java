package com.graphhopper.reader.osm.construction;

import com.graphhopper.routing.OneToAllDijkstra;
import com.graphhopper.routing.util.DefaultEdgeFilter;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.weighting.ShortestWeighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.*;

import java.util.*;

/**
 * Finds all way id 's which should be considered "under construction" even if they were not marked as such in OSM,
 * eg. for connectivity reasons.
 */
public class DefaultConstructionHandler implements ConstructionHandler {

    protected int nextConstructionNr = 0;

    protected Set<EdgeIteratorState> edgesVisited;
    // stores for every edge ID the construction it belongs to
    protected Map<Integer, Integer> constructionPerEdgeID;
    // stores for every node the construction it belongs to
    protected Map<Integer, Integer> constructionPerNode;
    // stores all edges which are part of it for every construction
    protected Map<Integer, Set<Integer>> edgesPerConstruction;
    // stores all nodes which are part of it for every construction
    protected Map<Integer, Set<Integer>> nodesPerConstruction;

    // contains the nearby nodes for every construction, i.e. they are at a give maximum distance (so we can check if other blocked roads should be included in the construction too)
    protected Map<Integer, Set<Integer>> nearbyNodesPerConstructionMap;
    // for every node which is near a construction, stores the nearby construction for this node
    protected Map<Integer, Integer> nearbyConstructionPerNode;
    // the IDs of the edges which were originally given as under construction (probably those which were tagged as under construction in osm)
    protected Set<Integer> originalEdgeIDsUnderConstruction;

    protected static final String kmlLocation = "C:/Stephanie/"; // change to your location


    /**
     * Adds the given node to the construction with the given construction number.
     * @param construction
     * @param node
     */
    private void addNodeToConstruction(int construction, int node) {
        constructionPerNode.put(node, construction);
        nodesPerConstruction.get(construction).add(node);
    }

    /**
     * Adds the given edge iD to the construction with the given construction number.
     * @param construction
     * @param edgeID
     */
    private void addEdgeToConstruction(int construction, int edgeID) {
        constructionPerEdgeID.put(edgeID, construction);
        edgesPerConstruction.get(construction).add(edgeID);
    }

    @Override
    public Set<Long> handleConstruction(Graph graph, List<EdgeIteratorState> edgesUnderConstruction, Set<Integer> accessNoEdges, Map<Integer, Long> edgeIDsToWayIDs, FlagEncoder flagEncoder) {

        initCollections(edgesUnderConstruction);

        // we have a queue of edges which need to be examined, start by adding all edges which are marked as under construction in OSM
        Queue<EdgeIteratorState> edgesToExamine = new LinkedList<>();
        edgesToExamine.addAll(edgesUnderConstruction);

        Set<Integer> alreadyExamined = new HashSet<>();

        int number = 0;
        while(!edgesToExamine.isEmpty()) {
//            if(number++ % 100 == 0) System.out.println("Examining edge "+number+" - left in queue at this point: "+edgesToExamine.size());

            // checks if more edges are disconnected because of this and, if so, adds them to the construction
            expandConstructionForEdge(graph, accessNoEdges, flagEncoder, edgesToExamine, alreadyExamined);
        }

        EdgeExplorer edgeExplorer = graph.createEdgeExplorer(new DefaultEdgeFilter(flagEncoder, true, true));

        // find all nodes which need to be examined further, i.e. the nodes which have incident edges which are not (yet) in the construction
        // since they might lead to edges which cannot be reached
        Set<Integer> nodesToExamineFurther = findNodesToExamineFurther(edgeExplorer);

        // find nearby nodes per construction (for possible "access no" edges)
        Set<Integer> allReachedNodes = findNearbyNodesPerConstruction(graph, flagEncoder, nodesToExamineFurther, 500);

        // find nearby edges with access no (should be incident with one of the nearby nodes per construction
        addNearbyAccessNoEdges(accessNoEdges, edgesToExamine, edgeExplorer, allReachedNodes);

//        KmlHelper.writeEdgeIteratorStates(edgesToExamine, graph, kmlLocation+"nearbyAccessNoEdges");


        // now examine all the nearby access no edges and treat them like they are constructions
        while(!edgesToExamine.isEmpty()) {
            expandConstructionForEdge(graph, accessNoEdges, flagEncoder, edgesToExamine, alreadyExamined);
        }

        // if parts of the network become disconnected because of the construction, add them to the construction
        ensureConnectivity(nodesToExamineFurther, graph, flagEncoder);

        // now convert the found edge IDs to their corresponding way IDs
        Set<Long> wayIDsUnderConstruction = new HashSet<>();

        for(int edgeIDUnderConstruction: constructionPerEdgeID.keySet()) {
            wayIDsUnderConstruction.add(edgeIDsToWayIDs.get(edgeIDUnderConstruction));
        }

        return wayIDsUnderConstruction;



    }



    private void writeKmls(Graph graph, Map<Integer, Long> edgeIDsToWayIDs, Set<Integer> nodesToExamineFurther) {

        Set<Integer> uniqueEdgesToWrite = new HashSet<>();
        for(EdgeIteratorState edgeIteratorState: edgesVisited) {
            if(!originalEdgeIDsUnderConstruction.contains(edgeIteratorState.getEdge()))
                uniqueEdgesToWrite.add(edgeIteratorState.getEdge());
        }

        KmlHelper.writeNodes(nodesToExamineFurther, graph, kmlLocation+"nodesToExamine");
        KmlHelper.writeEdgeIDs(uniqueEdgesToWrite, graph, kmlLocation+"DFS_VisitedEdges");
        KmlHelper.writeNodes(constructionPerNode.keySet(), graph, kmlLocation+"DFS_VisitedNodes");

        Map<EdgeIteratorState, String> forKml = new HashMap<>();
        for(int constructionNr: edgesPerConstruction.keySet()) {
            for(Integer edge: edgesPerConstruction.get(constructionNr)) {
                forKml.put(graph.getEdgeIteratorState(edge, Integer.MIN_VALUE), constructionNr+"");
            }
        }

        Map<EdgeIteratorState, String> forKml2 = new HashMap<>();
        for(int constructionNr: edgesPerConstruction.keySet()) {
            for(Integer edge: edgesPerConstruction.get(constructionNr)) {
                forKml2.put(graph.getEdgeIteratorState(edge, Integer.MIN_VALUE), edgeIDsToWayIDs.get(edge)+"");
            }
        }
        KmlHelper.writeEdgeIteratorStates(forKml, graph, kmlLocation+"constructions");
        KmlHelper.writeEdgeIteratorStates(forKml2, graph, kmlLocation+"constructionWayIDs");
    }

    private void addNearbyAccessNoEdges(Set<Integer> accessNoEdges, Queue<EdgeIteratorState> edgesToExamine, EdgeExplorer edgeExplorer, Set<Integer> allReachedNodes) {
        Set<Integer> nearbyAccessNoEdges = new HashSet<>(); // set containing only id's to avoid examining the same one twice (eg in both directions)

        for(int node: allReachedNodes) {
            EdgeIterator edgeIterator = edgeExplorer.setBaseNode(node);
            while(edgeIterator.next()) {
                int edge = edgeIterator.getEdge();
                if((accessNoEdges.contains(edge)) && !constructionPerEdgeID.containsKey(edge) && !nearbyAccessNoEdges.contains(edge)) { // use also a set with only the edge ID 's to avoid having edgeiteratorstates for the same edge in both directions
                    edgesToExamine.add(edgeIterator.detach(false));
                    nearbyAccessNoEdges.add(edge);
                }
            }
        }
    }

    // find all nodes which need to be examined further, i.e. the nodes which have incident edges which are not (yet) in the construction
    // since they might lead to edges which cannot be reached
    private Set<Integer> findNodesToExamineFurther(EdgeExplorer edgeExplorer) {
//        System.out.println("\nFind nodes to examine further...");
        Set<Integer> nodesToExamineFurther = new HashSet<>();
        for(int node: constructionPerNode.keySet()) {
            EdgeIterator edgeIterator = edgeExplorer.setBaseNode(node);
            while(edgeIterator.next()) {
                // edge is not under construction
                if(!constructionPerEdgeID.containsKey(edgeIterator.getEdge())) {
                    nodesToExamineFurther.add(node);
                }
            }
        }
        return nodesToExamineFurther;
    }


    /**
     * Finds all nodes which are within the given distance of any construction (and are not in a construction yet)
     * @param graph
     * @param flagEncoder
     * @param nodesToExamineFurther
     * @param distance
     * @return
     */
    private Set<Integer> findNearbyNodesPerConstruction(Graph graph, FlagEncoder flagEncoder, Set<Integer> nodesToExamineFurther, double distance) {
        Set<Integer> allReachedNodes = new HashSet<>();
        for(int node: nodesToExamineFurther) {

            Set<Integer> reachedNodesForward = findNodesWithinRadius(graph, flagEncoder, node, false, distance);
            Set<Integer> reachedNodesBackward = findNodesWithinRadius(graph, flagEncoder, node, true, distance);
            Set<Integer> reachedNodes = new HashSet<>();
            reachedNodes.addAll(reachedNodesForward);
            reachedNodes.addAll(reachedNodesBackward);
            allReachedNodes.addAll(reachedNodes);

            for(int reachedNode: reachedNodes) {
                if (!constructionPerNode.containsKey(reachedNode)) {
                    Integer construction = constructionPerNode.get(node);
                    nearbyConstructionPerNode.put(reachedNode, construction);
                    if (!nearbyNodesPerConstructionMap.containsKey(construction))
                        nearbyNodesPerConstructionMap.put(construction, new HashSet<Integer>());
                    nearbyNodesPerConstructionMap.get(construction).add(reachedNode);
                }
            }
        }
        return allReachedNodes;
    }

    private void initCollections(List<EdgeIteratorState> edgesUnderConstruction) {
        originalEdgeIDsUnderConstruction = new HashSet<>();
        for(EdgeIteratorState edgeIteratorState: edgesUnderConstruction) {
            originalEdgeIDsUnderConstruction.add(edgeIteratorState.getEdge());
        }
        nearbyConstructionPerNode = new HashMap<>();
        edgesVisited = new HashSet<>();
        constructionPerEdgeID = new HashMap<>();
        edgesPerConstruction = new HashMap<>();
        constructionPerNode = new HashMap<>();
        nodesPerConstruction = new HashMap<>();
        nearbyNodesPerConstructionMap = new HashMap<>();
    }

    /**
     * Find possible "isolated" small groups of nodes and add them to the construction.
     * @param nodesToExamineMore
     * @param graph
     * @param flagEncoder
     */
    private void ensureConnectivity(Set<Integer> nodesToExamineMore, Graph graph, FlagEncoder flagEncoder){

        Set<Integer> edgesAddedForConnectivity = new HashSet<>();
        EdgeExplorer edgeExplorer = graph.createEdgeExplorer(new DefaultEdgeFilter(flagEncoder, true, true));
        int groupNr = 0;
        List<SortedSet<Integer>> constructionsToMerge = new ArrayList<>();
        Set<Integer> startNodes = new HashSet<>();
        Set<Integer> unreachedNodes = new HashSet<>();
        // first group them by construction
        Map<Integer, Set<Integer>> nodesToExamineMorePerConstruction = new HashMap<>();
        for(int node: nodesToExamineMore) {
            int construction = constructionPerNode.get(node);
            if(!nodesToExamineMorePerConstruction.containsKey(construction)) {
                nodesToExamineMorePerConstruction.put(construction, new HashSet<Integer>());
            }
            nodesToExamineMorePerConstruction.get(construction).add(node);
        }

        List<ConnectivityGroup> allConnectivityGroups = new ArrayList<>();

        for(int construction: nodesToExamineMorePerConstruction.keySet()) {
//            System.out.println("\nLooking at construction "+construction);
            List<Integer> nodesToReach = new ArrayList<>(nodesToExamineMorePerConstruction.get(construction));
            double totalDistance = 0;
            Set<Integer> edgeIDs = edgesPerConstruction.get(construction);
            for(int edgeID: edgeIDs) {
                totalDistance += graph.getEdgeIteratorState(edgeID, Integer.MIN_VALUE).getDistance();
            }

            Set<Integer> notFound = new HashSet<>(nodesToReach);

            double radius = Math.max(5*totalDistance, 10000);

            EdgeFilter additionalEdgeFilter = new EdgeFilter() {
                @Override
                public boolean accept(EdgeIteratorState edgeState) {
                    return !constructionPerEdgeID.containsKey(edgeState.getEdge());
                }
            };

            List<ConnectivityGroup> connectivityGroups = new ArrayList<>();

            while(!notFound.isEmpty()) {

                int startNode = chooseStartNode(notFound);
                startNodes.add(startNode);

                OneToAllDijkstra dijkstraForward = new OneToAllDijkstra(graph, new ShortestWeighting(flagEncoder), radius, false);
                dijkstraForward.setEdgeFilter(additionalEdgeFilter);
                dijkstraForward.run(startNode);

                OneToAllDijkstra dijkstraBackward = new OneToAllDijkstra(graph, new ShortestWeighting(flagEncoder), radius, true);
                dijkstraBackward.setEdgeFilter(additionalEdgeFilter);
                dijkstraBackward.run(startNode);

                Set<Integer> found = new HashSet<>();
                for(int node: notFound) {
                    if(dijkstraForward.getReachedNodes().contains(node) || dijkstraBackward.getReachedNodes().contains(node)) {
                        found.add(node);
                    }
                }
                Set<Integer> allNodesReached = new HashSet<>();
                allNodesReached.addAll(dijkstraForward.getReachedNodes());
                allNodesReached.addAll(dijkstraBackward.getReachedNodes());
                notFound.removeAll(found);
                // did not cover the entire distance and also found less than half of the nodes in the graph (in case the graph is too small for actually reaching such a distance)
                boolean isolated = Math.max(dijkstraForward.getFurthestDistance(), dijkstraBackward.getFurthestDistance()) < 0.8 * radius && allNodesReached.size() < graph.getNodes()/2;
                connectivityGroups.add(new ConnectivityGroup(found, isolated, allNodesReached));

                allConnectivityGroups.addAll(connectivityGroups);

            }
//            System.out.println("Connectivity groups for construction "+construction+" ("+connectivityGroups.size()+"):");
//            for(ConnectivityGroup connectivityGroup: connectivityGroups) {
//                System.out.println("  "+connectivityGroup);
//            }
            SortedSet<Integer> toMerge = new TreeSet<>();
            for(ConnectivityGroup connectivityGroup: connectivityGroups) {

                if(connectivityGroup.isIsolated()) {

                    //add the node to the construction
                    for(Integer node: connectivityGroup.allReachedNodes) {
                        if(!constructionPerNode.containsKey(node)) {
                            addNodeToConstruction(construction, node);
                        }
                        else if(constructionPerNode.get(node) != construction) {
                            toMerge.add(construction);
                            toMerge.add(constructionPerNode.get(node));
                        }
                        EdgeIterator edgeIterator = edgeExplorer.setBaseNode(node);
                        // add all its incident edges to the construction
                        while(edgeIterator.next()) {
                            int edgeID = edgeIterator.getEdge();
                            if(!constructionPerEdgeID.containsKey(edgeID)) {
                                addEdgeToConstruction(construction, edgeID);
                                edgesAddedForConnectivity.add(edgeID);
                            }
                        }
                    }
                    if(!toMerge.isEmpty())
                        constructionsToMerge.add(toMerge);
                }
            }


        }

//        System.out.println("\n\nConstructions to merge:\n\n");
//        System.out.println("size: "+constructionsToMerge.size());
//        for(SortedSet<Integer> set: constructionsToMerge){
//            System.out.println(set);
//        }
//        System.out.println();
//        System.out.println();

        mergeOverlaps(constructionsToMerge);
//        System.out.println("\n\nConstructions to merge:\n\n");
//        System.out.println("size: "+constructionsToMerge.size());
//        for(SortedSet<Integer> set: constructionsToMerge){
//            System.out.println(set);
//        }
//        System.out.println();
//        System.out.println();

        for(SortedSet<Integer> set: constructionsToMerge)
            mergeConstructions(set);

        Collections.shuffle(allConnectivityGroups);
        Map<Integer, String> forKml = new HashMap<>();
        int nr = 0;
        for(ConnectivityGroup connectivityGroup: allConnectivityGroups) {
            for(Integer node: connectivityGroup.getNodes()){
                forKml.put(node, nr+"");
            }
            if(connectivityGroup.isIsolated()) {
                for(Integer node: connectivityGroup.allReachedNodes)
                    forKml.put(node, nr+"");
            }
            nr++;
        }
//        KmlHelper.writeNodes(forKml, graph, kmlLocation+"connectivityGroups");
//        KmlHelper.writeNodes(startNodes, graph, kmlLocation+"dijkstraStartNodes");
//        KmlHelper.writeEdgeIDs(edgesAddedForConnectivity, graph, kmlLocation+"edgesAddedForConnectivity");
//        KmlHelper.writeNodes(unreachedNodes, graph, kmlLocation+"dijkstraNotFound");



    }

    private int chooseStartNode(Set<Integer> set) {
        return set.iterator().next();
    }

    /*
     * HELPER METHODS FOR MERGING CONSTRUCTIONS
     */

    private static boolean overlap(Collection c1, Collection c2) {
        for(Object o: c2)
            if(c1.contains(o))
                return true;
        return false;
    }

    private static void mergeOverlaps(List<SortedSet<Integer>> list) {
        for(int i = list.size()-1; i >= 0; i--) {
            for (int j = i-1; j >=0 ; j--) {
                SortedSet<Integer> set1 = list.get(i);
                SortedSet<Integer> set2 = list.get(j);
                if(overlap(set1, set2)){
                    set2.addAll(set1);
                    list.remove(i);
                    break;
                }
            }
        }
    }


    /**
     * Group of nodes that are connected.
     */
    private class ConnectivityGroup {

        // We were testing for certain nodes if they can still reach each other.
        // This is the subset of such nodes which are in this specific connectivity group.
        private Set<Integer> nodes;

        // If this is a "small" connectivity group, isolated from the larger part of the network.
        private boolean isolated;

        // These are all the nodes which can be reached in this connectivity group (not necessarilly the ones we were actually testing)/
        private Set<Integer> allReachedNodes;


        public ConnectivityGroup(Set<Integer> nodes, boolean isolated, Set<Integer> allReachedNodes) {
            this.nodes = nodes;
            this.isolated = isolated;
            this.allReachedNodes = allReachedNodes;
        }

        public Set<Integer> getNodes() {
            return nodes;
        }

        public void setNodes(Set<Integer> nodes) {
            this.nodes = nodes;
        }

        public boolean isIsolated() {
            return isolated;
        }



        @Override
        public String toString() {
            return "ConnectivityGroup{" +
                    "nodes=" + nodes +
                    ", isIsolated=" + isolated +
                    ", totalNrNodesReached=" + allReachedNodes.size() +
                    '}';
        }
    }


    /**
     * Polls one edge from the EdgeIteratorStates to examine and expands the construction accordingly
     * (by adding edges which can no longer be reached because of the construction)
     * @param graph
     * @param accessNoEdges
     * @param flagEncoder
     * @param edgesToExamine
     * @param alreadyExamined
     */
    private void expandConstructionForEdge(Graph graph, Set<Integer> accessNoEdges, FlagEncoder flagEncoder, Queue<EdgeIteratorState> edgesToExamine, Set<Integer> alreadyExamined) {
        EdgeIteratorState edgeIteratorState = edgesToExamine.poll();
        if(alreadyExamined.contains(edgeIteratorState.getEdge())) {
            return;
        }
        alreadyExamined.add(edgeIteratorState.getEdge());

        Set<Integer> visitedNodes = new HashSet<>();
        Set<EdgeIteratorState> visitedEdges = new HashSet<>();


        if(edgeIteratorState.isForward(flagEncoder)) {
            runDepthFirstSearch(graph, accessNoEdges, flagEncoder, true, edgeIteratorState, edgeIteratorState.getAdjNode(), visitedNodes, visitedEdges);
            updateConstructions(edgesToExamine, alreadyExamined, visitedNodes, visitedEdges);
            runDepthFirstSearch(graph, accessNoEdges, flagEncoder, false, edgeIteratorState.detach(true), edgeIteratorState.getBaseNode(), visitedNodes, visitedEdges);
            updateConstructions(edgesToExamine, alreadyExamined, visitedNodes, visitedEdges);
        }

        if(edgeIteratorState.isBackward(flagEncoder)) {
            runDepthFirstSearch(graph, accessNoEdges, flagEncoder, false, edgeIteratorState, edgeIteratorState.getAdjNode(), visitedNodes, visitedEdges);
            updateConstructions(edgesToExamine, alreadyExamined, visitedNodes, visitedEdges);
            runDepthFirstSearch(graph, accessNoEdges, flagEncoder, true, edgeIteratorState.detach(true), edgeIteratorState.getBaseNode(), visitedNodes, visitedEdges);
            updateConstructions(edgesToExamine, alreadyExamined, visitedNodes, visitedEdges);
        }

    }

    /**
     * Searches all nodes within the given radius from the given start node (forward or backward) but prunes any edges which are
     * already under construction.
     * @param graph
     * @param flagEncoder
     * @param startnode
     * @param forward
     * @param radius
     * @return
     */
    private Set<Integer> findNodesWithinRadius(Graph graph, FlagEncoder flagEncoder, int startnode, boolean forward, double radius) {
        Set<Integer> reachedNodes = new HashSet<>();
        OneToAllDijkstra oneToAllDijkstra = new OneToAllDijkstra(graph, new ShortestWeighting(flagEncoder), radius, forward);
        oneToAllDijkstra.setEdgeFilter(new EdgeFilter() {
            @Override
            public boolean accept(EdgeIteratorState edgeState) {
                return !constructionPerEdgeID.containsKey(edgeState.getEdge());
            }
        });
        oneToAllDijkstra.run(startnode);
        reachedNodes.addAll(oneToAllDijkstra.getReachedNodes());

        return reachedNodes;
    }

    /**
     * After running a depth first search from an edge part of a construction (or considered to be part of it), update the constructions
     * with the found edges and nodes.
     * The found edges and nodes can be added to already existing constructions or a new construction number can be started.
     * Where necessary, constructions are also merged.
     * @param edgesToExamine
     * @param alreadyExamined
     * @param visitedNodes
     * @param visitedEdges
     */
    private void updateConstructions(Queue<EdgeIteratorState> edgesToExamine, Set<Integer> alreadyExamined, Set<Integer> visitedNodes, Set<EdgeIteratorState> visitedEdges) {
        SortedSet<Integer> reachedConstructions = new TreeSet<>();

        for(int node: visitedNodes) {
            if(constructionPerNode.containsKey(node)) {
                reachedConstructions.add(constructionPerNode.get(node));
            }
            else if(nearbyConstructionPerNode.containsKey(node))
                reachedConstructions.add(nearbyConstructionPerNode.get(node));
        }

        int constructionNr = !reachedConstructions.isEmpty() ? reachedConstructions.first() : ++nextConstructionNr;


//        System.out.println("reachedConstructions = " + reachedConstructions);

        for(int node: visitedNodes) {
            constructionPerNode.put(node, constructionNr);
            if(!nodesPerConstruction.containsKey(constructionNr))
                nodesPerConstruction.put(constructionNr, new HashSet<Integer>());
            nodesPerConstruction.get(constructionNr).add(node);
        }

        for(EdgeIteratorState visitedEdge: visitedEdges) {
//            System.out.println("reached edge "+visitedEdge+", putting it in construction "+constructionNr);
            edgesVisited.add(visitedEdge);
            constructionPerEdgeID.put(visitedEdge.getEdge(), constructionNr);
            if(!edgesPerConstruction.containsKey(constructionNr))
                edgesPerConstruction.put(constructionNr, new HashSet<Integer>());
            edgesPerConstruction.get(constructionNr).add(visitedEdge.getEdge());
        }

        mergeConstructions(reachedConstructions);

        for(EdgeIteratorState edge: visitedEdges) {
            if(!alreadyExamined.contains(edge.getEdge()))
                edgesToExamine.add(edge);
        }

    }

    /**
     * Merges the given constructions into one construction (with one single construction number)
     * @param constructionsToMerge
     */
    private void mergeConstructions(SortedSet<Integer> constructionsToMerge) {

        if(constructionsToMerge.isEmpty()) {
            return;
        }
        Iterator<Integer> iterator = constructionsToMerge.iterator();
        int targetConstructionNr = iterator.next();


        while(iterator.hasNext()) {
            int constructionNr = iterator.next();

            Set<Integer> edgesToMove = edgesPerConstruction.get(constructionNr);
            edgesPerConstruction.remove(constructionNr);

            edgesPerConstruction.get(targetConstructionNr).addAll(edgesToMove);
            for(Integer edgeToMove: edgesToMove) {
                constructionPerEdgeID.put(edgeToMove, targetConstructionNr);
            }
            Set<Integer> nodesToMove = nodesPerConstruction.get(constructionNr);
            nodesPerConstruction.remove(constructionNr);
            nodesPerConstruction.get(targetConstructionNr).addAll(nodesToMove);
            for(Integer nodeToMove: nodesToMove) {
                constructionPerNode.put(nodeToMove, targetConstructionNr);
            }

            Set<Integer> nearbyNodesToMove = nearbyNodesPerConstructionMap.get(constructionNr);
            if(nearbyNodesToMove != null) { // not all constructions have such nearby nodes
                nearbyNodesPerConstructionMap.remove(constructionNr);
                if (!nearbyNodesPerConstructionMap.containsKey(targetConstructionNr))
                    nearbyNodesPerConstructionMap.put(targetConstructionNr, new HashSet<Integer>());
                nearbyNodesPerConstructionMap.get(targetConstructionNr).addAll(nearbyNodesToMove);
                for (Integer nearbyNodeToMove : nearbyNodesToMove) {
                    nearbyConstructionPerNode.put(nearbyNodeToMove, targetConstructionNr);
                }
            }
        }
    }

    private void runDepthFirstSearch(Graph graph, Set<Integer> accessNoEdges, FlagEncoder flagEncoder, boolean forward, EdgeIteratorState edgeIteratorState, int startNode, Set<Integer> visitedNodes, Set<EdgeIteratorState> visitedEdges) {
        DepthFirstSearch depthFirstSearch = new DepthFirstSearch(graph, startNode, edgeIteratorState, constructionPerEdgeID.keySet(), forward, flagEncoder);
        if(forward)
            depthFirstSearch.addPruner(new BackwardConstructionPruner(flagEncoder, constructionPerEdgeID.keySet(), accessNoEdges));
        else
            depthFirstSearch.addPruner(new ForwardConstructionPruner(flagEncoder, constructionPerEdgeID.keySet(), accessNoEdges));
        depthFirstSearch.run();
        visitedNodes.addAll(depthFirstSearch.getNodesVisited());
        visitedEdges.addAll(depthFirstSearch.getEdgesVisited());

    }


}
