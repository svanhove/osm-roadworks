package com.graphhopper.reader.osm.construction;

import com.graphhopper.routing.util.DefaultEdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;

import java.util.*;

/**
 * Depth first search which allows to specify blocked edges and to add pruners.
 * Developed for use by the DefaultConstructionHandler to identify edges affected by constructions.
 */
public class DepthFirstSearch {

    private Graph graph;
    private int startNode;
    private List<Pruner> pruners;
    private Stack<Integer> stack;
    private EdgeExplorer edgeExplorer;
    private EdgeIteratorState parentEdge;

    private Set<Integer> nodesVisited;
    private Set<EdgeIteratorState> edgesVisited;
    private Set<Integer> blockedEdges;

    public DepthFirstSearch(Graph graph, int startNode, EdgeIteratorState parentEdge, Set<Integer> blockedEdges, boolean forward, FlagEncoder flagEncoder) {
        this.graph = graph;
        this.startNode = startNode;
        this.parentEdge = parentEdge;
        this.blockedEdges = blockedEdges;
        pruners = new ArrayList<>();
        stack = new Stack<>();
        edgeExplorer = forward ? graph.createEdgeExplorer(new DefaultEdgeFilter(flagEncoder, false, true)) :
                graph.createEdgeExplorer(new DefaultEdgeFilter(flagEncoder, true, false));

        nodesVisited = new HashSet<>();
        edgesVisited = new HashSet<>();
    }

    public void addPruner(Pruner pruner) {
        pruners.add(pruner);
    }

    private boolean prune(EdgeIteratorState edgeIteratorState)  {
        for(Pruner pruner: pruners)
            if(pruner.prune(graph, edgeIteratorState))
                return true;
        return false;
    }

    public void run(){
        edgesVisited.add(parentEdge);
        nodesVisited.add(parentEdge.getBaseNode());
        nodesVisited.add(parentEdge.getAdjNode());
        if(!prune(parentEdge)) {
            stack.push(startNode);
            nodesVisited.add(startNode);
        }
        while(!stack.isEmpty()) {
            int currentNode = stack.pop();
            EdgeIterator edgeIterator = edgeExplorer.setBaseNode(currentNode);
            while (edgeIterator.next()) {
                if(!blockedEdges.contains(edgeIterator.getEdge())) {
                    EdgeIteratorState edgeIteratorState = edgeIterator.detach(false);
                    edgesVisited.add(edgeIteratorState);
                    boolean notYetVisited = nodesVisited.add(edgeIterator.getAdjNode());

                    // if not finished in the adjacent node, add to stack for further search
                    if (notYetVisited && !prune(edgeIteratorState)) {

                        stack.push(edgeIterator.getAdjNode());
                    }
                }
            }
        }
    }

    public void writeVisited(){
        KmlHelper.writeEdgeIteratorStates(edgesVisited, graph, "C:/Stephanie/DFS_VisitedEdges");
        KmlHelper.writeNodes(nodesVisited, graph, "C:/Stephanie/DFS_VisitedNodes");
    }

    public Set<Integer> getNodesVisited() {
        return nodesVisited;
    }

    public Set<EdgeIteratorState> getEdgesVisited() {
        return edgesVisited;
    }
}
