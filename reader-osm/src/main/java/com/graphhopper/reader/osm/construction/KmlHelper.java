package com.graphhopper.reader.osm.construction;


import com.graphhopper.routing.Path;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.GHPoint;

import com.graphhopper.util.shapes.GHPoint3D;
import de.micromata.opengis.kml.v_2_2_0.*;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

/**
 * Helper methods to write points and lines to KML (for debug purposes)
 */
public class KmlHelper {

    /**
     * Writes the given point to a kml file with the given name.
     */
    public static void writeNodes(int node, Graph graph, String filename) {
        filename = fixFilename(filename);
        writeNodes(Collections.singleton(node), graph, filename);
    }



    /**
     * Writes the given points to a kml file with the given name.
     */
    public static void writeNodes(String filename, Graph g, Integer... nodes) {
        List<Integer> list = new ArrayList<>();
        for(int nodeID: nodes)
            list.add(nodeID);
        writeNodes(list, g, filename);
    }


    /**
     * Writes the given nodes to a kml file with the given name.
     */
    public static void writeNodes(Collection<Integer> nodes, Graph g, String filename) {
        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writeNodes(nodes, g, outputStream);
            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void writePoints(Collection<GHPoint> points, String filename) {

        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writePoints(points, outputStream);
            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Writes the given nodes to kml (to System.out, not to a file)
     */
    public static void writeNodes(Collection<Integer> nodes, Graph g) {
        writeNodes(nodes, g, System.out);
    }

    public static void writeEdgeIDs(Collection<Integer> edges, Graph g, String filename) {
        Collection<EdgeIteratorState> edgeIteratorStates = new HashSet<>();
        for(int edge: edges){
            edgeIteratorStates.add(g.getEdgeIteratorState(edge, Integer.MIN_VALUE));
        }
        writeEdgeIteratorStates(edgeIteratorStates, g, filename);
    }

    public static void writeEdgeIteratorStates(Collection<EdgeIteratorState> edgeIteratorStates, Graph g, String filename) {
        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writeEdgeIteratorStates(edgeIteratorStates, g, outputStream);

            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void writePath(Path path, Graph g, String filename) {
        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writePath(path, g, outputStream);

            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the given lines to a kml with the given filename.
     */
    public static void writeLines(Collection<Integer> lines, Graph g,  String filename) {
        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writeLines(lines, g, outputStream);
            System.out.println("writing "+lines.size()+" lines to "+filename);
            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the given lines as a kml to the given output stream.
     */
    public static void writeLines(Collection<Integer> lines, Graph g, OutputStream outputStream) {
        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();
        for(int lineID: lines) {
            document.addToFeature(createLineString(lineID, g, lineID+"", lineID+""));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    private static String getType(EdgeIteratorState edgeIteratorState) {
        return OSMTypeMapping.getType(edgeIteratorState.getType());
    }

    /**
     * Writes the given lines as a kml to the given output stream.
     */
    public static void writeEdgeIteratorStates(Collection<EdgeIteratorState> edgeIteratorStates, Graph g, OutputStream outputStream) {
        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();
        for(EdgeIteratorState edgeIteratorState: edgeIteratorStates) {
            document.addToFeature(createLineString(edgeIteratorState, g, edgeIteratorState.getEdge()+"", getType(edgeIteratorState)+""));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the given nodes to a kml file with the given name.
     */
    public static void writeNodes(Map<Integer, String> nodes, Graph g, String filename) {
        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writeNodes(nodes, g, outputStream);
            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * Writes the given lines as a kml to the given file
     */
    public static void writeEdgeIteratorStates(Map<EdgeIteratorState, String> edgeIteratorStates, Graph g, String filename) {

        filename = fixFilename(filename);
        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            writeEdgeIteratorStates(edgeIteratorStates, g, outputStream);

            outputStream.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Writes the given nodes as a kml to the given output stream.
     */
    public static void writeNodes(Map<Integer, String> nodes, Graph g, OutputStream outputStream) {

        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();

        NodeAccess nodeAccess = g.getNodeAccess();

        for(int nodeId: nodes.keySet()) {
            document.addToFeature(createPoint(nodeAccess.getLon(nodeId), nodeAccess.getLat(nodeId), nodeId+"", nodes.get(nodeId), nodeId+""));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Writes the given lines as a kml to the given output stream.
     */
    public static void writeEdgeIteratorStates(Map<EdgeIteratorState, String> edgeIteratorStates, Graph g, OutputStream outputStream) {
        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();
        for(EdgeIteratorState edgeIteratorState: edgeIteratorStates.keySet()) {
            document.addToFeature(createLineString(edgeIteratorState, g, edgeIteratorState.getEdge()+"", edgeIteratorStates.get(edgeIteratorState)));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the given lines as a kml to the given output stream.
     */
    public static void writePath(Path path, Graph g, OutputStream outputStream) {
        writeEdgeIteratorStates(path.calcEdges(), g, outputStream);
    }

    public static void writePoints(Collection<GHPoint> points, OutputStream outputStream) {

        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();

        for(GHPoint point: points) {

            document.addToFeature(createPoint(point.getLon(), point.getLat(), "", "", ""));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Writes the given nodes as a kml to the given output stream.
     */
    public static void writeNodes(Collection<Integer> nodeIds, Graph g, OutputStream outputStream) {

        Kml kml = new Kml();
        Document document = kml.createAndSetDocument();

        NodeAccess nodeAccess = g.getNodeAccess();

        for(int nodeId: nodeIds) {
            document.addToFeature(createPoint(nodeAccess.getLon(nodeId), nodeAccess.getLat(nodeId), nodeId+"", nodeId+"", nodeId+""));
        }
        try {
            kml.marshal(outputStream);
        }
        catch(IOException e) {
            e.printStackTrace();
        }

    }

    public static Placemark createPoint(final double longitude,
                                        final double latitude, final String id, final String name,
                                        final String description) {

        Placemark placemark = new Placemark()
                .withName(name)
                .withDescription(description);
        Point point = placemark.createAndSetPoint().withId(id);
        point.addToCoordinates(longitude, latitude);
        return placemark;
    }



    /**
     * Helper method: adds the extension *.kml to a filename if it is not already present.
     */
    private static String fixFilename(String file) {
        if(!file.endsWith(".kml"))
            file = file + ".kml";
        return file;
    }


    public static Placemark createLineString(
            int edge, Graph g,
            final String name, final String type) {

        EdgeIteratorState edgeIteratorState = g.getEdgeIteratorState(edge, Integer.MIN_VALUE);
        return createLineString(edgeIteratorState, g, name, type);
    }


    public static Placemark createLineString(
            EdgeIteratorState edgeIteratorState, Graph g,
            final String name, final String type) {
        final Placemark placemark = new Placemark().withName(name)
                .withDescription(type);
        final LineString lineString = placemark.createAndSetLineString()
                .withId(edgeIteratorState.getEdge()+"");
        PointList points = edgeIteratorState.fetchWayGeometry(3);
        for(GHPoint3D point: points)
            lineString.addToCoordinates(point.getLon(), point.getLat());
        return placemark;
    }

}
