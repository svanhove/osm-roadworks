package com.graphhopper.reader.osm.construction;

import java.util.HashMap;
import java.util.Map;

/**
 * Maps type numbers to their original names.
 */
public class OSMTypeMapping {

    private static Map<String,Integer> typeMapping;
    private static Map<Integer,String> reverseTypeMapping;

    public static String getType(int roadTypeIndex){
        if(reverseTypeMapping==null){
            initTypeMapping();
        }
        return reverseTypeMapping.get(roadTypeIndex);
    }

    private static void registerTypeMapping(String type, int index){
        typeMapping.put(type,index);
        reverseTypeMapping.put(index,type);
    }

    private static void initTypeMapping() {
        typeMapping = new HashMap<>();
        reverseTypeMapping = new HashMap<>();
        registerTypeMapping("motorway", 1);
        registerTypeMapping("motorway_link", 2);
        registerTypeMapping("primary", 3);
        registerTypeMapping("primary_link", 4);
        registerTypeMapping("secondary", 5);
        registerTypeMapping("secondary_link", 6);
        registerTypeMapping("tertiary", 7);
        registerTypeMapping("tertiary_link", 8);
        registerTypeMapping("trunk", 9);
        registerTypeMapping("trunk_link", 10);
        registerTypeMapping("unclassified", 11);
        registerTypeMapping("residential", 12);
        registerTypeMapping("service", 13);
        registerTypeMapping("living_street", 14);
        registerTypeMapping("pedestrian", 15);
        registerTypeMapping("track", 16);
        registerTypeMapping("bus_guideway", 17);
        registerTypeMapping("escape", 18);
        registerTypeMapping("road", 19);
        registerTypeMapping("footway", 20);
        registerTypeMapping("bridleway", 21);
        registerTypeMapping("steps", 22);
        registerTypeMapping("path", 23);
        registerTypeMapping("cycleway", 24);
        registerTypeMapping("", 100);
        registerTypeMapping(null, 404);
    }
}
