package com.graphhopper.reader.osm.construction;

import com.graphhopper.storage.Graph;
import com.graphhopper.util.EdgeIteratorState;

/**
 * Created by Stéphanie Vanhove on 18/04/2018.
 *
 * Used for pruning DepthFirstSearch.
 */
public interface Pruner {


    /**
     * Returns true if the search should be discontinued from the given edgeIteratorState, otherwise false.
     * @param g
     * @param edgeIteratorState
     * @return
     */
    boolean prune(Graph g, EdgeIteratorState edgeIteratorState);
}
