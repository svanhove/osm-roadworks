package com.graphhopper.reader.osm.construction;

import com.graphhopper.reader.osm.GraphHopperOSM;
import com.graphhopper.reader.osm.OSMReader;
import com.graphhopper.routing.util.EncodingManager;

import org.apache.commons.cli.*;
/**
 * Created by Stéphanie Vanhove on 7/06/2018.
 */
public class Roadworks {

    public static void main(String[] args) {

        Options options = new Options();
        options.addOption(Option.builder("s").argName("sourceMap").hasArg(true).longOpt("sourceMap").desc("osm source map (required)").type(String.class).required().build());
        options.addOption(Option.builder("i").argName("writeIntermediaryFormat").hasArg(true).longOpt("writeIntermediaryFormat").desc("output file for intermediary format (not required)").type(String.class).build());
        options.addOption(Option.builder("t").argName("writeTagOverrides").hasArg(true).longOpt("writeTagOverrides").desc("output file for tag overrides (required)").type(String.class).required().build());
        String osmfile=null;
        try {
            CommandLine parsedArguments = (new DefaultParser()).parse(options, args);
            OSMReader.WRITE_TAG_OVERRIDES = true;
            OSMReader.targetFileTagOverrides = parsedArguments.getOptionValue("writeTagOverrides");
            OSMReader.WRITE_INTERMEDIARY_FORMAT = parsedArguments.hasOption("writeIntermediaryFormat");
            OSMReader.targetFileIntermediate = parsedArguments.getOptionValue("writeIntermediaryFormat");
            osmfile = parsedArguments.getOptionValue("sourceMap");
        }
        catch(ParseException e) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("This tool identifies all OSM way IDs which are part of roadworks. This can be either because they are tagged in osm as such, or because our algorithm detected that the way must be part of a roadwork as well (eg. for connectivity reasons). The tool writes a file with the neccesary OSM tag overrides. Optionally, the tool can additionally also write the intermediate output file as discussed with Maarten Houbraken.", options);
            System.exit(1);
        }

        GraphHopperOSM graphHopperOSM = new GraphHopperOSM();
        graphHopperOSM.setOSMFile(osmfile);
        graphHopperOSM.setCHEnabled(false);
        graphHopperOSM.setStoreOnFlush(false);  // so it wouldn't write a dump to file !! - not necessary
        graphHopperOSM.setEncodingManager(new EncodingManager("car"));
        graphHopperOSM.setGraphHopperLocation("26pu15a6156d1892"); // just adding a random string here, nothing should be written to this and should be empty since setStoreOnFlush is false, we don't want a graph to be written
        graphHopperOSM.importOrLoad();


    }
}


