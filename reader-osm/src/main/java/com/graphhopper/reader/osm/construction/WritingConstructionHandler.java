package com.graphhopper.reader.osm.construction;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.EdgeIteratorState;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Same as the default construction handler, but also writes the intermediary format (as discussed with Maarten Houbraken) to a file.
 */
public class WritingConstructionHandler extends DefaultConstructionHandler {

    private String targetFile;

    public WritingConstructionHandler(String targetFile) {
        this.targetFile = targetFile;
    }

    @Override
    public Set<Long> handleConstruction(Graph graph, List<EdgeIteratorState> edgesUnderConstruction, Set<Integer> accessNoEdges, Map<Integer, Long> edgeIDsToWayIDs, FlagEncoder flagEncoder) {
        Set<Long> wayIDsUnderConstruction = super.handleConstruction(graph, edgesUnderConstruction, accessNoEdges, edgeIDsToWayIDs, flagEncoder);
        writeIntermediaryResult(edgeIDsToWayIDs, targetFile);
        return wayIDsUnderConstruction;
    }

    private void writeIntermediaryResult(Map<Integer, Long> edgeIDsToWayIDs, String targetFile) {
        System.out.println("\n\nWriting intermediary result to "+targetFile+" ...\n\n");
        PrintWriter writer = null;
        try {

            writer = new PrintWriter(new File(targetFile));

            for (int construction : edgesPerConstruction.keySet()) {
                Set<Long> originalWayIDs = new HashSet<>();
                Set<Long> addedWayIDs = new HashSet<>();
                Set<Integer> edgesInConstruction = edgesPerConstruction.get(construction);
                for (int edgeID : edgesInConstruction) {
                    long wayID = edgeIDsToWayIDs.get(edgeID);
                    if (originalEdgeIDsUnderConstruction.contains(edgeID)) {
                        originalWayIDs.add(wayID);
                    }

                    if (!originalWayIDs.contains(wayID))
                        addedWayIDs.add(wayID);
                }
                writer.print(construction + ";");
                Iterator<Long> iterator = originalWayIDs.iterator();
                if (iterator.hasNext())
                    writer.print(iterator.next());
                while (iterator.hasNext()) {
                    writer.print("," + iterator.next());
                }
                writer.print(";");
                iterator = addedWayIDs.iterator();
                if (iterator.hasNext())
                    writer.print(iterator.next());
                while (iterator.hasNext()) {
                    writer.print("," + iterator.next());
                }

                writer.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(writer != null)
                writer.close();
        }
    }




}
