#!/bin/bash

if [ -z "$LOCAL_CORPDUMP_ROOT" ]; then
    LOCAL_CORPDUMP_ROOT="/data/europe-latest.osm-gh"
    echo "WARN: LOCAL_CORPDUMP_ROOT not set, using default value '"$LOCAL_CORPDUMP_ROOT"'"
fi
LOCAL_CORPDUMP_ROOT=${LOCAL_CORPDUMP_ROOT%/} # remove trailing slash

if [ -z "$REPO_CORPDUMP_ROOT" ]; then
    REPO_CORPDUMP_ROOT="https://repository.be-mobile.biz/maps/corpdump/"
    echo "WARN: REPO_CORPDUMP_ROOT not set, using default value '"$REPO_CORPDUMP_ROOT"'"
fi
REPO_CORPDUMP_ROOT=${REPO_CORPDUMP_ROOT%/} # remove trailing slash

if [ -z "$CORPDUMP_FILE_PREFIX" ]; then
    CORPDUMP_FILE_PREFIX="corp-europe-gh-dump-"
    echo "WARN: CORPDUMP_FILE_PREFIX not set, using default value '"$CORPDUMP_FILE_PREFIX"'"
fi

if [ -z "$CORPDUMP_FILE_COLOR" ]; then
    CORPDUMP_FILE_COLOR=""
    echo "WARN: CORPDUMP_FILE_COLOR not set, using default value '"$CORPDUMP_FILE_COLOR"'"
fi

if [ -z "$CORPDUMP_FILE_SUFFIX" ]; then
    CORPDUMP_FILE_SUFFIX=".zip"
    echo "WARN: CORPDUMP_FILE_SUFFIX not set, using default value '"$CORPDUMP_FILE_SUFFIX"'"
fi

CORPDUMP_FILENAME=$CORPDUMP_FILE_PREFIX$CORPDUMP_FILE_COLOR$CORPDUMP_FILE_SUFFIX
REPO_FILE="$REPO_CORPDUMP_ROOT/$CORPDUMP_FILENAME"
LOCAL_FILE="$LOCAL_CORPDUMP_ROOT/$CORPDUMP_FILENAME"

if [ ! -d "$LOCAL_CORPDUMP_ROOT" ] || [ `ls $LOCAL_CORPDUMP_ROOT | wc -l` -lt 1 ] || [ "$FORCE_DOWNLOAD" == "true" ]; then
    mkdir -p $LOCAL_CORPDUMP_ROOT

    if [ -n "${REPO_USERNAME}" ]; then
        USER="--user=${REPO_USERNAME}"
    fi

    if [ -n "${REPO_PASSWORD}" ]; then
        PASS="--password=${REPO_PASSWORD}"
    fi

    echo "INFO: Downloading '"$REPO_FILE"' to '"$LOCAL_FILE"'"
    wget --progress=dot:giga --no-check-certificate $USER $PASS -O "$LOCAL_FILE" "$REPO_FILE"
    if [ $? -ne 0 ]; then
        echo "FATAL: Download failed"
        exit $?
    fi

    echo "INFO: Deflating '"$LOCAL_FILE"' to '"$LOCAL_CORPDUMP_ROOT"'"
    unzip -o -u $LOCAL_FILE -d $LOCAL_CORPDUMP_ROOT
    if [ $? -ne 0 ]; then
        echo "FATAL: Deflate failed"
        exit $?
    fi
fi

CMD="java $JAVA_ARGS -jar CORP.jar -configFile defaultCHconfig.properties -graphFolder $LOCAL_CORPDUMP_ROOT -api"
echo "INFO: Starting OSMGraphhopper ('"$CMD"')"
$CMD
