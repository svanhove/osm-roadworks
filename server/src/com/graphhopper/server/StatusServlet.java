package com.graphhopper.server;

import com.graphhopper.http.GHBaseServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maarten
 */
public class StatusServlet extends GHBaseServlet{
    
@Override
    public void doGet(HttpServletRequest httpReq, HttpServletResponse httpRes) throws ServletException, IOException {
        if(!UndertowServer.isStarted()){
            httpRes.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        }else{
            httpRes.setStatus(HttpServletResponse.SC_OK);
        }
    }
}
