package com.graphhopper.server;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.servlet.GuiceFilter;
import com.graphhopper.http.DefaultModule;
import com.graphhopper.http.GHServletModule;
import com.graphhopper.http.GraphHopperServlet;
import com.graphhopper.http.TrafficDataServlet;
import com.graphhopper.util.CmdArgs;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.predicate.Predicates;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.encoding.ContentEncodingRepository;
import io.undertow.server.handlers.encoding.EncodingHandler;
import io.undertow.server.handlers.encoding.GzipEncodingProvider;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.ClassIntrospecter;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.FilterInfo;
import io.undertow.servlet.api.InstanceFactory;
import io.undertow.servlet.api.ServletInfo;
import io.undertow.servlet.util.ImmediateInstanceFactory;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class UndertowServer {
    protected static Logger logger = LoggerFactory.getLogger(UndertowServer.class);
    public static int port;
//    private static String MYAPP="map/BE_20140901_2";//Line to be used while in development with PJ DS
    private static String MYAPP = "server";
    private final CmdArgs cmdargs;
    private static boolean started=false;
    public static void main(String[] args) throws Exception {
        new UndertowServer(new CmdArgs(readInputArguments(args))).start();
    }

    public UndertowServer(CmdArgs param) {
        this.cmdargs = param;
    }
    public static boolean isStarted(){
        return started;
    }
    public void start() throws Exception {
        Module m = createModule();
        Injector injector = Guice.createInjector(m);
        start(injector);
    }

    private void start(Injector injector) {
        if (cmdargs.has("api")) {
            startAPI(injector);
        }
    }

    private void startAPI(Injector injector) {
        try {
            port = cmdargs.getInt("port", 8088);
            //            c=null;
            ArrayList<String> welcomepages = new ArrayList<>();
            //            welcomepages.add("index.html");
            welcomepages.add("/index.html");
            Collection<ServletInfo> servlets = new HashSet<>();
            servlets.add(
                    new ServletInfo("GraphHopperServlet", GraphHopperServlet.class)
                    .addMapping("/route"));
            servlets.add(
                    new ServletInfo("StatusServlet", StatusServlet.class)
                    .addMapping("/status"));
            servlets.add(
                    new ServletInfo("TrafficDataServlet", TrafficDataServlet.class)
                    .addMapping("/importTrafficData"));
            
//            FilterInfo filter = Servlets.filter("/*", GuiceFilter.class);

            //            welcomepages.add("WebContent/index.html");
            DeploymentInfo servletBuilder = Servlets.deployment()
//                    .setClassIntrospecter(new GuiceClassIntrospector(injector))//deployment()
                    .setClassLoader(UndertowServer.class.getClassLoader())
                    .setContextPath(MYAPP)
                    .setDeploymentName("GH.war")
//                    .addFilter(filter)
                    .addServlets(servlets);
                    
            //                    .setResourceManager(new FileResourceManager(new File("WebContent/index.html"), 1024));

            DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);//new DeploymentManagerImpl(servletBuilder, null);
            manager.deploy();
            
            HttpHandler servletHandler = manager.start();
          
            PathHandler path = Handlers.path()//Handlers.redirect(MYAPP))
                    .addPrefixPath(MYAPP, servletHandler);

            EncodingHandler handler
                    = new EncodingHandler(new ContentEncodingRepository()
                            .addEncodingHandler("gzip",
                                    new GzipEncodingProvider(), 50,
                                    Predicates.parse("max-content-size[5]")))
                    .setNext(path);
            Undertow server = Undertow.builder()
                    .addHttpListener(port, "0.0.0.0")
                    .setHandler(handler)
                    .build();
            server.start();
            started=true;
            logger.info("Started server on port " + port);
        } catch (ServletException se) {
            se.printStackTrace();
        }
    }

    private static Map<String, String> readConfigFile(String configFile) {
        return readConfigFile(configFile, null);
    }
    private static Map<String, String> readConfigFile(String configFile,Map<String,String> prev) {
        Map<String, String> map = new HashMap<>();
        BufferedReader in = null;
        try {
            logger.info("Reading from configFile = " + configFile);
            in = new BufferedReader((new FileReader(configFile)));
            String line = null;
            while ((line = in.readLine()) != null) {
                try {
                    if (line.isEmpty()||line.startsWith("//") || line.startsWith("#"))
                        continue;
                    StringTokenizer st = new StringTokenizer(line, "=");
                    String name = st.nextToken();
                    String value = line.substring(line.indexOf("=") + 1);
                    map.put(name, value);
                    logger.info("Param "+name +":"+value);
                } catch (Exception e) {
                    logger.info("Error reading conf line " + line, e);
                }
            }
            in.close();
        } catch (Exception aE) {
            logger.error("ERROR reading conf: %s\n", aE.getMessage());
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(UndertowServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if(prev!=null)map.putAll(prev);
        if (map.containsKey("osm") && !map.containsKey("datareader.file")) {
            map.put("datareader.file", map.get("osm"));
        }
        if (map.containsKey("graphFolder") && !map.containsKey("graph.location")) {
            map.put("graph.location", map.get("graphFolder"));
        }

        return map;

    }

    public static void Die(String msg) {
        System.out.println(msg);
        System.exit(1);
    }

    private Module createModule() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                binder().requireExplicitBindings();
                install(new DefaultModule(cmdargs));
            }
        };
    }

    private static Map<String, String> readInputArguments(String[] args) {
        String configFile = null;
        String osm = null;
        String graphFolder = null;
        boolean api=false;
        
////        String configFile = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\UndertowConfig.txt";
////        String config = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\defaultCHconfig.properties";
////        String osm = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\france-latest.osm.pbf";
////        String osm = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\spain-latest.osm.pbf";
////        String osm = "C:\\mobiroute\\Be-Mobile maps\\BE-LU-NL_20170913_1\\source.osm.pbf";
////        String osm = "C:\\BeMobile\\Osmosis\\blaat.osm.pbf";
//        String osm = "C:\\BeMobile\\Osmosis\\LU_Enriched.osm.pbf";
////        String osm = "C:\\BeMobile\\Osmosis\\LU_Enriched.osm.pbf";
////        String osm = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\LU_Buffer_completeWays_Enriched.osm.pbf";
////        String osm = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\belgium-latest.osm.pbf";
////        String osm = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\luxembourg-latest.osm.pbf";
////        String graphFolder = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\france\\";
////        String graphFolder = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\spain\\";
//        String graphFolder = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\lux\\";
////        String graphFolder = "C:\\BeMobile\\GIT\\OSMGraphhopper\\graphhopper\\belgium\\";
//        boolean api = true;
        Options opts = new Options();
        opts.addOption("configFile", true, "File name of configuration file");
//            opts.addOption("build", false, "Creates dump");
        opts.addOption("api", false, "Starts planning API using Undertow");
        opts.addOption("osm",true,"OSM source file");
        opts.addOption("graphFolder",true,"Folder containing/to contain CORP dump");
        Map<String,String> cmdParam=new HashMap<>();
        //parse options
        CommandLineParser parser = new PosixParser();
        try {
            CommandLine cmd = parser.parse(opts, args);
            if (cmd.hasOption("configFile")) {
                configFile = cmd.getOptionValue("configFile");
            }
            if (cmd.hasOption("osm")) {
                osm = cmd.getOptionValue("osm");   
            }
            if (cmd.hasOption("graphFolder")) {
                graphFolder = cmd.getOptionValue("graphFolder");
            }
//                if(cmd.hasOption("build"))build=true;
            if (cmd.hasOption("api")){
                api = true;
            }
            if(configFile!=null)cmdParam.put("configFile", configFile);
            if(osm!=null)cmdParam.put("osm", osm); 
            if(graphFolder!=null)cmdParam.put("graphFolder", graphFolder);
            if(api!=false)cmdParam.put("api", "true");
        } catch (org.apache.commons.cli.ParseException e) {
            e.printStackTrace();
            Die("Error: Parsing error");
        }
        Map<String, String> param = readConfigFile(configFile,cmdParam);
//        if(!param.containsKey("config")){
////            param.put("config","defaultCHconfig.properties");//only for developers
//            param.put("config",config);
//        }
        System.out.println("osm "+osm);
        logger.info("osm "+osm);
        return param;
    }
    
}
