package com.graphhopper.http;

import com.graphhopper.routing.Path;
import com.graphhopper.routing.VirtualEdgeIteratorState;
import com.graphhopper.routing.weighting.DelayWeighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.GHUtility;
import com.graphhopper.util.Helper;
import com.graphhopper.util.PointList;
import com.graphhopper.util.los.LOSEdgeIteratorState;
import com.graphhopper.util.los.LinkSegmentState;
import com.graphhopper.util.los.LinkSegmentation;
import com.graphhopper.util.los.TrafficData;
import com.graphhopper.util.los.TrafficDataSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class BeMobilePathProcessor {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    List<EdgeIteratorState> edgesInFeature;
    List<JSONObject> features;
    int currentLOS=-1;
    int currentType=-1;
    String currentCountryCode=null;
    boolean groupOnLOS;
    boolean groupOnType;
    boolean groupOnCountryCode;
    boolean realtimeCost;
    boolean trafficEvents;
    int delayInMilliSeconds=0;
    Set<JSONObject>events;
    Path path;
    Graph g;
    TrafficDataSet td;
    ArrayList<Integer> edgeIDs;
    boolean processed=false;
    
    private static final DistanceCalc distCalc = Helper.DIST_EARTH;

    BeMobilePathProcessor(Path path,Graph g, TrafficDataSet td, boolean groupOnLOS, 
            boolean groupOnType, boolean groupOnCountryCode,boolean realtimeCost,boolean trafficEvents) {
        features=new ArrayList<>();
        edgesInFeature=new ArrayList<>();
        this.groupOnLOS=groupOnLOS;
        this.groupOnType=groupOnType;
        this.groupOnCountryCode=groupOnCountryCode;
        this.path=path;
        this.td=td;
        this.realtimeCost=realtimeCost;
        this.g=g;
        this.trafficEvents=trafficEvents;
        this.edgeIDs=new ArrayList<>();
        if(trafficEvents)events=new HashSet<>();
    }

    void accountForEdgeIteratorState(EdgeIteratorState edgeIteratorState,int LOS,int type,String cc) {
        boolean newFeature=false;
        if(currentType==-1)currentType=type;
        if(currentCountryCode==null)currentCountryCode=cc;
//        if(currentLOS==-1)currentLOS=LOS;
        if(groupOnType){
            newFeature|=type!=-1&&currentType!=type;
        }
        if(groupOnLOS){
            newFeature|=LOS!=currentLOS;
        }
        if(groupOnCountryCode){
            newFeature|=cc!=null&&!cc.equalsIgnoreCase(currentCountryCode);
        }
        if(newFeature){
                //current edge starts new feature
            JSONObject feature=getFeature(edgesInFeature,currentLOS,currentType,currentCountryCode);
            features.add(feature);
            edgesInFeature.clear();
            edgesInFeature.add(edgeIteratorState);
            currentLOS=LOS;
            currentType=type!=-1?type:currentType;
            currentCountryCode=cc!=null?cc:currentCountryCode;
        }else {
            edgesInFeature.add(edgeIteratorState);
        }
        edgeIDs.add(edgeIteratorState.getEdge());
    }

    void close() {
    	JSONObject lastFeature=getFeature(edgesInFeature,currentLOS,currentType,currentCountryCode);
    	features.add(lastFeature);
        processed=true;
    }
    protected JSONObject getFeature(List<EdgeIteratorState> edgesInFeature,int currentLOS,int currentType,String currentCountryCode){
        PointList pl=new PointList(edgesInFeature.size()+1,false);
        double lengthInM=0;
    	for (int i=0;i<edgesInFeature.size();i++){
            EdgeIteratorState edgeIteratorState =edgesInFeature.get(i);
            edgeIDs.add(edgeIteratorState.getEdge());
            PointList edgeGeom=edgeIteratorState.fetchWayGeometry(i==0?3:2);
            pl.add(edgeGeom);
            lengthInM+=edgeIteratorState.getDistance();
        }
    	JSONObject feature=new JSONObject();
    	JSONObject jgeometry=new JSONObject();
    	jgeometry.put("type", "LineString");
    	jgeometry.put("coordinates", pl.toGeoJson(false));
    	feature.put("geometry", jgeometry);
    	feature.put("type","Feature");
        
//        JSONObject feature=getFeatureFromEdges(edgesInFeature);
        JSONObject properties=new JSONObject();
        if(groupOnLOS)properties.put("los", currentLOS);
        if(groupOnType)properties.put("roadType", currentType);
        if(groupOnCountryCode)properties.put("countryCode", currentCountryCode);
        properties.put("lengthInM", lengthInM);
        feature.put("properties",properties);
        return feature;
    }

    List<JSONObject> getFeatures() {
        return features;
    }
    protected JSONArray createJSONFeatureArray()
    {
        if(!processed)process();
        List<JSONObject>features=getFeatures();
    	JSONArray ret=new JSONArray(features);
    	return ret;
    }
    private void process(){
        
//        int nrSegmentedLinks=0;
        List<EdgeIteratorState> eisl= path.calcEdges();
    	for (EdgeIteratorState edgeIteratorState : eisl) {
            int type=groupOnType?edgeIteratorState.getType():0;
            String cc=groupOnCountryCode?edgeIteratorState.getCountryCode():null;
//            int ghID=edgeIteratorState.getEdge();
//            logger.info("Doing link "+ghID);
            if(groupOnLOS||realtimeCost||trafficEvents){
                List<LOSEdgeIteratorState> parts=getLOSEdgeIteratorStates(edgeIteratorState);
                if(parts==null){
                    accountForEdgeIteratorState(edgeIteratorState,-1,type,cc);
                }
                else{
                    for (LOSEdgeIteratorState leis : parts) {
//                        logger.info("Found segmented link");
                        accountForEdgeIteratorState(leis,leis.getLOS(),type,cc);
                        if(realtimeCost)delayInMilliSeconds+=leis.getDelayInMs();
                        if(trafficEvents)events.addAll(leis.getActiveEvents());
                    }
//                    nrSegmentedLinks++;
                }
            }else{
                accountForEdgeIteratorState(edgeIteratorState, -1, type,cc);
            }
        }
//        logger.info("Found "+nrSegmentedLinks+" segmentedLinks");
//        events.add(new JSONObject(st));
    	close();
    }
    /**
     * Creates EdgeIteratorState list for this link according to the linkSegmentation.
     * @param linkSegmentation
     * @param eis
     * @param edgeID
     * @param sharedOrigin
     * @param sharedDestination
     * @return 
     */
    private List<LOSEdgeIteratorState> createLOSEdgeIteratorStates(LinkSegmentation linkSegmentation,EdgeIteratorState eis,int edgeID,boolean sharedOrigin,boolean sharedDestination) { 
        List<LOSEdgeIteratorState>r=new ArrayList<>();
        List<LinkSegmentState> l =linkSegmentation.getStatesOnLink();
        //retrieve the pointlist of the edge. Note that this is already of the correct full length (important for virtual edges at the beginning and end of a route)
        PointList pl=eis.fetchWayGeometry(3);
        //to couple the beginning/end of the link, we need to figure out what the offset is on the link
        int distanceOnLinkInMM=(int)(eis.getDistance()*1000);
        int startOffset=0;
        int indexToSegmentation=0;
        LinkSegmentState ll=l.get(indexToSegmentation);
        int s=ll.getStartOffsetInMm();
        if(!sharedOrigin&&!sharedDestination){
            //now we need to figure out where the first node starts on the link
            //the last node then starts eis.getDistance further
            VirtualEdgeIteratorState v=(VirtualEdgeIteratorState)eis;
            int edgeKey=v.getOriginalTraversalKey();
            boolean forward=edgeKey%2==0;
            
            double dofStart=(v.getDistanceToOriginalStart()*1000);
            if(!forward)dofStart=linkSegmentation.getLengthOfLinkInMM()-dofStart;
            startOffset=(int)dofStart;
            s=startOffset;
//            logger.info("distanceOnLinkInMM="+distanceOnLinkInMM);
//            logger.info("Got start offset on link of "+startOffset+" with endoffset of "+ll.getEndOffsetInMm());
//            linkSegmentation.printState();
            while(indexToSegmentation<l.size()-1&&s>=ll.getEndOffsetInMm()){
                indexToSegmentation++;
                ll=l.get(indexToSegmentation);
            }
        }else if(!sharedOrigin){
            //for the beginning of a route, the pointlist is only the tail end of the link. The first few meters are not covered
            //these do need to be accounted for in the offsetting of the segmentation
            startOffset=linkSegmentation.getLengthOfLinkInMM()-distanceOnLinkInMM;
            s=startOffset;
            while(indexToSegmentation<l.size()-1&&s>=ll.getEndOffsetInMm()){
                indexToSegmentation++;
                ll=l.get(indexToSegmentation);
            }
        }
        while(ll!=null){
            int end=ll.getEndOffsetInMm();
            int start=ll.getStartOffsetInMm();
            if(end!=start){
                int x=s-startOffset;
                int y=Math.min(end-startOffset,distanceOnLinkInMM);
                double f=(y-x)/(double)(end-start);
                PointList p=getPointListForRangeInMM(pl,x,y);
                double d=f*ll.getDelayInMs();
//                logger.info("dist = "+(y-x)+", delay = "+d+", seconds/meter = "+(d/(y-x))+", edgeID = "+edgeID+", LOS = "+ll.getLOS());
                LOSEdgeIteratorState le=new LOSEdgeIteratorState(p, ll.getLOS(),d,ll.getActiveEvents(),edgeID);
                r.add(le);
            }
            indexToSegmentation++;
            if(indexToSegmentation>=l.size())break;
            if(end-startOffset>=distanceOnLinkInMM)break;
            s=end;
            ll=l.get(indexToSegmentation);
        }
        return r;
    }

    private PointList getPointListForRangeInMM(PointList pl, int startOffsetInMm, int endOffsetInMm) {
        PointList r=new PointList();
        int nrNodes=pl.size();
        double dist=0;
        double clat=pl.getLat(0);
        double clon=pl.getLon(0);
        int i=1;
        for(;i<nrNodes;i++){
            double lat=pl.getLat(i);
            double lon=pl.getLon(i);
            double d=distCalc.calcDist(clat, clon, lat, lon)*1000;
            if(dist+d>=startOffsetInMm){
                double f=(startOffsetInMm-dist)/(d);
                double fplat=f*lat+(1-f)*clat;
                double fplon=f*lon+(1-f)*clon;
                r.add(fplat,fplon);
                if(dist+d>endOffsetInMm){
                    double ff=(endOffsetInMm-dist)/(d);
                    double ffplat=ff*lat+(1-ff)*clat;
                    double ffplon=ff*lon+(1-ff)*clon;
                    r.add(ffplat,ffplon);
                    return r;
                }
                r.add(lat,lon);    
                clat=lat;clon=lon;dist+=d;
                break;
            }
            clat=lat;clon=lon;dist+=d;
        }
        for(;i<nrNodes;i++){
            double lat=pl.getLat(i);
            double lon=pl.getLon(i);
            double d=distCalc.calcDist(clat, clon, lat, lon)*1000;
            if(dist+d<endOffsetInMm){
                r.add(lat,lon);
                dist+=d;
                clat=lat;
                clon=lon;
            }else{
                double ff=(endOffsetInMm-dist)/(d);
                double ffplat=ff*lat+(1-ff)*clat;
                double ffplon=ff*lon+(1-ff)*clon;
                r.add(ffplat,ffplon);
                return r;
            }
        }
        return r;
    }
    private List<LOSEdgeIteratorState> getLOSEdgeIteratorStates(EdgeIteratorState eis) {
        if(td==null)return null;
        boolean forward=true;

        //quickly return if the edge is not to be segmented
        int edgeID=eis.getEdge();
        boolean isEndOrBegin=(eis instanceof VirtualEdgeIteratorState);
        if(isEndOrBegin){
            VirtualEdgeIteratorState ve=(VirtualEdgeIteratorState)eis;
            int edgeKey=ve.getOriginalTraversalKey();
            edgeID=GHUtility.getEdgeFromEdgeKey(edgeKey);
            forward=edgeKey%2==0;//if edgeKey is even, it concerns an original edge going from a low to a high ID
        }
//        logger.info("Looking for data for link "+edgeID);
        if(!td.has(edgeID, true)&&!td.has(edgeID, false))return null;
        //if a segmentation exists, figure out if we need it (=if it is not the wrong direction)
        int currentFrom=eis.getBaseNode();
        int currentTo=eis.getAdjNode();
//        logger.info("Looking for data for link "+edgeID+" with one side");
        EdgeIteratorState eisOriginal=g.getEdgeIteratorState(edgeID, Integer.MIN_VALUE );
        int ghFrom=eisOriginal.getBaseNode();
        int ghTo=eisOriginal.getAdjNode();

        if(!isEndOrBegin){
            if(ghTo==currentTo||ghFrom==currentFrom){
                //we are looking at the correct edge
                forward=ghFrom<=ghTo;
            }else{
                forward=ghFrom>=ghTo;
            }
        }
        if(!(td.has((long)edgeID,forward)))//if no entry is present, no segmentations is necessary
            return null;
        
        LinkSegmentation linkSegmentation = td.getLinkSegmentation(edgeID, forward);
        if(!isEndOrBegin){
            List<LOSEdgeIteratorState> ll=linkSegmentation.getLOSEdgeIteratorStates();
            if(ll!=null){
                return ll;
            }else{
                ll=createLOSEdgeIteratorStates(linkSegmentation,eis,edgeID,true,true);
                linkSegmentation.setLOSEdgeIteratorStates(ll);
                return ll;
            }
        }else{
            boolean sharedOrigin,sharedDestination;
            if(forward){
                sharedOrigin=ghFrom==currentFrom;
                sharedDestination=ghTo==currentTo;
            }else{
                sharedOrigin=ghTo==currentFrom;
                sharedDestination=ghFrom==currentTo;
            }
            return createLOSEdgeIteratorStates(linkSegmentation, eis,edgeID,sharedOrigin,sharedDestination);
        }
    }

    double getRealTime() {
        if(!processed)process();
        if(path.getWeighting() instanceof DelayWeighting)return path.getTime();
        return path.getTime()+delayInMilliSeconds;
    }
    public double getDelayInMilliSeconds(){
        return delayInMilliSeconds;
    }
    
    Set<JSONObject> getTrafficEvents() {
        if(!processed)process();
        return events;
    }
    List<Integer> getEdgeIDs(){
        if(!processed)process();
        return edgeIDs;
    }
}
