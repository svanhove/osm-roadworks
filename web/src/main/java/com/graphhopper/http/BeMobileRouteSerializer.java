package com.graphhopper.http;

import com.graphhopper.util.los.LOSEdgeIteratorState;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.weighting.DelayWeighting;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.Helper;
import com.graphhopper.util.los.LinkSegmentState;
import com.graphhopper.util.los.LinkSegmentation;
import com.graphhopper.util.PointList;
import com.graphhopper.util.los.TrafficData;
import com.graphhopper.util.los.TrafficDataManager;
import com.graphhopper.util.los.TrafficDataSet;
import com.graphhopper.util.shapes.BBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maarten
 */
public class BeMobileRouteSerializer implements RouteSerializer{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final DistanceCalc distCalc = Helper.DIST_EARTH;

    private final BBox maxBounds;
    Graph g;
    public BeMobileRouteSerializer(GraphHopperStorage ghs) {
        this.maxBounds = ghs.getBounds();
        this.g=ghs;
    }

    @Override
    public Map<String, Object> toJSON(GHResponse response, boolean calcPoints, boolean pointsEncoded, boolean includeElevation, boolean enableInstructions) {
        return null;
    }

    @Override
    public Map<String, Object> toJSON(GHResponse rsp, HttpServletRequest httpReq) {
    	String include=GHBaseServlet.getParam(httpReq,"include",null);
    	boolean enableVisualization=GHBaseServlet.getBooleanParam(httpReq,"visualization",false)||GHBaseServlet.getBooleanParam(httpReq,"visualisation",false);
        boolean writeGPX = "gpx".equalsIgnoreCase(GHBaseServlet.getParam(httpReq, "type", "json"));
        boolean enableInstructions = writeGPX || GHBaseServlet.getBooleanParam(httpReq, "instructions", false);
        boolean pointsEncoded = GHBaseServlet.getBooleanParam(httpReq, "points_encoded", true)&&GHBaseServlet.getBooleanParam(httpReq, "pointsEncoded", true);
        boolean includeElevation = GHBaseServlet.getBooleanParam(httpReq, "elevation", false);
        boolean groupOnLOS=GHBaseServlet.getBooleanParam(httpReq,"groupOnLOS",true);
        boolean groupOnType=GHBaseServlet.getBooleanParam(httpReq,"groupOnType",false);
        boolean groupOnCountryCode=GHBaseServlet.getBooleanParam(httpReq,"groupOnCountryCode",false);
        boolean enabletrafficEvents=GHBaseServlet.getBooleanParam(httpReq,"trafficEvents",false)||GHBaseServlet.getBooleanParam(httpReq,"events",false);
        boolean realtimeCost=true;
        boolean includeEdgeIDs=GHBaseServlet.getBooleanParam(httpReq,"edges",false);
//        int nrWantedAlternatives=(int)getDoubleParam(httpReq,"nrAlternativePathsWanted",0);
//        String algorithm=GHBaseServlet.getParam(httpReq, "algorithm", null);
//        boolean alternatives=algorithm!=null&&algorithm.equalsIgnoreCase("alternativeRoute");
//        boolean alternatives=true;
        if(include!=null){
            String[] tok=include.split(",");
            for(String opt:tok){
                if("instructions".equalsIgnoreCase(opt)){
                    enableInstructions=true;
                }else if("visualization".equalsIgnoreCase(opt)||"visualisation".equalsIgnoreCase(opt)){
                    enableVisualization=true;
                }else if("trafficEvents".equalsIgnoreCase(opt)||"events".equalsIgnoreCase(opt)){
                    enabletrafficEvents=true;
                }else if("edges".equalsIgnoreCase(opt)){
                    includeEdgeIDs=true;
                }
            }
        }
        boolean calcPoints = GHBaseServlet.getBooleanParam(httpReq, "calc_points", true)||enableInstructions;

//        String mode=GHBaseServlet.getParam(httpReq, "vehicle", "car");
        String departure_time=GHBaseServlet.getParam(httpReq,"departure_time",null);
        if(departure_time==null)departure_time=GHBaseServlet.getParam(httpReq,"departureTime",null);
        String arrival_time=GHBaseServlet.getParam(httpReq,"arrival_time",null);
        if(arrival_time==null)GHBaseServlet.getParam(httpReq, "arrivalTime", null);
        boolean forwardRouting=(arrival_time == null);
        boolean timed=departure_time!=null||arrival_time!=null;
        
        Map<String, Object> json = new HashMap<>();
        Map<String, Object> jsonInfo = new HashMap<>();
        json.put("info", jsonInfo);
        if (rsp.hasErrors())
        {
            jsonInfo.put("message", getMessage(rsp.getErrors().get(0)));
            List<Map<String, String>> list = new ArrayList<>();
            for (Throwable t : rsp.getErrors())
            {
                Map<String, String> map = new HashMap<>();
                map.put("message", t.getMessage());
                map.put("details", t.getClass().getName());
                list.add(map);
            }
            jsonInfo.put("hints", list);
        } else{
            List<PathWrapper> pathWrappers=rsp.getAll();
            DateTime start=departure_time!=null?dateFormatter.parseDateTime(departure_time):null;
            DateTime end=arrival_time!=null?dateFormatter.parseDateTime(arrival_time):null;
            int nrAlternatives=pathWrappers.size();
            List<Map<String, Object> >pp=new ArrayList<>();
            TrafficDataSet tdcall=TrafficDataManager.getTrafficDataManager().getActiveTrafficDataSet();
            
            for(int i=0;i<nrAlternatives;i++){
                PathWrapper pw=pathWrappers.get(i);
                Weighting w=pw.getWeighting() ;
                if(w instanceof DelayWeighting)tdcall=((DelayWeighting)w).getActiveTrafficDataSet();
                Map<String, Object> jsonPath=processPath(pw,tdcall,start,end,forwardRouting,
                                timed, enableVisualization, calcPoints, 
                                enableInstructions, enabletrafficEvents,groupOnLOS, groupOnType, 
                                groupOnCountryCode,realtimeCost,pointsEncoded, includeElevation,includeEdgeIDs);
                pp.add(jsonPath);
            }
            json.put("routes", pp);
        }
        return json;
    }
    private String getMessage(Throwable t) {
        if (t.getMessage() == null)
            return t.getClass().getSimpleName();
        else
            return t.getMessage();
    }
    private Map<String, Object>  processPath(PathWrapper pw,TrafficDataSet td,DateTime start, DateTime end, boolean forwardRouting,
    		boolean timed,boolean enableVisualization,boolean calcPoints,
    		boolean enableInstructions,boolean enableTrafficEvents,
                boolean groupOnLOS,boolean groupOnType,boolean groupOnCountryCode,
                boolean realtimeCost,boolean pointsEncoded, boolean includeElevation,boolean includeEdgeIDs){
    	
        Map<String, Object> jsonPath = new HashMap<>();
        Map<Path,Double> timePerLeg=new HashMap<>();
//        NameIndex linkAccess=isAlternative?this.hopper.getGraphHopperStorage().get():null;
        //now do route parts
        JSONArray jsonLegs=new JSONArray();
        jsonPath.put("legs", jsonLegs);
        int index=0;
        double sumOfRealTimes=0;
        double sumOfDelays=0;
        List<Path> paths=pw.getPaths();
        boolean delayweighting=pw.getWeighting() instanceof DelayWeighting;
        for (Path path : paths) {
            JSONObject leg=new JSONObject();
            JSONObject times=new JSONObject();
            leg.put("travelTimes",times);
            leg.put("distance", path.getDistance());
            times.put("optimal", path.getTime()/1000.0);
            String modeString="car";
            leg.put("mode", modeString);           
            if(enableVisualization||realtimeCost||enableTrafficEvents){
                BeMobilePathProcessor bmv= new BeMobilePathProcessor(path,g,td,groupOnLOS,groupOnType,groupOnCountryCode,realtimeCost,enableTrafficEvents);
                if(enableVisualization){
                    Map<String,Object> jsonvisualization=new HashMap<>();
                    jsonvisualization.put("type","FeatureCollection");
                    JSONArray featureArray=bmv.createJSONFeatureArray();

                    jsonvisualization.put("features",featureArray);
                    leg.put("visualization",jsonvisualization);
                }
                if(realtimeCost){
                    double timeOfLegInSeconds=bmv.getRealTime()/1000.0;
                    times.put("real",timeOfLegInSeconds);
                    sumOfRealTimes+=timeOfLegInSeconds;
                    timePerLeg.put(path, timeOfLegInSeconds);
                    double d=bmv.getDelayInMilliSeconds()/1000.0;
                    sumOfDelays+=d;
                    double x=path.getTime()/1000.0;
                    if(delayweighting)x-=d;
                    times.put("optimal", x);

                }
                if(enableTrafficEvents){
                    leg.put("trafficEvents",bmv.getTrafficEvents());
                }
                
            }
            if(includeEdgeIDs){
                    jsonPath.put("edges",makeEdgeList(path.calcEdges()));
            }           
            jsonLegs.put(index++,leg);
	}
        if(calcPoints){
            PointList points=pw.getPoints();
            jsonPath.put("points", createPoints(points, pointsEncoded, includeElevation));
        }
        if (enableInstructions){
            jsonPath.put("instructions", pw.getInstructions().createJson());
        }
        
        if(timed){
            addTimesToJSON(start,end,forwardRouting,paths,realtimeCost,jsonPath,jsonLegs,timePerLeg);
        }
        JSONObject times=new JSONObject();
        jsonPath.put("travelTimes",times);
        jsonPath.put("distance", round(pw.getDistance(), 3));
        if(pw.getWeighting() instanceof DelayWeighting){
            double rt=pw.getTime();
            times.put("real",rt/1000.0);
            times.put("optimal",rt/1000.0-sumOfDelays);
        }else{
            times.put("optimal", pw.getTime()/1000.0);
            if(realtimeCost)times.put("real",sumOfRealTimes);
        }
        
        return jsonPath;
    }
    private int[] makeEdgeList(List<EdgeIteratorState> list){
        List<Integer> e=new ArrayList<>();
        for (EdgeIteratorState edgeIteratorState : list) {
            e.add(edgeIteratorState.getEdge());
        }
        int[]a=new int[e.size()];
        for (int i = 0; i < e.size(); i++) {
            a[i]=(int)e.get(i);
        }
        return a;
    }
    public static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }
    private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZoneUTC();
    
    public Object createPoints( PointList points, boolean pointsEncoded, boolean includeElevation )
    {
        Map<String, Object> jsonPoints = new HashMap<String, Object>();
        jsonPoints.put("type", "LineString");
        jsonPoints.put("coordinates", points.toGeoJson(includeElevation));
        return jsonPoints;
    }
    public static double round( double value, int exponent )
    {
        double factor = Math.pow(10, exponent);
        return Math.round(value * factor) / factor;
    }

    private void addTimesToJSON(DateTime start, DateTime end, boolean forwardRouting, List<Path> paths, boolean realtimeCost, Map<String, Object> jsonPath,JSONArray jsonLegs,Map<Path,Double>timePerLeg) {
        DateTime[] completeDates={start,end};
        DateTime s=forwardRouting?start:end;
        for(int i=0;i<paths.size();i++){
            int in=forwardRouting?i:paths.size()-i-1;
            Path path=paths.get(in);
            JSONObject leg=(JSONObject)jsonLegs.get(in);
            double t=timePerLeg.get(path);
            leg.put(forwardRouting?"departureTime":"arrivalTime", dateFormatter.print(s));
            if(forwardRouting){
                if(realtimeCost){
                    s=s.plusSeconds((int)(t));
                }
                else s=s.plusSeconds((int) (path.getTime()/1000.0));
            }else{
                if(realtimeCost){
                    s=s.minusSeconds((int)(t));
                }else{
                    s=s.minusSeconds((int) (path.getTime()/1000.0));
                }
            }
            leg.put(forwardRouting?"arrivalTime":"departureTime", dateFormatter.print(s));
        }
        completeDates[forwardRouting?1:0]=s;
//      
        jsonPath.put("departureTime", dateFormatter.print(completeDates[0]));
        jsonPath.put("arrivalTime", dateFormatter.print(completeDates[1]));
    }
}
