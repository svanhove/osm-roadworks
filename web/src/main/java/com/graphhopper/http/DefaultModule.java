/*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for 
 *  additional information regarding copyright ownership.
 * 
 *  GraphHopper GmbH licenses this file to you under the Apache License, 
 *  Version 2.0 (the "License"); you may not use this file except in 
 *  compliance with the License. You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.graphhopper.http;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.graphhopper.GraphHopper;
import com.graphhopper.reader.osm.GraphHopperOSM;
import com.graphhopper.util.CmdArgs;
import com.graphhopper.util.TranslationMap;
import com.graphhopper.util.los.TrafficDataKafkaListener;
import com.graphhopper.util.los.TrafficDataReader;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Peter Karich
 */
public class DefaultModule extends AbstractModule {
    protected final CmdArgs args;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private GraphHopper graphHopper;

    public DefaultModule(CmdArgs args) {
        this.args = CmdArgs.readFromConfigAndMerge(args, "config", "graphhopper.config");
//        for(Entry<String,String>e:args.toMap().entrySet()){
//            logger.info("found config parameter "+e.getKey()+":"+e.getValue());
//        }
    }

    public GraphHopper getGraphHopper() {
        if (graphHopper == null)
            throw new IllegalStateException("createGraphHopper not called");

        return graphHopper;
    }

    /**
     * @return an initialized GraphHopper instance
     */
    protected GraphHopper createGraphHopper(CmdArgs args) {
        GraphHopper tmp = new GraphHopperOSM().forServer().init(args);
        if(args.getBool("ch.disabling_allowed",false)){
            logger.info("Allowing disabling CH");
            
        }
        tmp.importOrLoad();
        logger.info("loaded graph at:" + tmp.getGraphHopperLocation()
                + ", data_reader_file:" + tmp.getDataReaderFile()
                + ", flag_encoders:" + tmp.getEncodingManager()
                + ", " + tmp.getGraphHopperStorage().toDetailsString());
        logger.info("graphid = "+tmp.getGraphHopperStorage().getProperties().get("graphid"));

        return tmp;
    }

    @Override
    public void configure() {
        try {
            graphHopper = createGraphHopper(args);
            bind(GraphHopper.class).toInstance(graphHopper);
            GraphHopperServlet.setGraphHopper(graphHopper);
            bind(TranslationMap.class).toInstance(graphHopper.getTranslationMap());

            long timeout = args.getLong("web.timeout", 3000);
            bind(Long.class).annotatedWith(Names.named("timeout")).toInstance(timeout);
            boolean jsonpAllowed = args.getBool("web.jsonp_allowed", false);

            bind(Boolean.class).annotatedWith(Names.named("jsonp_allowed")).toInstance(jsonpAllowed);
            BeMobileRouteSerializer rs = new BeMobileRouteSerializer(graphHopper.getGraphHopperStorage());
            bind(RouteSerializer.class).toInstance(rs);
            GraphHopperServlet.setRouteSerializer(rs);
//            bind(RouteSerializer.class).toInstance(new SimpleRouteSerializer(graphHopper.getGraphHopperStorage().getBounds()));
            Map<String,String> env=System.getenv();
            String kafkaSourceTopic=env.get("kafkaSourceTopic");
            String kafkaBroker=env.get("kafkaBroker");
            String groupId=env.get("groupId");
            String username=env.get("trafficData.username");
            if(username==null)username=env.get("trafficData_username");
            String pass=env.get("trafficData.password");
            if(pass==null)pass=env.get("trafficData_password");
//            if(kafkaTopic==null&&kafkaBroker==null&&groupId==null&&username==null&&pass==null){
//                kafkaTopic=args.get("kafkaSourceTopic", "");
//                kafkaBroker=args.get("kafkaBroker", "");
//                groupId=args.get("groupId", "");
//                username=args.get("trafficData.username","");
//                pass=args.get("trafficData.password","");
//            }
            TrafficDataReader.initTrafficDataImporter(username, pass, graphHopper.getGraphHopperStorage());

            if(kafkaSourceTopic==null||kafkaBroker==null||groupId==null||username==null||pass==null){
                String param="";
                if(kafkaSourceTopic==null)param+="kafkaSourceTopic";
                if(kafkaBroker==null){
                    if(!param.isEmpty())param+=",";
                    param+="kafkaBroker";
                }
                if(groupId==null){
                    if(!param.isEmpty())param+=",";
                    param+="groupId";
                }
                if(username==null){
                    if(!param.isEmpty())param+=",";
                    param+="trafficData.username";
                }
                if(pass==null){
                    if(!param.isEmpty())param+=",";
                    param+="trafficData.password";
                }
                logger.warn("Found missing parameters ("+param+") for live traffic event data, live traffic information will NOT be imported from Kafka, only though webcalls");
                
//                bind(TrafficDataKafkaListener.class).toInstance(tdp);
                
            }else{
                logger.info("Found "+kafkaSourceTopic+" as topic and "+kafkaBroker+" as broker");
            
                TrafficDataKafkaListener tdkl=new TrafficDataKafkaListener(
                        graphHopper.getGraphHopperStorage(),
                        kafkaSourceTopic,  
                        kafkaBroker,
                        groupId
                );

//                bind(TrafficDataKafkaListener.class).toInstance(tdkl);
                Thread tr=new Thread(tdkl);
                tr.start();
            }
        } catch (Exception ex) {
            throw new IllegalStateException("Couldn't load graph", ex);
        }
    }
}
