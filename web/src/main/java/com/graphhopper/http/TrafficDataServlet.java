package com.graphhopper.http;

import com.graphhopper.util.los.TrafficData;
import com.graphhopper.util.los.TrafficDataKafkaListener;
import com.graphhopper.util.los.TrafficDataManager;
import com.graphhopper.util.los.TrafficDataReader;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joda.time.DateTime;

/**
 * Provides url interface for inserting traffic information in CORP
 * @author Maarten
 */
public class TrafficDataServlet extends GHBaseServlet{
    
    @Override
    public void doGet(HttpServletRequest httpReq, HttpServletResponse httpRes) throws ServletException, IOException {
        String url=GHBaseServlet.getParam(httpReq, "trafficDataURL", null);
        String datasetCode=GHBaseServlet.getParam(httpReq, "datasetCode", "");
        String countryCode=GHBaseServlet.getParam(httpReq, "countryCode", "");
        String expirationTime=GHBaseServlet.getParam(httpReq, "expirationTime", null);
        String creationTime=GHBaseServlet.getParam(httpReq, "creationTime", null);
        
        DateTime n=DateTime.now();
        DateTime et=expirationTime==null?n.plusMinutes(10):TrafficDataReader.dtfs.parseDateTime(expirationTime);
        DateTime ct=creationTime==null?n:TrafficDataReader.dtfs.parseDateTime(creationTime);
        
        logger.info("Got call with "+url);
        TrafficData td = TrafficDataReader.readDataFromURL(datasetCode, countryCode,ct,et,url);
        TrafficDataManager.getTrafficDataManager().update(td);
        logger.info("Done call for "+url);
    }
}